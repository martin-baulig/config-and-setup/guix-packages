#!/bin/sh

rsync -acv --exclude .git --delete --rsync-path=/home/aragorn/.guix-profile/bin/rsync /Workspace/guix-packages aragorn@lothlorien:/home
