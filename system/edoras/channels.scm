;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(list
  (channel
    (name 'guix)
    (url "https://gitlab.com/martin-baulig/forks/guix.git")
    (branch "master-stable")
    (introduction
      (make-channel-introduction
        "9d16547afcabf89b3f80e8febd9580a3af302d15"
        (openpgp-fingerprint
          "C00C D05A 798C 805C B18B  4D43 753A 96FB B4A8 B274"))))
  (channel
    (name 'baulig)
    (url "https://gitlab.com/martin-baulig/config-and-setup/guix-packages.git")
    (branch "main")
    (introduction
      (make-channel-introduction
        "31bf484ac8e3ca90ef9f4a8e70b26062882ba22d"
        (openpgp-fingerprint
          "C00C D05A 798C 805C B18B  4D43 753A 96FB B4A8 B274")))))
