;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(use-modules (gnu))
(use-modules (baulig config common))
(use-modules (baulig config edoras parameters))
(use-modules (baulig packages ksh93))
(use-modules (baulig packages gitlab-runner))
(use-modules (baulig packages loki))
(use-modules (baulig services gitlab-runner))
(use-modules (baulig services promtail))
(use-modules (baulig services rsyslog))
(use-package-modules admin bash docker linux logging ssh)
(use-service-modules admin dbus desktop docker monitoring networking ssh)

(operating-system
  (locale "en_US.utf8")
  (timezone "America/New_York")
  (keyboard-layout (keyboard-layout "us"))
  (host-name "edoras.baulig.is")
  (hosts-file %hosts-file)

  (groups (cons*
           (user-group (name "operator"))
           (user-group (name "postgres")
                       (id 4000))
           (user-group (name "bacula-storage")
                       (id 4001))
           (user-group (name "grafana-socket")
                       (id 4002))
           (user-group (name "postgres-socket")
                       (id 4003))
           %base-groups))

  (users (cons*
          (user-account
           (name "operator")
           (group "operator")
           (comment "System Administrator")
           (home-directory "/home/operator")
           (shell (file-append ksh93-baulig "/bin/ksh"))
           (supplementary-groups (list "wheel" "netdev" "docker" %rsyslog-group
                                       "bacula-console-edoras")))
          (user-account
           (name "martin")
           (group "users")
           (comment "Martin Baulig")
           (home-directory "/home/martin")
           (shell (file-append ksh93-baulig "/bin/ksh"))
           (supplementary-groups (list "netdev" "docker" %rsyslog-group)))
          (user-account
           (name "postgres")
           (group "postgres")
           (uid 4000)
           (system? #t)
           (comment "PostgreSQL Daemon User")
           (home-directory "/var/empty")
           (shell (file-append shadow "/sbin/nologin")))
          (user-account
           (name "bacula-storage-full")
           (group "bacula-storage")
           (uid 4001)
           (system? #t)
           (comment "Bacula Backup User")
           (home-directory "/var/services/bacula-storage-full")
           (shell (file-append shadow "/sbin/nologin")))
          (user-account
           (name "bacula-storage-differential")
           (group "bacula-storage")
           (uid 4002)
           (system? #t)
           (comment "Bacula Backup User")
           (home-directory "/var/services/bacula-storage-differential")
           (shell (file-append shadow "/sbin/nologin")))
          (user-account
           (name "bacula-storage-incremental")
           (group "bacula-storage")
           (uid 4003)
           (system? #t)
           (comment "Bacula Backup User")
           (home-directory "/var/services/bacula-storage-incremental")
           (shell (file-append shadow "/sbin/nologin")))
          %base-user-accounts))

  (sudoers-file (plain-file "sudoers" "\
Cmnd_Alias GUIX=/root/.config/guix/current/bin/guix *
Cmnd_Alias HERD=/run/current-system/profile/bin/herd *
Cmnd_Alias BCONSOLE_EDORAS=/home/services/bacula-console-edoras/bconsole/.bconsole-real

root ALL=(ALL) ALL
%wheel ALL=(ALL) ALL
operator ALL=(ALL) NOPASSWD: GUIX, HERD

%bacula-console-edoras ALL=(bacula-console-edoras) NOPASSWD: BCONSOLE_EDORAS
"))

  (packages (append (map specification->package
                         '("gcc-toolchain" "glibc-locales"
                           "inotify-tools" "neovim" "emacs-tramp"
                           "gitlab-runner" "loki"
                           "openssh" "net-tools" "nss-certs" "xauth"))
                    %base-packages))

  (services
   (append (list
            ;; Basic networking services
            (service openssh-service-type
                     (openssh-configuration
                      (openssh openssh-sans-x)
                      (permit-root-login #f)
                      (x11-forwarding? #t)
                      (password-authentication? #f)
                      (challenge-response-authentication? #f)
                      (extra-content "XAuthLocation /run/current-system/profile/bin/xauth\n")
                      (authorized-keys
                       `(("operator" ,(local-file "/etc/guix/martin-yubikey.pub"))
                         ("martin"
                          ,(local-file "/etc/guix/martin-yubikey.pub")
                          ,(local-file "/etc/guix/martin-id.pub"))))))
            (service dhcp-client-service-type)
            (service ntp-service-type)

            ;; These are dependencies of Docker
            (service elogind-service-type)
            (service dbus-root-service-type)

            ;; Docker and Singularity
            (service docker-service-type)
            (service singularity-service-type)

            ;; Prometheus Node Exporter
            (service prometheus-node-exporter-service-type
                     (prometheus-node-exporter-configuration
                      (web-listen-address "10.8.0.10:9100")))

            ;; Rsyslog
            (service rsyslog-service-type
                     (rsyslog-configuration
                      (server (rsyslog-address (ip %edoras-vpn-ip-address) (port 514)))
                      (promtail (rsyslog-address (ip "127.0.0.1") (port 1514)))))

            ;; Log rotation
            (simple-service 'rotate-logs rottlog-service-type %log-rotations))

           %parameterized-services-edoras

           (modify-services %base-services
             (delete syslog-service-type))))

  (bootloader (bootloader-configuration
               (bootloader grub-bootloader)
               (targets (list "/dev/vda"))
               (keyboard-layout keyboard-layout)))

  (swap-devices (list (swap-space
                       (target (uuid
                                "ca2ace76-36ca-4f64-8acb-bafe3828b1ce")))))

  (file-systems (cons* (file-system
                         (mount-point "/")
                         (device (uuid
                                  "5ad33202-43f6-434d-ba06-aa51ccd0fbad"
                                  'ext4))
                         (type "ext4"))
                       (file-system
                         (mount-point "/data")
                         (device (uuid
                                  "c1fcbacb-7ce0-4e42-9a9c-1eb8ef606892"
                                  'ext4))
                         (type "ext4"))
                       (file-system
                         (mount-point "/backup")
                         (device (uuid
                                  "6a3dee92-29b7-43b1-9a0e-c0080763c5fd"
                                  'ext4))
                         (type "ext4"))
                       (file-system
                         (mount-point "/home")
                         (device (uuid
                                  "d42fc324-2229-4c37-97e8-04805377e0d7"
                                  'ext4))
                         (type "ext4"))
                       (file-system
                         (mount-point "/storage")
                         (device (uuid
                                  "3553a94e-7bd1-4438-8b87-9bfade7287bf"
                                  'ext4))
                         (type "ext4"))
                       %base-file-systems)))
