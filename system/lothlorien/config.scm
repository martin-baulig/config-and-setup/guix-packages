;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(use-modules (gnu))
(use-modules (gnu system setuid))
(use-modules (baulig config common))
(use-modules (baulig config nftables))
(use-modules (baulig config lothlorien parameters))
(use-modules (baulig packages ksh93))
(use-modules (baulig services chrony))
(use-modules (baulig services rsyslog))
(use-package-modules admin nfs ntp shells ssh web)
(use-service-modules admin dbus desktop networking nfs ssh virtualization web)

;;;
;;; This is running as a VM on my Synology NAS.
;;;
;;; Unfortunately, full-disk encryption on GNU Guix is still a bit experimental
;;; and the system can easily become unbootable after a 'guix system reconfigure'.
;;;
;;; Therefore, the root partition is not encrypted and all passwords and keys are
;;; stored on the encrypted /home partition.
;;;

(operating-system
  (locale "en_US.utf8")
  (timezone "America/New_York")
  (host-name "lothlorien.baulig.is")
  ;; We do this here instead of using 'hosts-service-type' because we do not
  ;; want the default of mapping the loopback address to our host name.
  (hosts-file
   (plain-file "hosts" (format #f "127.0.0.1 localhost~%")))

  ;; IMPORTANT: Both UID's and GID's must be kept in sync with the
  ;;            ones on the Synology NAS for NFS Mounts to work
  ;;            without any idmap setup.
  (groups (cons*
           (user-group (name "white-council")
                       (id 65542))
           (user-group (name "fellowship")
                       (id 65543))
           ;; This group gives access to the /Data/Storage directory.
           (user-group (name "lothlorien-storage")
                       (id 65544))
           ;; This group gives access to the PostgreSQL-14 data directory.
           (user-group (name "lothlorien-postgres")
                       (id 65545))
           ;; This group gives access to the Bacula data directory.
           (user-group (name "lothlorien-backup")
                       (id 65546))
           ;; This group gives access to the local postgresql socket.
           (user-group (name "postgres")
                       (system? #t))
           %base-groups))

  (users (cons*
          (user-account
           (name "elrond")
           (comment "Master Elrond")
           (group "white-council")
           (uid 1026)
           (password "*")
           (home-directory "/home/elrond")
           (shell (file-append ksh93-baulig "/bin/ksh"))
           (supplementary-groups '("wheel" "netdev" "audio" "video" "syslog" "fellowship"
                                   "bacula-console-master" "bacula-console-external")))
          (user-account
           (name "aragorn")
           (comment "Lord Aragorn")
           (group "fellowship")
           (uid 1031)
           (password "*")
           (home-directory "/home/aragorn")
           (shell (file-append ksh93-baulig "/bin/ksh"))
           (supplementary-groups '("wheel" "netdev" "audio" "video" "syslog"
                                   "bacula-console-external")))
          (user-account
           (name "operator")
           (comment "System Administrator")
           (group "users")
           (home-directory "/home/operator")
           (shell (file-append ksh93-baulig "/bin/ksh"))
           (supplementary-groups '("wheel" "netdev" "syslog" "fellowship"
                                   "bacula-console-master" "bacula-console-external"
                                   "lothlorien-storage" "lothlorien-backup"
                                   "lothlorien-postgres" "postgres")))
          (user-account
           (name "bacula-storage-master")
           (group "lothlorien-backup")
           (supplementary-groups '("lothlorien-storage"))
           (uid 1033)
           (system? #t)
           (comment "Bacula Master Backup User")
           (home-directory "/home/services/bacula-storage-master")
           (shell (file-append shadow "/sbin/nologin")))
          (user-account
           (name "bacula-storage-external")
           (group "lothlorien-backup")
           (supplementary-groups '("lothlorien-storage"))
           (uid 1034)
           (system? #t)
           (comment "Bacula External Backup User")
           (home-directory "/home/services/bacula-storage-external")
           (shell (file-append shadow "/sbin/nologin")))
          (user-account
           (name "bacula-storage-gondor")
           (group "lothlorien-backup")
           (supplementary-groups '("lothlorien-storage"))
           (uid 1035)
           (system? #t)
           (comment "Bacula Gondor Backup User")
           (home-directory "/home/services/bacula-storage-gondor")
           (shell (file-append shadow "/sbin/nologin")))
          (user-account
           (name "bacula-storage-s3")
           (group "lothlorien-backup")
           (supplementary-groups '("lothlorien-storage"))
           (uid 1041)
           (system? #t)
           (comment "Bacula Gondor S3 User")
           (home-directory "/home/services/bacula-storage-s3")
           (shell (file-append shadow "/sbin/nologin")))
          (user-account
           (name "postgres")
           (group "lothlorien-postgres")
           (supplementary-groups '("lothlorien-storage" "postgres"))
           (uid 1036)
           (system? #t)
           (comment "PostgreSQL Daemon User")
           (home-directory "/var/empty")
           (shell (file-append shadow "/sbin/nologin")))
          %base-user-accounts))

  ;; Packages installed system-wide.  Users can also install packages
  ;; under their own account: use 'guix search KEYWORD' to search
  ;; for packages and 'guix install PACKAGE' to install a package.
  (packages (append (map specification->package
                         '("emacs-motif" "neovim" "nfs-utils"
                           "gcc-toolchain" "git" "inotify-tools" "ksh93-baulig"
                           "nss-certs" "chrony"))
                    %base-packages))

  ;; IMPORTANT:
  ;; The "NOPASSWD: GUIX, HERD" line is equivalent of giving the 'operator' user a
  ;; passwordless, unrestricted root shell.
  ;;
  ;; However, I only use that account to reconfigure the system, while doing all my
  ;; editing and everything else as a regular user.
  ;;
  ;; So the 'operator' account is essentially a second 'root' account, but with a
  ;; little protection against accidentally doing something stupid.
  (sudoers-file (plain-file "sudoers" "\
Cmnd_Alias GUIX=/root/.config/guix/current/bin/guix *
Cmnd_Alias HERD=/run/current-system/profile/bin/herd *
Cmnd_Alias BCONSOLE_EXTERNAL=/home/services/bacula-console-external/.bconsole-real
Cmnd_Alias BCONSOLE_MASTER=/home/services/bacula-console-master/.bconsole-real

root ALL=(ALL) ALL

%fellowship ALL=(ALL) ALL
%white-council ALL=(ALL) NOPASSWD: ALL
operator ALL=(ALL) NOPASSWD: GUIX, HERD

%bacula-console-external ALL=(bacula-console-external) NOPASSWD: BCONSOLE_EXTERNAL
%bacula-console-master ALL=(bacula-console-master) NOPASSWD: BCONSOLE_MASTER
 "))

  ;; Below is the list of system services.  To search for available
  ;; services, run 'guix system search KEYWORD' in a terminal.
  (services
   (append (list
            (service openssh-service-type
                     (openssh-configuration
                      (openssh openssh-sans-x)
                      (permit-root-login #t)
                      (password-authentication? #t)
                      (challenge-response-authentication? #t)
                      (authorized-keys
                       `(("elrond" ,(local-file "/etc/guix/martin-yubikey.pub"))
                         ("aragorn"
                          ,(local-file "/etc/guix/martin-yubikey.pub")
                          ,(local-file "/etc/guix/martin-id.pub"))))
                      ;; Restrict Elrond to logging in from Gondor va Yubikey.
                      ;; Aragorn may log in from any host in the local network.
                      (extra-content "\
AllowUsers elrond@192.168.8.3
AllowUsers aragorn@192.168.8.*
")))

            (service chrony-service-type)

            (extra-special-file "/bin/ls" (file-append coreutils "/bin/ls"))
            (extra-special-file "/bin/scp" (file-append openssh "/bin/scp"))

            (service elogind-service-type)
            (service dbus-root-service-type)

            (service qemu-guest-agent-service-type)

            (service nftables-service-type
                     (nftables-configuration (ruleset %baulig-nftables-ruleset)))

            ;; Rsyslog
            (service rsyslog-service-type
                     (rsyslog-configuration
                      (forward (rsyslog-address (ip %edoras-vpn-ip-address) (port 514)))))

            ;; Log Rotation
            (simple-service 'rotate-logs rottlog-service-type %log-rotations)

            ;; Static IP Address.
            (service static-networking-service-type
                     (list (static-networking
                            (addresses
                             (list (network-address
                                    (device "eth0")
                                    (value "192.168.8.2/16"))))
                            (routes
                             (list (network-route
                                    (destination "default")
                                    (gateway "192.168.1.1"))))
                            (name-servers '("192.168.8.1" "192.168.1.1"))))))

           %parameterized-services-lothlorien

           (modify-services %base-services
             (delete syslog-service-type))))

  (kernel-arguments
   '("nomodeset"))

  (bootloader (bootloader-configuration
               (bootloader grub-bootloader)
               (timeout 30)
               (targets (list "/dev/sda"))))
  (initrd-modules (append '("virtio_scsi") %base-initrd-modules))
  (mapped-devices (list (mapped-device
                         (source (uuid "4ceba478-8da2-446d-9b3c-e37ebae91227"))
                         (target "root")
                         (type luks-device-mapping))))
  (file-systems (cons* (file-system
                         (mount-point "/")
                         (device (uuid "10b23f2d-bda4-4db6-829f-543ff4ec42ea"))
                         (type "ext4")
                         (dependencies mapped-devices))
                       (file-system
                         (mount-point "/Data/Storage")
                         (device "imladris.baulig.is:/volume1/Storage")
                         (type "nfs")
                         (mount? #t)
                         (create-mount-point? #t)
                         (options "nfsvers=3,nolock"))
                       %base-file-systems))

  (swap-devices
   (list
    (swap-space
     (target "/swapfile")
     (dependencies (filter (file-system-mount-point-predicate "/")
                           file-systems))))))
