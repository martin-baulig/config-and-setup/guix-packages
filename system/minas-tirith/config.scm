;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(use-modules (gnu))
(use-modules (gnu system setuid))
(use-modules (baulig config common))
(use-modules (baulig config nftables))
(use-modules (baulig config minas-tirith parameters))
(use-modules (baulig packages ksh93))
(use-modules (baulig packages vmm-clock))
(use-modules (baulig services chrony))
(use-modules (baulig services rsyslog))
(use-package-modules admin nfs shells ssh web)
(use-service-modules dbus desktop linux networking nfs ssh)

(operating-system
  (locale "en_US.utf8")
  (timezone "America/New_York")
  (keyboard-layout (keyboard-layout "us"))
  (host-name "minas-tirith.baulig.is")
  ;; We do this here instead of using 'hosts-service-type' because we do not
  ;; want the default of mapping the loopback address to our host name.
  (hosts-file
   (plain-file "hosts" (format #f "127.0.0.1 localhost~%")))

  (users (cons* (user-account
                 (name "martin")
                 (comment "Martin Baulig")
                 (group "users")
                 (home-directory "/home/martin")
                 (shell (file-append ksh93-baulig "/bin/ksh"))
                 (supplementary-groups '("wheel" "netdev" "audio" "video" "syslog")))
                (user-account
                 (name "operator")
                 (comment "System Administrator")
                 (group "users")
                 (home-directory "/home/operator")
                 (shell (file-append ksh93-baulig "/bin/ksh"))
                 (supplementary-groups '("wheel" "netdev" "syslog")))
                %base-user-accounts))

  ;; IMPORTANT:
  ;; The "NOPASSWD: GUIX, HERD" line is equivalent of giving the 'operator' user a
  ;; passwordless, unrestricted root shell.
  ;;
  ;; However, I only use that account to reconfigure the system, while doing all my
  ;; editing and everything else as a regular user.
  ;;
  ;; So the 'operator' account is essentially a second 'root' account, but with a
  ;; little protection against accidentally doing something stupid.
  (sudoers-file (plain-file "sudoers" "\
Cmnd_Alias GUIX=/root/.config/guix/current/bin/guix *
Cmnd_Alias GUIX_SECRETS=/root/.local/bin/guix-secrets *
Cmnd_Alias HERD=/run/current-system/profile/bin/herd *
Cmnd_Alias HERD_STATUS=/run/current-system/profile/bin/herd *
Cmnd_Alias BCONSOLE=/home/services/bacula-console-external/.bconsole-real

root ALL=(ALL) ALL
%wheel ALL=(ALL) ALL
%wheel ALL=(ALL) NOPASSWD: HERD_STATUS
operator ALL=(ALL) NOPASSWD: GUIX, GUIX_SECRETS, HERD

%bacula-console-external ALL=(bacula-console-external) NOPASSWD: BCONSOLE
"))

  ;; Packages installed system-wide.  Users can also install packages
  ;; under their own account: use 'guix search KEYWORD' to search
  ;; for packages and 'guix install PACKAGE' to install a package.
  (packages (append (map specification->package
                         '("emacs-motif" "neovim" "ksh93-baulig"
                           "gcc-toolchain" "git" "inotify-tools" "ncurses"
                           "net-tools" "nfs-utils" "nss-certs"))
                    %base-packages))

  (setuid-programs (append (list
                            (setuid-program
                             (program (file-append nfs-utils "/sbin/mount.nfs"))))
                           %setuid-programs))

  ;; Below is the list of system services.  To search for available
  ;; services, run 'guix system search KEYWORD' in a terminal.
  (services
   (append (list
            (service openssh-service-type
                     (openssh-configuration
                      (x11-forwarding? #t)
                      (permit-root-login #f)
                      (password-authentication? #f)
                      (authorized-keys
                       `(("operator" ,(local-file "/etc/guix/martin-yubikey.pub"))
                         ("martin"
                          ,(local-file "/etc/guix/martin-yubikey.pub")
                          ,(local-file "/etc/guix/martin-id.pub"))))
                      ;; Restrict Operator to logging in from Gondor via Yubikey.
                      ;; Martin may log in from any host in the local network.
                      (extra-content "\
AllowUsers operator@192.168.8.3
AllowUsers martin@192.168.8.*")))

            (service kernel-module-loader-service-type
                     '("vmm-clock"))

            (service chrony-service-type)

            (extra-special-file "/bin/ls" (file-append coreutils "/bin/ls"))
            (extra-special-file "/bin/scp" (file-append openssh "/bin/scp"))

            (service nftables-service-type
                     (nftables-configuration (ruleset %baulig-nftables-ruleset)))

            ;; Rsyslog
            (service rsyslog-service-type
                     (rsyslog-configuration
                      (forward (rsyslog-address
                                (ip %edoras-vpn-ip-address)
                                (port 514)))))

            (service elogind-service-type)
            (service dbus-root-service-type)

            ;; Static IP Address.
            (service static-networking-service-type
                     (list (static-networking
                            (addresses
                             (list (network-address
                                    (device "eth0")
                                    (value "192.168.8.5/16"))))
                            (routes
                             (list (network-route
                                    (destination "default")
                                    (gateway "192.168.1.1"))))
                            (name-servers '("192.168.8.1"))))))

           %parameterized-services-minas-tirith

           (modify-services %base-services
             (delete syslog-service-type))))

  (kernel-arguments
   '("console=ttyS0,115200"))

  (kernel-loadable-modules
   (list vmm-clock))

  (bootloader (bootloader-configuration
               (bootloader grub-bootloader)
               (targets (list "/dev/vda"))
               (terminal-outputs '(serial))
               (terminal-inputs '(serial))
               (serial-unit 0)
               (serial-speed 115200)
               (keyboard-layout keyboard-layout)))

  (swap-devices (list (swap-space
                       (target (uuid
                                "36282479-ffa1-41b1-9589-e471e877e84a")))))

  (file-systems (cons* (file-system
                         (mount-point "/")
                         (device (uuid
                                  "4cdf35bc-caaa-412b-b30f-024df01c18a1"
                                  'ext4))
                         (type "ext4"))
                       (file-system
                         (mount-point "/home")
                         (device (uuid
                                  "843d6943-926e-45ab-8b0f-c69f222d23bc"
                                  'ext4))
                         (type "ext4"))
                       (file-system
                         (mount-point "/Workspace")
                         (device "gondor.baulig.is:/Workspace")
                         (type "nfs")
                         (mount? #f)
                         (create-mount-point? #t)
                         (options "nfsvers=3,nolock,user,soft,exec,nodev,nosuid"))
                       %base-file-systems)))
