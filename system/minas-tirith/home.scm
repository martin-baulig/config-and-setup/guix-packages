;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(use-modules (gnu home)
             (gnu home services)
             (gnu packages)
             (gnu services)
             (guix gexp)
             (gnu home services shells))

(home-environment
 ;; Below is the list of packages that will show up in your
 ;; Home profile, under ~/.guix-home/profile.
  (packages (specifications->packages (list
                                       "aspell"
                                       "binutils"
                                       "bpytop"
                                       "cmake"
                                       "docker-cli"
                                       "docker-compose"
                                       "emacs-geiser"
                                       "emacs-geiser-guile"
                                       "emacs-guix"
                                       "emacs-motif"
                                       "emacs-tramp"
                                       "emacs-vterm"
                                       "font-fira-code"
                                       "font-fira-mono"
                                       "font-fira-sans"
                                       "gcc-toolchain"
                                       "git"
                                       "gnupg"
                                       "guile"
                                       "ksh93-baulig"
                                       "libvterm"
                                       "make"
                                       "ncurses"
                                       "neovim"
                                       "openssl"
                                       "recode"
                                       "ripgrep"
                                       "rsync"
                                       "sharutils"
                                       "texlive-utopia"
                                       "xauth")))

 ;; Below is the list of Home services.  To search for available
 ;; services, run 'guix home search KEYWORD' in a terminal.
 (services
  (list
   (simple-service 'martin-env-vars
                   home-environment-variables-service-type
                   `(("GUIX_BUILD_OPTIONS" . "-L /Workspace/guix-packages/packages -v 3"))))))
