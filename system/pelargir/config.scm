;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(use-modules (gnu))
(use-modules (baulig config common))
(use-modules (baulig config pelargir parameters))
(use-modules (baulig packages ksh93))
(use-modules (baulig services rsyslog))
(use-package-modules admin bash docker logging ssh)
(use-service-modules admin dbus desktop docker monitoring networking ssh)

(operating-system
  (locale "en_US.utf8")
  (timezone "America/New_York")
  (keyboard-layout (keyboard-layout "us"))
  (host-name "pelargir.baulig.is")
  (hosts-file %hosts-file)

  (groups (cons*
           (user-group (name "operator"))
           %base-groups))

  (users (cons*
          (user-account
           (name "operator")
           (group "operator")
           (comment "System Administrator")
           (home-directory "/home/operator")
           (shell (file-append ksh93-baulig "/bin/ksh"))
           (supplementary-groups (list "wheel" "netdev" "docker" %rsyslog-group)))
          (user-account
           (name "martin")
           (group "users")
           (comment "Martin Baulig")
           (home-directory "/home/martin")
           (shell (file-append ksh93-baulig "/bin/ksh"))
           (supplementary-groups (list "netdev" "docker" %rsyslog-group)))
          %base-user-accounts))

  (sudoers-file (plain-file "sudoers" "\
Cmnd_Alias GUIX=/root/.config/guix/current/bin/guix *
Cmnd_Alias HERD=/run/current-system/profile/bin/herd *

root ALL=(ALL) ALL
%wheel ALL=(ALL) ALL
operator ALL=(ALL) NOPASSWD: GUIX, HERD "))


  (packages (append (map specification->package
                         '("gcc-toolchain" "glibc-locales"
                           "inotify-tools" "neovim" "emacs-tramp"
                           "openssh" "net-tools" "nss-certs" "xauth"))
                    %base-packages))

  (services
   (append (list
            ;; Basic networking services
            (service openssh-service-type
                     (openssh-configuration
                      (openssh openssh-sans-x)
                      (permit-root-login #f)
                      (x11-forwarding? #t)
                      (password-authentication? #f)
                      (challenge-response-authentication? #f)
                      (extra-content "XAuthLocation /run/current-system/profile/bin/xauth\n")
                      (authorized-keys
                       `(("operator" ,(local-file "/etc/guix/martin-yubikey.pub"))
                         ("martin"
                          ,(local-file "/etc/guix/martin-yubikey.pub")
                          ,(local-file "/etc/guix/martin-id.pub"))))))

            ;; These are needed by "guix home" as well as Docker.
            (service elogind-service-type)
            (service dbus-root-service-type)

            ;; Network
            (service static-networking-service-type
                     (list
                      (static-networking
                       (addresses (list (network-address
                                         (device "eth0")
                                         (value "149.28.98.140/23"))
                                        (network-address
                                         (device "eth1")
                                         (value "10.32.0.3/16"))))
                       (routes (list (network-route
                                      (destination "default")
                                      (gateway "149.28.98.1"))))
                       (name-servers '("108.61.10.10" "8.8.8.8")))))

            (service ntp-service-type)

            ;; Rsyslog
            (service rsyslog-service-type
                     (rsyslog-configuration
                      (forward (rsyslog-address (ip %edoras-vpn-ip-address) (port 514)))))

            ;; Docker and Singularity
            (service docker-service-type)
            (service singularity-service-type)

            ;; Log Rotation
            (simple-service 'rotate-logs rottlog-service-type %log-rotations)

            ;; Prometheus Node Exporter
            (service prometheus-node-exporter-service-type
                     (prometheus-node-exporter-configuration
                      (web-listen-address "10.8.0.11:9100"))))

           ;; Parameterized services
           %parameterized-services-pelargir

           (modify-services %base-services
             (delete syslog-service-type))))

  (bootloader (bootloader-configuration
               (bootloader grub-bootloader)
               (targets (list "/dev/vda"))
               (keyboard-layout keyboard-layout)))

  (swap-devices (list (swap-space
                       (target (uuid
                                "ea0cdad4-5cc2-44a9-9386-b3b079a0f1c0")))))

  (file-systems (cons* (file-system
                         (mount-point "/")
                         (device (uuid
                                  "957b5903-f7b2-43ff-9a11-62a607c4e556"
                                  'ext4))
                         (type "ext4"))
                       (file-system
                         (mount-point "/home")
                         (device (uuid
                                  "2b2b7b8d-e726-40f1-9a96-425d7a76219f"
                                  'ext4))
                         (type "ext4"))
                       %base-file-systems)))
