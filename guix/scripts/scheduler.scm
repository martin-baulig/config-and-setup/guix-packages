;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (guix scripts scheduler)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 binary-ports)
  #:use-module (ice-9 textual-ports)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-26)
  #:use-module (rnrs enums)
  #:use-module (sqlite3))


;;;
;;; Scheduler Database
;;;

(define %scheduler-database "scheduler.db")

(define (sqlite-exec* db sql key value)
  (let ((stmt (sqlite-prepare db sql)))
    (sqlite-bind stmt key value)
    (let ((result (sqlite-map identity stmt)))
      (sqlite-finalize stmt)
      result)))

(define-public (scheduler-database-create file)
  (when (file-exists? file)
    (report-error "Scheduler database '~a' already exists" file))
  (let* ((db (sqlite-open file)))
    (sqlite-exec db "CREATE TABLE scheduler (name STRING PRIMARY KEY, last INTEGER, interval INTEGER, command TEXT)")
    (sqlite-close db)))


(define-record-type <scheduler-database>
  (make-scheduler-database db)
  scheduler-database?
  (db scheduler-database-db set-scheduler-database-db!))


(define-record-type <scheduler-entry>
  (make-scheduler-entry name last interval command)
  scheduler-entry?

  (name                 scheduler-entry-name)
  (last                 scheduler-entry-last)
  (interval             scheduler-entry-interval)
  (command              scheduler-entry-command))


;;;
;;; Regex checks
;;;

(define-public (scheduler-database-key? value)
  (and (string? value)
       (string-match %name-regex value)))


(define-public %name-regex "^[A-Za-z][A-Za-z0-9]*([\\.-][A-Za-z0-9]+)*$")


;;;
;;; Opening and closing
;;;

(define* (scheduler-database-open #:optional file-name)
  (let* ((file (or file-name %scheduler-database))
         (flags (logior SQLITE_OPEN_READWRITE SQLITE_OPEN_EXCLUSIVE))
         (db (sqlite-open file flags)))
    (make-scheduler-database db)))

(export scheduler-database-open)


(define-public scheduler-database-close
  (match-lambda
    (($ <scheduler-database> db)
     (sqlite-close db))))


;;;
;;; Querying entries
;;;

(define-public scheduler-database-list
  (match-lambda
    (($ <scheduler-database> db)
     (let ((stmt (sqlite-prepare db "SELECT name FROM scheduler")))
       (map (lambda (vector)
              (vector-ref vector 0))
            (sqlite-map identity stmt))))))


(define* (scheduler-database-get-item scheduler-database name
                                      #:key (throw-on-error #t))
  (unless (scheduler-database-key? name)
    (report-error "invalid name: ~a" name))
  (match scheduler-database
    (($ <scheduler-database> db)
     (let ((stmt (sqlite-prepare db "SELECT last, interval, command FROM scheduler WHERE name = :name")))
       (sqlite-bind stmt 'name name)
       (let ((result (sqlite-step stmt)))
         (sqlite-finalize stmt)
         (if (vector? result)
             (make-scheduler-entry name (vector-ref result 0) (vector-ref result 1) (vector-ref result 2))
             (begin
               (when throw-on-error
                 (report-error "no such key in scheduler database: ~a" name))
               #f)))))))

(export scheduler-database-get-item)


(define-public (scheduler-database-has-entry? scheduler-database name)
  (unless (scheduler-database-key? name)
    (report-error "invalid name: ~a" name))
  (match scheduler-database
    (($ <scheduler-database> db)
     (let ((result (sqlite-exec* db "SELECT last FROM scheduler WHERE name = :name" 'name name)))
       (> (length result) 0)))))


;;;
;;; Adding and removing entries
;;;

(define* (scheduler-database-add-item scheduler-database item #:key (replace #f))
  (match item
    (($ <scheduler-entry> name last interval command)
     (unless (scheduler-database-key? name)
       (report-error "invalid name: ~a" name))

     (when (and (not replace) (scheduler-database-has-entry? scheduler-database name))
       (report-error "entry already exists: ~a" name))

     (match scheduler-database
       (($ <scheduler-database> db)
        (let ((stmt (sqlite-prepare db "INSERT OR REPLACE INTO scheduler (name, last, interval, command) VALUES (?, ?, ?, ?)")))
          (sqlite-bind stmt 1 name)
          (sqlite-bind stmt 2 last)
          (sqlite-bind stmt 3 interval)
          (sqlite-bind stmt 4 command)
          (sqlite-step stmt)
          (sqlite-finalize stmt)))))))

(export scheduler-database-add-item)

;;;
;;; Command Actions
;;;

(define (report-error . args)
  (let* ((formatted (apply format #f args))
         (message (format #f "scheduler database: ~a~%" formatted)))
    (display message)
    (display message (current-error-port))
    (exit -1)))


(define (check-scheduler-database file-name)
  (unless (file-exists? file-name)
    (report-error "scheduler database does not exist: ~a" file-name))
  (unless (access? file-name (logior R_OK W_OK))
    (report-error "insufficient permissions to access scheduler database: ~a" file-name)))


(define-syntax with-scheduler-database
  (syntax-rules ()
    ((_ file-name func args ...)
     (begin
       (check-scheduler-database file-name)
       (let* ((database (scheduler-database-open file-name))
              (result (func database args ...)))
         (scheduler-database-close database)
         result)))))


;;;
;;; Run Scheduler
;;;

(define* (scheduler-database-run-item scheduler-database name)
  (let* ((item (scheduler-database-get-item scheduler-database name))
         (last (scheduler-entry-last item))
         (interval (scheduler-entry-interval item))
         (command (scheduler-entry-command item))
         (next (+ last interval))
         (now (current-time)))
    (format #t "RUN SCHEDULER: ~a - ~a / ~a - ~a~%" name now next item)
    (when (< next now)
      (format #t "NEED TO RUN: ~a - ~a~%" name command)
      (if (eq? last 0)
          (format #t "Scheduler item ~s has never been run.~%" name)
          (format #t "Scheduler item ~s has last been run at ~a and was due at ~a~%"
                  name (strftime "%c" (localtime last))
                  (strftime "%c" (localtime next))))
      (format #t "Invoking command ~s~%" command)
      (let ((result (system command)))
        (format #t "Command result: ~a~%" result)
        (unless (eq? result 0)
          (report-error "Scheduler item ~s failed with exit code ~a~%" name result))))))


(define-public (scheduler-run)
  (with-scheduler-database
   %scheduler-database
   (lambda (db)
     (let ((items (scheduler-database-list db)))
       (format #t "SCHEDULER: ~a~%" items)
       (for-each (cut scheduler-database-run-item db <>) items)
       (format #t "SCHEDULER DONE.~%")))))
