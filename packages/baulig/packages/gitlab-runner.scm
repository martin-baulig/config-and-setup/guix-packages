;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig packages gitlab-runner)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (baulig multi-fetch)
  #:use-module (baulig build-system go-module))

(define-public gitlab-runner
  (let ((commit "v15.11.0")
        (git-hash "0jmk6lka1cpaac82rnpajx9hlh7j7qzikzzaww4zynjdx88rv0jb")
        (hash "1as87adck88mmxcxzn6hm17x0raj785lj3n6vbhihr18by1l45dc")
        (revision "1"))
    (package
      (name "gitlab-runner")
      (version "15.11.0")
      (source
       (origin
         (method multi-fetch)
         (uri (multi-fetch-reference
               (git (git-reference
                     (url "https://gitlab.com/gitlab-org/gitlab-runner.git")
                     (commit commit)
                     (recursive? #t)))
               (git-hash (base32 git-hash))))
         (file-name (git-file-name name version))
         (sha256 (base32 hash))))
      (build-system go-module-build-system)
      (home-page "https://gitlab.com/gitlab-org/gitlab-runner")
      (synopsis "gitlab-runner")
      (description "GitLab Runner is the open source project that is used to run your CI/CD jobs and send the results back to GitLab")
      (license license:expat))))
