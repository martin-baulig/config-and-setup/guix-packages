;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig packages loki)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages)
  #:use-module (baulig multi-fetch)
  #:use-module (baulig build-system go-module))

(define-public loki
  (let ((commit "v2.9.2")
        (git-hash "1jba9czh8i1d09am4khb0wckg1w39klfzmfsp1fhq6b6n12p862z")
        (hash "171hbwz2vv2wc6j8kq7yzj6cmxl0lxyx1mhm9ja9bq1aak3cxfd3")
        (revision "1"))
    (package
      (name "loki")
      (version "2.9.2")
      (source
       (origin
         (method multi-fetch)
         (uri (multi-fetch-reference
               (git (git-reference
                     (url "https://github.com/grafana/loki")
                     (commit commit)
                     (recursive? #t)))
               (git-hash (base32 git-hash))))
         (file-name (git-file-name name version))
         (sha256 (base32 hash))))
      (build-system go-module-build-system)
      (arguments '(#:environment-variables (("CGO_ENABLED" . "0"))
                   #:go-commands ("cmd/loki"
                                  "clients/cmd/promtail")))
      (home-page "https://grafana.com/loki")
      (synopsis "Like Prometheus, but for logs.")
      (description "Loki is a horizontally-scalable, highly-available, multi-tenant log aggregation system inspired by Prometheus.")
      (license license:agpl3))))
