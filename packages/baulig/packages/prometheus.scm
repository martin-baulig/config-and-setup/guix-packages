;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig packages prometheus)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix build utils)
  #:use-module (guix build-system copy)
  #:use-module (gnu packages))


(define-public prometheus
  (package
    (name "prometheus")
    (version "2.45.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://github.com/prometheus/prometheus/releases/download/v2.45.1/prometheus-2.45.1.linux-amd64.tar.gz")
       (sha256 (base32 "1787vxy00mm8074n1c4q1crbndgfgxm5x8ggp7zjhyk2y3kg7ga8"))))
    (build-system copy-build-system)
    (home-page "https://prometheus.is/")
    (synopsis "Prometheus")
    (description "Prometheus")
    (license license:asl2.0)))

