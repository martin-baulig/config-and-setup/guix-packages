;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig packages vmm-clock)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix build-system linux-module)
  #:use-module (gnu packages))

(define-public vmm-clock
  (let ((commit "9db6f6ccea982c6fef81d7c0d7858ba15cf9d04f")
        (hash "1slldhf8wr83aqijvd1w3zcbl94r9nmqvyy1s9w5fy74svywsgzk"))
    (package
      (name "vmm-clock")
      (version "0.2.0")
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/voutilad/vmm_clock")
               (commit commit)
               (recursive? #t)))
         (file-name (git-file-name name version))
         (sha256 (base32 hash))))
      (build-system linux-module-build-system)
      (arguments
       (list #:tests? #f))
      (home-page "https://github.com/voutilad/vmm_clock")
      (synopsis "Linux clocksource for OpenBSD hypervisor guests")
      (description "This is a Linux clocksource implementation based on the existing Linux kvmclock.
It's specifically designed for using when running a Linux kernel as a guest under
OpenBSD's vmm(4)/vmd(8) hypervisor framework.")
      (license license:gpl2))))
