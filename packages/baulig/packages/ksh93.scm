;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig packages ksh93)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (gnu packages ed)
  #:use-module (gnu packages rust-apps)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix utils)
  #:use-module (baulig build-system ksh))

(define-public ksh93
  (package
    (name "ksh93")
    (version "1.4.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ksh93/ksh")
                    (commit "v1.0.4")))
              (file-name (git-file-name name version))
              (sha256
               (base32 "02dqfxpaszvv5nw1izbn3m18yhq51iqrxfzzi2pbsrqa1cldqx4z"))))
    (build-system ksh-build-system)
    (arguments `())
    (home-page "https://github.com/ksh93/ksh")
    (synopsis "AT&T Korn Shell")
    (description "AT&T Korn Shell")
    (license license:epl2.0)))

(define-public ksh93-baulig
  (package
    (name "ksh93-baulig")
    (version "1.4.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://gitlab.com/martin-baulig/config-and-setup/ksh-config.git")
                    (commit "55daca38ec56353d6a544d835d62c8b157428214")
                    (recursive? #t)))
              (sha256
               (base32 "0p2pqd48g8if81iiqdhr1dw4k6ym84m4yqlbwzji45lpsq5lijik"))))
    (build-system ksh-build-system)
    (arguments `(#:root "external/ksh"
                 #:install-etc? #t))
    (propagated-inputs (list ed vivid))
    (home-page "https://gitlab.com/martin-baulig/config-and-setup/ksh-config")
    (synopsis "AT&T Korn Shell - Baulig Scripts")
    (description "AT&T Korn Shell with my ksh.kshrc scripts.")
    (license license:epl2.0)))
