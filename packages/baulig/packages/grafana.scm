;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig packages grafana)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix build utils)
  #:use-module (guix build-system copy)
  #:use-module (gnu packages))


(define-public grafana
  (package
    (name "grafana")
    (version "10.1.5")
    (source
     (origin
       (method url-fetch)
       (uri "https://dl.grafana.com/oss/release/grafana-10.1.5.linux-amd64.tar.gz")
       (sha256 (base32 "03amdsk7mj9y8y47ljq1qqcpgql27skkslkhbhqc2r2vrbzsrcgc"))))
    (build-system copy-build-system)
    (home-page "https://www.grafana.com/")
    (synopsis "Grafana Community Edition")
    (description "Grafana Community Edition")
    (license license:agpl3)))
