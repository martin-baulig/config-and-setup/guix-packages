;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig packages baculum)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages)
  #:use-module (baulig build-system baculum))

(define-public baculum
  (let ((commit "767abf0deae52dc25ba8a94f3334620d932d4525")
        (hash "1a2an1rszrrca78s3szd173vcdaz3bb1r5i92vg2knlqkjcfifay")
        (revision "1"))
    (package
      (name "baculum")
      (version "13.0")
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/martin-baulig/forks/bacula-community.git")
               (commit commit)
               (recursive? #t)))
         (file-name (git-file-name name version))
         (sha256 (base32 hash))))
      (build-system baculum-build-system)
      (home-page "https://www.bacula.org/")
      (synopsis "Bacula Community Edition")
      (description "Bacula API and Web Interface")
      (license license:agpl3))))
