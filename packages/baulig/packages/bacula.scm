;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig packages bacula)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix build utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages xml)
  #:use-module (baulig build-system bacula))


(define-public bacula-libs3
  (package
    (name "bacula-libs3")
    (version "4.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://www.bacula.org/downloads/libs3-20200523.tar.gz")
       (sha256 (base32 "0bw12x46c6mgqyxyj8433g3pxf3a99i6n5agy50lm6cm9ifdy156"))
       (patches
        (search-patches "patches/bacula-libs3.patch"))))
    (inputs
     (list coreutils curl gnu-make libxml2 openssl))
    (build-system gnu-build-system)
    (arguments
     `(#:make-flags (list "VERBOSE=1" "CC=gcc")
       #:phases (modify-phases %standard-phases
                  (delete 'check)
                  (delete 'configure)
                  (replace 'install
                    (lambda* (#:key outputs #:allow-other-keys)
                      (let ((out (assoc-ref outputs "out")))
                        (invoke "make" "install"
                                (string-append "DESTDIR=" out))))))))
    (home-page "https://www.bacula.org/")
    (synopsis "Bacula S3 Library")
    (description "Bacula S3 Library")
    (license license:lgpl3)))


(define-public bacula
  (let ((commit "111333a97b10ce620c5b5bcf290980aaf454d922")
        (hash "1aaj3imln64chjin1gifhzdz8fidb10bl83pifc92grbzb364g4l")
        (revision "1"))
    (package
      (name "bacula")
      (version "13.0")
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.bacula.org/bacula-community-edition/bacula-community")
               (commit commit)
               (recursive? #t)))
         (file-name (git-file-name name version))
         (sha256 (base32 hash))))
      (inputs (list bacula-libs3 zlib lzo))
      (build-system bacula-build-system)
      (home-page "https://www.bacula.org/")
      (synopsis "Bacula Community Edition")
      (description "Bacula Community Edition")
      (license license:agpl3))))
