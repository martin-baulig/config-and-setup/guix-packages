;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig packages martin-org-publish)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system emacs)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages emacs-xyz))

(define-public martin-org-publish
  (let ((commit "b7744088b0fe9851a328ff53ba4f7c391e4dc5d9")
        (hash "07lbh74jx1pgyk31cfkk2h14f2wp080cq82xgp67ymvdvv5kfd66")
        (revision "1"))
    (package
      (name "martin-org-publish")
      (version (git-version "0.1" revision commit))
      (source
       ;; (origin
       ;;   (method url-fetch)
       ;;   (uri "/home/martin/martin-org-publish")
       ;;   (hash (content-hash "0xqylnnis6dj3gg0fyf5jwjvii75h3x2gh1cqjxxvgf34ax8agb5")))
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/martin-baulig/config-and-setup/martin-org-publish.git")
               (commit commit)
               (recursive? #t)))
         (file-name (git-file-name name version))
         (sha256 (base32 hash))))
      (build-system emacs-build-system)
      (propagated-inputs (list emacs-htmlize))
      (home-page "https://gitlab.com/martin-baulig/config-and-setup/martin-org-publish")
      (synopsis "Martin's org-mode publishing scripts.")
      (description "Martin's org-mode publishing scripts.")
      (license license:gpl3+))))
