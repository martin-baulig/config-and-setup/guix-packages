;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig multi-fetch)
  #:use-module (web uri)
  #:use-module (guix gexp)
  #:use-module (guix store)
  #:use-module (guix monads)
  #:use-module (guix records)
  #:use-module (guix packages)
  #:use-module (guix modules)
  #:use-module (guix git-download)
  #:use-module (gnu packages base)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages golang)
  #:autoload   (guix build-system gnu) (standard-packages)
  #:export (multi-fetch-reference
            multi-fetch-reference?
            multi-fetch-reference-git

            multi-fetch))


(define-record-type* <multi-fetch-reference>
  multi-fetch-reference make-multi-fetch-reference
  multi-fetch-reference?
  (git multi-fetch-reference-git)
  (git-hash multi-fetch-reference-git-hash))

(define* (multi-fetch ref hash-algo hash
                      #:optional name
                      #:key (system (%current-system)) (guile (default-guile))
                      (go go-1.19))

  (define inputs
    `(("go" ,go)
      ("nss-certs" ,nss-certs)
      ,@(standard-packages)))

  (define guile-json
    (module-ref (resolve-interface '(gnu packages guile)) 'guile-json-4))

  (define modules
    (delete '(guix config)
            (source-module-closure '((guix build download)
                                     (guix build utils)
                                     (web uri)))))

  (define* (build source)
    (with-imported-modules modules
      (with-extensions (list guile-json)
        #~(begin
            (use-modules (guix build download)
                         (guix build utils)
                         (web uri)
                         (ice-9 match))

            (let ((cert-dir (string-append #$nss-certs "/etc/ssl/certs"))
                  (source-dir (string-append #$output "/source"))
                  (mod-cache (string-append #$output "/go/pkg")))
              (setenv "SSL_CERT_DIR" cert-dir)
              (setenv "GOCACHE" "/homeless-shelter")
              (setenv "GOPATH" "/homeless-shelter")
              (setenv "GOMODCACHE" mod-cache)

              (mkdir-p mod-cache)
              (mkdir-p source-dir)

              (copy-recursively #$source source-dir)

              (with-directory-excursion #$source
                (invoke (string-append #$go "/bin/go") "mod" "download")))))))

  (mlet* %store-monad ((guile-for-build (package->derivation guile system))
                       (source (git-fetch (multi-fetch-reference-git ref) hash-algo
                                          (multi-fetch-reference-git-hash ref)
                                          #:system system #:guile guile)))
    (gexp->derivation (or name "multi-fetch") (build source)

                      #:script-name "multi-fetch"

                      #:leaked-env-vars '("http_proxy" "https_proxy"
                                          "LC_ALL" "LC_MESSAGES" "LANG"
                                          "COLUMNS")

                      #:system system
                      #:local-build? #t
                      #:recursive? #t
                      #:hash-algo hash-algo
                      #:hash hash
                      #:guile-for-build guile-for-build)))
