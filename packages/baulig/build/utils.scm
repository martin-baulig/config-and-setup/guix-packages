;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig build utils)
  #:use-module (guix build utils)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 textual-ports)
  #:export (invoke-as-user
            postgresql-database-exists?
            postgresql-role-exists?
            postgresql-create-role
            read-from-psql
            read-from-su
            run-psql))


(define-public (install-with-owner source dest uid gid mode)
  (copy-file source dest)
  (chown dest uid gid)
  (chmod dest mode))


(define-public (create-file-with-owner file uid gid mode)
  (unless (file-exists? file)
    (close (open file (logior O_WRONLY O_CREAT) mode)))
  (chmod file mode)
  (chown file uid gid))


(define-public (create-directory-with-owner dir uid gid mode)
  (unless (directory-exists? dir)
    (mkdir-p dir))
  (chmod dir mode)
  (chown dir uid gid))


(define-public (set-owner-and-mode file uid gid mode)
  (chmod file mode)
  (chown file uid gid))


(define-public (set-owner-and-mode-if-exists file uid gid mode)
  (when (file-exists? file)
    (set-owner-and-mode file uid gid mode)))


(define* (read-from-psql psql #:rest args)
  (let* ((port (open-pipe* OPEN_READ psql "-X" "-t" "-A" "-d" "postgres" "-U" "postgres"
                           "-c" (string-join args " ")))
         (output (get-string-all port)))
    (close-pipe port)
    output))


(define* (read-from-su user command #:key shell)
  (let* ((shell-args (if shell `("-s" ,shell) '()))
         (args `(,OPEN_READ "/run/setuid-programs/su" ,user ,@shell-args "-c" ,command))
         (port (apply open-pipe* args))
         (output (get-string-all port)))
    (close-pipe port)
    output))


(define* (invoke-as-user user command #:rest args)
  (invoke "/run/setuid-programs/sudo" "-n" "-u" user "--" command (string-join args " ")))


(define* (run-psql psql #:rest args)
  (invoke psql "-d" "postgres" "-U" "postgres" "-c" (string-join args " ")))


(define* (postgresql-create-role psql role #:optional password)
  (if password
      (run-psql psql "CREATE ROLE" role "WITH login createdb" "PASSWORD" (format #f "'~a'" password))
      (run-psql psql "CREATE ROLE" role "WITH login createdb")))


(define (postgresql-role-exists? psql role)
  (let* ((command (format #f "SELECT EXISTS (SELECT FROM pg_catalog.pg_roles WHERE rolname = '~a');" role))
         (output (string-delete #\newline (read-from-psql psql command))))
    (equal? output "t")))


(define (postgresql-database-exists? psql database)
  (let* ((command (format #f "SELECT EXISTS (SELECT FROM pg_catalog.pg_database WHERE datname = '~a');" database))
         (output (string-delete #\newline (read-from-psql psql command))))
    (equal? output "t")))


(define-public (flatten . lst)
  "Return a list that recursively concatenates all sub-lists of LST."
  (define (flatten1 head out)
    (if (list? head)
        (fold-right flatten1 out head)
        (cons head out)))
  (fold-right flatten1 '() lst))


(define-public (substitute-regexp-list contents regexp-list)
  (format #t "SUBSTITUTE LIST: ~a~%" regexp-list)
  (fold-right
   (match-lambda*
     (((regexp . replacement) input)
      (regexp-substitute/global #f regexp input 'pre replacement 'post)))
   contents regexp-list))


(define-syntax define-if-set
  (lambda (x)
    (syntax-case x ()
      ((_ condition (name args ...) body ...)
       (identifier? #'name)
       (when (syntax->datum #'condition)
         #'(define-public (name args ...) body ...))))))


(define-public (lookup-proc-mounts local-path)
  (call-with-input-file "/proc/mounts"
    (lambda (port)
      (let ((found #f))
        (do ((line (get-line port)
                   (begin (set! line (get-line port)) line)))
            ((eof-object? line))
          (let* ((items (string-split line #\space)))
            (match items
              ((device path type options _ _)
               (when (equal? path local-path)
                 (format #t "Found mount entry: ~a~%" line)
                 (set! found device)))
              (else (format #t "Invalid entry in /proc/mounts: ~a~%" line)))))
        found))))
