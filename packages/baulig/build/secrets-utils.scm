;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig build secrets-utils)
  #:use-module (ice-9 regex))


;;;
;;; Regex checks
;;;

(define-public (secrets-file-key? value)
  (and (string? value)
       (string-match %name-regex value)))


(define-public (secrets-file-password? value)
  (and (string? value)
       (string-match %password-regex value)))


(define-public %name-regex "^[A-Za-z][A-Za-z0-9]*([\\.-][A-Za-z0-9]+)*$")
(define-public %password-regex "^[A-Za-z0-9$&{}(=*)!~%;:,<.>_]{12,31}$")

(define-public %template-regex "@(password|text|blob|parameter):([A-Za-z0-9\\.-]+)*@")
