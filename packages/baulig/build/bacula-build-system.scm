;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig build bacula-build-system)
  #:use-module ((guix build gnu-build-system) #:prefix gnu:)
  #:use-module ((guix build utils) #:hide (delete))
  #:use-module (srfi srfi-1)
  #:export (%standard-phases
            bacula-build))

(define* (unpack #:key inputs source original #:allow-other-keys)
  ;; The git module contains two subdirectories, "bacula" and "gui".
  ;; We need to patch the standard unpack to only grab the "bacular"
  ;; subdirectory.
  (let* ((bacula-source (string-append source "/bacula"))
         (bash (assoc-ref inputs "bash"))
         (bin-sh (string-append bash "/bin/sh")))
    (apply original (list #:source bacula-source))

    (substitute* "autoconf/Make.common.in"
      (("/bin/sh") bin-sh))))


(define (temp-state-dir outputs)
  (let* ((out (assoc-ref outputs "out")))
    (string-append out "/delete-me")))

(define* (configure #:key build target native-inputs inputs outputs original
                    client-only? out-of-source? (configure-flags '())
                    #:allow-other-keys)
  (format #t "CONFIGURE: ~a - ~a~%" build configure-flags)
  (let* ((out (assoc-ref outputs "out"))
         (s3 (assoc-ref inputs "bacula-libs3"))
         (state-dir (temp-state-dir outputs))
         (new-configure (list
                         (string-append "--prefix=" out)
                         (string-append "--datarootdir=" out "/share")
                         (string-append "--mandir=" out "/share/man")
                         (string-append "--with-pid-dir=" state-dir)
                         (string-append "--with-subsys-dir=" out "/etc/bacula")
                         (string-append "--with-logdir=" state-dir)
                         (string-append "--with-working-dir=" state-dir)
                         (string-append "--with-s3=" s3)
                         (if client-only? "--enable-client-only"
                             (string-append "--with-postgresql="
                                            (assoc-ref inputs "postgresql"))))))
    (format #t "CONFIGURE #1: ~a~%" new-configure)
    (apply original (list #:build build #:native-inputs native-inputs
                          #:inputs inputs #:outputs outputs
                          #:configure-flags (append new-configure configure-flags)
                          #:out-of-source? out-of-source?))))

(define* (post-install #:key outputs #:allow-other-keys)
  (format #t "POST-INSTALL~%")
  (rmdir (temp-state-dir outputs)))


(define* (extend-phase key value alist)
  (let ((old (assq-ref alist key)))
    (assq-set! alist key
               (lambda* (#:rest args)
                 (apply value (append (list #:original old) args))))))


(define %standard-phases
  (let ((standard (modify-phases gnu:%standard-phases
                    (delete 'check)
                    (add-after 'install 'post-install post-install))))
    (extend-phase 'configure configure
                  (extend-phase 'unpack unpack standard))))

(define* (bacula-build #:key inputs (phases %standard-phases)
                       #:allow-other-keys #:rest args)
  "Build Bacula."
  (apply gnu:gnu-build
         #:inputs inputs
         #:phases phases
         args))

;;; bacula-build-system.scm ends here
