;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig build config-utils)
  #:use-module (srfi srfi-1)
  #:use-module (rnrs enums)
  #:export (config-field-name
            config-section
            indent-item
            array-config-section-key-value
            array-config-section-key-value-fragment
            array-config-section-yaml))

(define (make-field-name str sep)
  (string-join
   (string-split (if (string-suffix? "?" str)
                     (string-drop-right str 1)
                     str)
                 #\-) sep))


(define-syntax config-field-name
  (syntax-rules (dash space underscore yaml)
    ((_ dash name)
     (make-field-name (symbol->string name) "-"))
    ((_ space name)
     (make-field-name (symbol->string name) " "))
    ((_ underscore name)
     (make-field-name (symbol->string name) "_"))
    ((_ yaml name)
     (make-field-name (symbol->string name) "_"))))


(define-public (format-section name contents indent format-string)
  (let* ((lines (remove string-null? (string-split contents #\newline)))
         (joined (string-join lines (string-append "\n" indent) 'prefix)))
    (format #f format-string name joined)))

(define-syntax config-section
  (syntax-rules (yaml key-value key-value-fragment header-key-value space-separated)
    ((_ yaml name contents)
     (format-section name contents "  " "~a:~a~%"))
    ((_ key-value name contents)
     (format-section name contents "    " "~a {~a~%}~%"))
    ((_ key-value-fragment contents)
     contents)
    ((_ header-key-value name contents)
     (format-section name contents "" "~%[~a]~a~%"))
    ((_ space-separated name contents)
     (format-section name contents "" "~a ~a~%"))))


(define-public (config-section-key-value name contents)
  (config-section key-value name contents))

(define-public (config-section-key-value-fragment name contents)
  (config-section key-value-fragment contents))

(define-public (config-section-header-key-value name contents)
  (config-section header-key-value name contents))

(define-public (config-section-space-separated name contents)
  (config-section space-separated name contents))

(define-public (config-section-yaml name contents)
  (config-section yaml name contents))

(define (indent-item-lines item first-prefix prefix)
  (let* ((lines (remove string-null? (string-split item #\newline)))
         (first (string-append first-prefix  (car lines)))
         (joined (if (> (length lines) 1)
                     (string-join (drop lines 1) (string-append "\n" prefix) 'prefix)
                     "")))
    (string-append first joined "\n")))

(define-syntax indent-item
  (syntax-rules (yaml key-value)
    ((_ yaml item)
     (indent-item-lines item "- " "  "))
    ((_ key-value item)
     (indent-item-lines item "    " "    "))))


(define (array-config-section-yaml section items)
  (if (null? items)
      ""
      (string-append section ":\n"
                     (string-join
                      (map (lambda (item) (indent-item yaml item)) items)
                      "\n"))))


(define (array-config-section-key-value section items)
  (string-join
   (map (lambda (item)
          (config-section key-value section item))
        items)
   "\n"))


(define (array-config-section-key-value-fragment items)
  (string-join
   (map (lambda (item)
          (config-section key-value-fragment item))
        items)
   "\n"))

