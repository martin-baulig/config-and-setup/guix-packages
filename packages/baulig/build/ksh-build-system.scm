;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig build ksh-build-system)
  #:use-module ((guix build gnu-build-system) #:prefix gnu:)
  #:use-module (guix build utils)
  #:export (%standard-phases
            ksh-build))

(define* (create-etc-profile out)
  (mkdir-p (string-append out "/etc"))
  (call-with-output-file (string-append out "/etc/profile")
    (lambda (p)
      (define source-if-exists
        (lambda (file)
          (format p "if [ -f ~a ]; then~%" file)
          (format p "\t. ~a~%" file)
          (format p "fi~%~%")))
      (source-if-exists "/etc/profile"))))

(define* (setup-etc inputs source-dir out)
  (let ((out-etc (string-append out "/etc"))
        (ed (assoc-ref inputs "ed"))
        (vivid (assoc-ref inputs "vivid")))
    (define map-etc (lambda* (#:rest files)
                      (map (lambda (file)
                             (substitute* file
                               (("/etc") out-etc)))
                           files)))
    (with-directory-excursion source-dir
      (map-etc "src/cmd/ksh93/data/builtins.c"
               "src/cmd/ksh93/data/msg.c"
               "src/cmd/ksh93/sh.1")
      (substitute* (string-append "src/cmd/ksh93/SHOPT.sh")
        (("SYSRC=(\\s+)" all space) (string-append "SYSRC=1" space))))
    (map-etc "ksh.kshrc")
    (substitute* "kshrc.d/10-ps1.ksh"
      (("(typeset vivid=).*" all typeset)
       (string-append typeset vivid "/bin/vivid")))
    (substitute* "kshrc.d/50-history.ksh"
      (("/bin/ed") (string-append ed "/bin/ed")))
    (create-etc-profile out)
    (invoke "cat" (string-append out "/etc/profile"))))

(define* (build #:key root inputs outputs install-etc? #:allow-other-keys)
  (let* ((out (assoc-ref outputs "out"))
         (source-dir (if root (string-append (getcwd) "/" root "/") (getcwd))))
    (when install-etc?
      (setup-etc inputs source-dir out))
    (with-directory-excursion source-dir
      (invoke "sh" "bin/package" "make"))))

(define* (install #:key root outputs install-etc? #:allow-other-keys)
  (let* ((out (assoc-ref outputs "out"))
         (source-dir (if root (string-append (getcwd) "/" root "/") (getcwd))))
    (with-directory-excursion source-dir
      (invoke "sh" "bin/package" "install" out))
    (when install-etc?
      (install-file "ksh.kshrc" (string-append out "/etc"))
      (copy-recursively "kshrc.d" (string-append out "/etc/ksh/kshrc.d")))))

(define %standard-phases
  (modify-phases gnu:%standard-phases
    (delete 'bootstrap)
    (delete 'configure)
    (delete 'check)
    (replace 'build build)
    (replace 'install install)))

(define* (ksh-build #:key inputs (phases %standard-phases)
                    #:allow-other-keys #:rest args)
  "Build the AT&T Korn Shell."
  (apply gnu:gnu-build
         #:inputs inputs
         #:phases phases
         args))

;;; ksh-build-system.scm ends here
