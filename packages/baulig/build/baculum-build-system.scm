;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig build baculum-build-system)
  #:use-module ((guix build gnu-build-system) #:prefix gnu:)
  #:use-module (guix build utils)
  #:export (%standard-phases
            baculum-build))

(define* (build #:key inputs source outputs #:allow-other-keys)
  (let* ((out (assoc-ref outputs "out"))
         (make (string-append (assoc-ref inputs "make") "/bin/make")))
    (invoke make "-C" "gui/baculum" (string-append "DESTDIR=" out))))


(define %standard-phases
  (modify-phases gnu:%standard-phases
    (delete 'check)
    (delete 'configure)
    (replace 'build build)
    (delete 'install)))

(define* (baculum-build #:key inputs (phases %standard-phases)
                       #:allow-other-keys #:rest args)
  "Build Baculum."
  (apply gnu:gnu-build
         #:inputs inputs
         #:phases phases
         args))

;;; baculum-build-system.scm ends here
