;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig build go-module-build-system)
  #:use-module ((guix build gnu-build-system) #:prefix gnu:)
  #:use-module (guix build utils)
  #:export (%standard-phases
            go-module-build))

(define* (go-command inputs)
  (let* ((go (assoc-ref inputs "go"))
         (checked (or go (error "Cannot find go in inputs: ~a" inputs))))
    (string-append checked "/bin/go")))

(define* (build #:key inputs outputs
                environment-variables go-commands
                verbose? debug?
                #:allow-other-keys)
  (let* ((output-path (assoc-ref outputs "out"))
         (source (string-append (getcwd) "/source"))
         (package-name (package-name->name+version
                        (strip-store-file-name output-path)))
         (out (string-append output-path "/bin/"  package-name))
         (cache (string-append (getcwd) "/go/pkg"))
         (go (go-command inputs)))
    (setenv "GOCACHE" cache)
    (setenv "GOMODCACHE" cache)
    (setenv "GOPATH" out)
    (system "pwd")
    (mkdir-p (string-append output-path "/bin"))
    (for-each (lambda (var)
                (setenv (car var) (cdr var)))
              environment-variables)
    (with-directory-excursion source
      (let ((base-args `(,go "build"
                         ,@(if verbose? '("-v") '())
                         ,@(if debug? '("-x") '()))))
        (if (null? go-commands)
            (let ((args `(,@base-args "-o" ,out)))
              (format #t "Building Go Module: ~a~%" base-args)
              (apply invoke args))
            (for-each (lambda (module)
                        (let ((args `(,@base-args
                                      "-o"
                                      ,(string-append output-path "/bin/" (basename module))
                                      ,(string-append "./" module))))
                          (format #t "Building Go Module: ~a - ~a~%" module args)
                          (apply invoke args)))
                      go-commands))))))

(define %standard-phases
  (modify-phases gnu:%standard-phases
    (delete 'bootstrap)
    (delete 'configure)
    (delete 'check)
    (replace 'build build)
    (delete 'install)))

(define* (go-module-build #:key inputs (phases %standard-phases)
                          #:allow-other-keys #:rest args)
  "Go Module Build System"
  (apply gnu:gnu-build
         #:inputs inputs
         #:phases phases
         args))

;;; go-module-build-system.scm ends here
