;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig build secrets-service)
  #:use-module (guix base16)
  #:use-module (baulig build utils)
  #:use-module (baulig build secrets-utils)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 binary-ports)
  #:use-module (ice-9 textual-ports)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-11)
  #:use-module (rnrs enums)
  #:use-module (sqlite3)
  #:use-module (gcrypt hash))


(define %secrets-file "/etc/guix/secrets.db")

(define (sqlite-exec* db sql key value)
  (let ((stmt (sqlite-prepare db sql)))
    (sqlite-bind stmt key value)
    (let ((result (sqlite-map identity stmt)))
      (sqlite-finalize stmt)
      result)))

(define-public (secrets-file-create file)
  (when (file-exists? file)
    (report-error "secrets file '~a' already exists" file))
  (let* ((db (sqlite-open file)))
    (sqlite-exec db "CREATE TABLE secrets (name STRING PRIMARY KEY, value BLOB, type INTEGER)")
    (sqlite-close db)))


(define-record-type <secrets-file>
  (make-secrets-file db)
  secrets-file?
  (db secrets-file-db set-secrets-file-db!))


(define %special-chars "$&{}(=*)!~%;:,<.>_")

(define (integer->alphanumeric-char n)
  "Map N, an integer in the [0..62] range, to an alphanumeric character."
  (cond ((< n 10)
         (integer->char (+ (char->integer #\0) n)))
        ((< n 36)
         (integer->char (+ (char->integer #\A) (- n 10))))
        ((< n 62)
         (integer->char (+ (char->integer #\a) (- n 36))))
        ((< n 80)
         (string-ref %special-chars (- n 62)))
        (else
         (error "integer out of bounds" n))))


(define %random-state
  (seed->random-state (+ (ash (cdr (gettimeofday)) 32) (getpid))))


(define (random-string len)
  "Compute a random string of size LEN where each character is alphanumeric."
  (let loop ((chars '())
             (len len))
    (if (zero? len)
        (list->string chars)
        (let ((n (random 80 %random-state)))
          (loop (cons (integer->alphanumeric-char n) chars)
                (- len 1))))))


(define (generate-password)
  (let ((length (random 17 %random-state)))
    (random-string (+ 14 length))))

;;;
;;; Opening and closing
;;;

(define* (secrets-file-open #:optional file-name)
  (let* ((file (or file-name %secrets-file))
         (flags (logior SQLITE_OPEN_READWRITE SQLITE_OPEN_EXCLUSIVE))
         (db (sqlite-open file flags)))
    (make-secrets-file db)))

(export secrets-file-open)


(define-public secrets-file-close
  (match-lambda
    (($ <secrets-file> db)
     (sqlite-close db))))


;;;
;;; Scan
;;;

(define* (secrets-file-substitute secrets-file template-file output-file
                                  #:key working-directory user group
                                  (external-blob-path #f)
                                  (file-mode #o600)
                                  (parameters '()))
  (match secrets-file
    (($ <secrets-file> db)
     (define (write-blob name contents)
       (let-values (((port get-hash) (open-sha256-port)))
         (put-bytevector port (base16-string->bytevector contents))
         (force-output port)
         (close-port port)
         (let* ((hash (bytevector->base16-string (get-hash)))
                (external-blob-path (assoc-ref parameters 'external-blob-path))
                (path-root (or external-blob-path working-directory))
                (output-file (string-append working-directory "/" hash "-" name))
                (substitute-path (string-append path-root "/" hash "-" name)))
           (format #t "Write blob: ~a -> ~a~%" name output-file)
           (create-file-with-owner output-file user group #o600)
           (call-with-output-file output-file
             (lambda (out)
               (put-bytevector out (base16-string->bytevector contents))))
           substitute-path)))

     (define (substitute type name)
       (format #t "Substituting ~a: ~a~%" type name)

       (unless (secrets-file-key? name)
         (report-error "Invalid secrets file key: ~a" name))

       (define (get-item)
         (secrets-file-get-item secrets-file name
                                #:type type #:throw-on-error #t))

       (define (get-parameter)
         (let ((result (assoc-ref parameters (string->symbol name))))
           (unless result
             (report-error "No such parameter: ~a" name))
           result))

       (case type
         ((password) (get-item))
         ((text) (get-item))
         ((blob)
          (unless (and working-directory user group)
            (report-error "blob requires #:working-directory, #:user and #:group"))
          (write-blob name (get-item)))
         ((parameter) (get-parameter))
         (else
          (report-error "invalid-type"))))

     (format #t "Substituting ~a into ~a.~%" template-file output-file)
     (let* ((input (call-with-input-file template-file get-string-all))
            (output (regexp-substitute/global #f %template-regex input 'pre
                                              (lambda (item)
                                                (let ((type (string->symbol (match:substring item 1)))
                                                      (name (match:substring item 2)))
                                                  (substitute type name)))
                                              'post)))
       (if (file-exists? output-file)
           (delete-file output-file))
       (create-file-with-owner output-file user group file-mode)
       (call-with-output-file output-file
         (lambda (port)
           (put-string port output)
           (force-output port)
           (close-port port)))

       (format #t "Done substituting ~a.~%" output-file)))))

(export secrets-file-substitute)


(define* (secrets-file-substitute/parameters-only template-file output-file parameters
                                                  #:key user group (file-mode #o600))
  (define (substitute type name)
    (format #t "Substituting ~a: ~a~%" type name)

    (define (get-parameter)
      (let ((result (assoc-ref parameters (string->symbol name))))
        (unless result
          (report-error "No such parameter: ~a" name))
        result))

    (case type
      ((password) (report-error "password not allowed in parameters-only mode"))
      ((text) (report-error "text not allowed in parameters-only mode"))
      ((blob) (report-error "blob not allowed in parameters-only mode"))
      ((parameter) (get-parameter))
      (else (report-error "invalid-type"))))

  (format #t "Substituting ~a into ~a.~%" template-file output-file)
  (let* ((input (call-with-input-file template-file get-string-all))
         (output (regexp-substitute/global #f %template-regex input 'pre
                                           (lambda (item)
                                             (let ((type (string->symbol (match:substring item 1)))
                                                   (name (match:substring item 2)))
                                               (substitute type name)))
                                           'post)))
    (if (file-exists? output-file)
        (delete-file output-file))
    (create-file-with-owner output-file user group file-mode)
    (call-with-output-file output-file
      (lambda (port)
        (put-string port output)
        (force-output port)
        (close-port port)))

    (format #t "Done substituting ~a.~%" output-file)))

(export secrets-file-substitute/parameters-only)


(define* (secrets-file-scan secrets-file file-name #:key (verify? #f))
  (match secrets-file
    (($ <secrets-file> db)
     (let* ((input (call-with-input-file file-name get-string-all))
            (verify-okay #t)
            (output (fold-matches %template-regex input '()
                                  (lambda (item prev)
                                    (let* ((type (string->symbol (match:substring item 1)))
                                           (name (match:substring item 2))
                                           (found (assoc-ref prev name)))
                                      (cond ((not (secrets-file-key? name))
                                             (report-error "invalid secrets key: ~a" name))
                                            ((eq? type found)
                                             prev)
                                            (found
                                             (report-error "conflicting entries for ~a" name))
                                            (else
                                             (when verify?
                                               (let ((lookup (secrets-file-get-item secrets-file name
                                                                                    #:type type #:throw-on-error #f)))
                                                 (when (not lookup)
                                                   (format (current-error-port) "Missing ~a entry: ~a~%" type name)
                                                   (set! verify-okay #f))))
                                             (acons name type prev))))))))
       (unless verify-okay
         (report-error "verification failed"))
       (map (lambda (item)
              (list (cdr item) (car item)))
            output)))))

;;;
;;; Querying entries
;;;

(define secrets-file-entry-type
  (make-enumeration '(password text blob)))

(define (number->type number)
  (case number
    ((0) 'password)
    ((1) 'text)
    ((2) 'blob)
    (else (report-error "invalid type"))))

(define (type->number type)
  (case type
    ((password) 0)
    ((text) 1)
    ((blob) 2)
    (else (report-error "invalid type"))))

(define-public secrets-file-list
  (match-lambda
    (($ <secrets-file> db)
     (let ((stmt (sqlite-prepare db "SELECT name, type FROM secrets")))
       (map (lambda (vector)
              (let ((type (number->type (vector-ref vector 1)))
                    (name (vector-ref vector 0)))
                (list type name)))
            (sqlite-map identity stmt))))))


(define* (secrets-file-get-item secrets-file name
                                #:key (type 'password) (throw-on-error #t))
  (unless (secrets-file-key? name)
    (report-error "invalid name: ~a" name))
  (match secrets-file
    (($ <secrets-file> db)
     (let ((stmt (sqlite-prepare db "SELECT value FROM secrets WHERE name = :name AND TYPE = :type")))
       (sqlite-bind stmt 'name name)
       (sqlite-bind stmt 'type (type->number type))
       (let ((result (sqlite-step stmt)))
         (sqlite-finalize stmt)
         (if (vector? result)
             (vector-ref result 0)
             (begin
               (when throw-on-error
                 (report-error "no such key in secrets file: ~a" name))
               #f)))))))

(export secrets-file-get-item)

(define-public (secrets-file-has-entry? secrets-file name)
  (unless (secrets-file-key? name)
    (report-error "invalid name: ~a" name))
  (match secrets-file
    (($ <secrets-file> db)
     (let ((result (sqlite-exec* db "SELECT value FROM secrets WHERE name = :name" 'name name)))
       (> (length result) 0)))))


;;;
;;; Adding entries
;;;

(define* (secrets-file-generate secrets-file name #:key (replace #f))
  (secrets-file-add-item secrets-file name (generate-password)
                         #:type 'password #:replace replace))


(define* (secrets-file-add-item secrets-file name item
                                #:key (type 'password) (replace #f))
  (unless (secrets-file-key? name)
    (report-error "invalid name: ~a" name))
  (unless (or (not (eq? type 'password))
              (secrets-file-password? item))
    (report-error "invalid password"))

  (when (and (not replace) (secrets-file-has-entry? secrets-file name))
    (report-error "entry already exists: ~a" name))

  (match secrets-file
    (($ <secrets-file> db)
     (let ((stmt (sqlite-prepare db "INSERT OR REPLACE INTO secrets (name, value, type) VALUES (?, ?, ?)")))
       (sqlite-bind stmt 1 name)
       (sqlite-bind stmt 2 item)
       (sqlite-bind stmt 3 (type->number type))
       (sqlite-step stmt)
       (sqlite-finalize stmt)))))

(export secrets-file-generate secrets-file-add)


;;;
;;; Removing entries
;;;

(define-public (secrets-file-remove secrets-file name)
  (unless (secrets-file-key? name)
    (report-error "invalid name: ~a" name))

  (match secrets-file
    (($ <secrets-file> db)
     (sqlite-exec* db "DELETE FROM secrets WHERE name = :name" 'name name))))


;;;
;;; Command Actions
;;;

(define (report-error . args)
  (let* ((formatted (apply format #f args))
         (message (format #f "guix secrets: ~a~%" formatted)))
    (display message)
    (display message (current-error-port))
    (exit -1)))


(define (check-secrets-file file-name)
  (unless (file-exists? file-name)
    (report-error "secrets file does not exist: ~a" file-name))
  (unless (access? file-name (logior R_OK W_OK))
    (report-error "insufficient permissions to access secrets file: ~a" file-name)))

(define-syntax with-secrets-file
  (syntax-rules ()
    ((_ file-name func args ...)
     (begin
       (check-secrets-file file-name)
       (let* ((secrets (secrets-file-open file-name))
              (result (func secrets args ...)))
         (secrets-file-close secrets)
         result)
       ))))


(define-public (secrets-file-action-create file-name opts . args)
  (let ((replace #f))
    (for-each
     (match-lambda
       ('force (set! replace #t)))
     opts)

    (match args
      (()
       (when (file-exists? file-name)
         (if force
             (delete-file file-name)
             (report-error "secrets file already exists: ~a" file-name)))
       (secrets-file-create file-name))
      (else (report-error "this command does not take any arguments")))))


(define-public (secrets-file-action-list file-name opts . args)
  (match args
    (()
     (with-secrets-file
      file-name
      (lambda (secrets)
        (map (lambda (name)
               (format #t "~s~%" name))
             (secrets-file-list secrets)))))
    (else (report-error "this command does not take any arguments"))))


(define-public (secrets-file-action-generate file-name opts . args)
  (let ((replace #f))
    (for-each
     (match-lambda
       ('force (set! replace #t)))
     opts)

    (match args
      ((name)
       (with-secrets-file file-name secrets-file-generate name #:replace replace))
      (else (report-error "invalid arguments")))))


(define-public (secrets-file-action-passwd file-name opts . args)
  (let ((replace #f))
    (for-each
     (match-lambda
       ('force (set! replace #t)))
     opts)

    (match args
      ((name)
       (with-secrets-file
        file-name
        (lambda (secrets)
          (let ((password (getpass (format #f "Enter new password for '~a': " name)))
                (repeat (getpass "Repeat password: ")))
            (unless (equal? password repeat)
              (report-error "Passwords do not match."))
            (unless (secrets-file-password? password)
              (report-error "invalid password"))
            (secrets-file-add-item secrets name password #:type 'password #:replace replace)))))
      (else (report-error "invalid arguments")))))


(define-public (secrets-file-action-get file-name opts . args)
  (match args
    ((name)
     (with-secrets-file
      file-name
      (lambda (secrets)
        (display (secrets-file-get-item secrets name))
        (newline))))
    (else (report-error "invalid arguments"))))


(define-public (secrets-file-action-remove file-name opts . args)
  (match args
    ((name)
     (with-secrets-file file-name secrets-file-remove name))
    (else (report-error "invalid arguments"))))


(define-public (secrets-file-action-read file-name opts . args)
  (let ((output-file #f)
        (type 'text))
    (for-each
     (match-lambda
       (('file path) (set! output-file path))
       (('blob path) (begin
                       (set! output-file path)
                       (set! type 'blob))))
     opts)

    (match args
      ((name)
       (let ((output (with-secrets-file file-name secrets-file-get-item name #:type type)))
         (if output-file
             (call-with-output-file output-file
               (lambda (port) (display output port)))
             (display output))))
      (else (report-error "invalid arguments")))))


(define-public (secrets-file-action-write file-name opts . args)
  (let ((replace #f)
        (type 'text)
        (input-file #f))
    (for-each
     (match-lambda
       ('force (set! replace #t))
       (('file path) (set! input-file path))
       (('blob path) (begin
                       (set! input-file path)
                       (set! type 'blob))))
     opts)

    (define (read-input port)
      (if (eq? type 'blob)
          (bytevector->base16-string (get-bytevector-all port))
          (get-string-all port)))

    (match args
      ((name)
       (with-secrets-file
        file-name
        (lambda (secrets)
          (let ((input (if input-file
                           (call-with-input-file input-file read-input)
                           (read-input (current-input-port)))))
            (secrets-file-add-item secrets name input #:type type #:replace replace)))))
      (else (report-error "invalid arguments")))))


(define-public (secrets-file-action-scan file-name opts . args)
  (match args
    ((input-file)
     (let ((output (with-secrets-file file-name secrets-file-scan input-file)))
       (for-each (lambda (item) (format #t "~s~%" item)) output)))
    (else (report-error "invalid arguments"))))


(define-public (secrets-file-action-verify file-name opts . args)
  (match args
    ((input-file)
     (with-secrets-file file-name secrets-file-scan input-file #:verify? #t))
    (else (report-error "invalid arguments"))))
