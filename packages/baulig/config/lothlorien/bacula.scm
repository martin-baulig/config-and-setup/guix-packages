;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config lothlorien bacula)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (guix gexp)
  #:use-module (baulig config bacula)
  #:use-module (baulig config lothlorien bacula console)
  #:use-module (baulig config lothlorien bacula director)
  #:use-module (baulig config lothlorien bacula external)
  #:use-module (baulig config lothlorien bacula storage)
  #:use-module (baulig services account)
  #:use-module (baulig services bacula common)
  #:use-module (baulig services bacula client)
  #:use-module (baulig services bacula database)
  #:re-export (%bacula-master-console-lothlorien
               %bacula-external-console-lothlorien
               %bacula-director-lothlorien
               %bacula-storage-lothlorien-master
               %bacula-storage-lothlorien-gondor
               %bacula-storage-lothlorien-external
               %bacula-storage-lothlorien-s3
               %bacula-external-client-gondor
               %bacula-external-client-gondor-external
               %bacula-external-client-fornost
               %bacula-external-client-amsterdam))


;;;
;;; Bacula Database
;;;

(define-public %bacula-database-lothlorien
  (bacula-database-configuration
   (account
    (account-configuration
     (id 'bacula-database)
     (supplementary-groups '("postgres"))))
   (password-key "bacula-database")))


;;;
;;; Bacula Client
;;;

(define-public %bacula-client-lothlorien
  (bacula-client-configuration
   (messages %default-messages)

   (director
    (list
     (bacula-client-director
      (name "Master-Director")
      (tls %tls-settings-lothlorien)
      (password "bacula-lothlorien"))))

   (client
    (bacula-client-resource
     (name "Lothlorien")
     (fd-address "192.168.8.2")
     (tls %tls-settings-lothlorien)))))

