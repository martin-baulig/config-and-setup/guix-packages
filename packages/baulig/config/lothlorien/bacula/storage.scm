;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config lothlorien bacula storage)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (guix gexp)
  #:use-module (baulig config bacula)
  #:use-module (baulig config common)
  #:use-module (baulig services account)
  #:use-module (baulig services bacula common)
  #:use-module (baulig services bacula storage)
  #:use-module (baulig services bacula tls))


;;;
;;; Master Storage
;;;

(define-public %bacula-storage-lothlorien-master
  (bacula-storage-configuration
   (id 'master)
   (account
    (account-configuration
     (id 'bacula-storage-master)
     (user "bacula-storage-master")
     (group "lothlorien-backup")
     (supplementary-groups '("lothlorien-storage"))
     (create-account? #f)))

   (messages %default-messages)

   (director
    (bacula-storage-director
     (name "Master-Director")
     (tls %tls-settings-lothlorien)
     (password "lothlorien-storage-master")))

   (storage
    (bacula-storage-resource
     (name "Lothlorien")
     (sd-address "192.168.8.2")
     (sd-port 9103)
     (tls-server
      (bacula-tls-server-settings
       (shared %tls-settings-lothlorien)
       (tls-dh-file "lothlorien.dh4096.pem")))))

   (device
    (bacula-device-resource
     (name "Lothlorien-Master")
     (media-type "Lothlorien-Master")
     (archive-device "/Data/Storage/Bacula/Lothlorien-Master")))))


;;;
;;; Gondor Storage
;;;

(define-public %bacula-storage-lothlorien-gondor
  (bacula-storage-configuration
   (id 'gondor)
   (account
    (account-configuration
     (id 'bacula-storage-gondor)
     (user "bacula-storage-gondor")
     (group "lothlorien-backup")
     (supplementary-groups '("lothlorien-storage"))
     (create-account? #f)
     (daemon
      (daemon-account-configuration
       (pid-file "bacula-sd.9104.pid")))))

   (messages %default-messages)

   (director
    (bacula-storage-director
     (name "Master-Director")
     (tls %tls-settings-lothlorien)
     (password "lothlorien-storage-gondor")))

   (storage
    (bacula-storage-resource
     (name "Lothlorien")
     (sd-address "192.168.8.2")
     (sd-port 9104)
     (tls-server
      (bacula-tls-server-settings
       (shared %tls-settings-lothlorien)
       (tls-dh-file "lothlorien.dh4096.pem")))))

   (device
    (bacula-device-resource
     (name "Lothlorien-Gondor")
     (media-type "Lothlorien-Gondor")
     (archive-device "/Data/Storage/Bacula/Lothlorien-Gondor")))))


;;;
;;; S3 Storage
;;;

(define-public %bacula-storage-lothlorien-s3
  (bacula-storage-configuration
   (id 's3)
   (account
    (account-configuration
     (id 'bacula-storage-s3)
     (user "bacula-storage-s3")
     (group "lothlorien-backup")
     (supplementary-groups '("lothlorien-storage"))
     (create-account? #f)
     (daemon
      (daemon-account-configuration
       (pid-file "bacula-sd.9108.pid")))))

   (messages %default-messages)

   (director
    (bacula-storage-director
     (name "Master-Director")
     (tls %tls-settings-lothlorien)
     (password "lothlorien-storage-s3")))

   (storage
    (bacula-storage-resource
     (name "Lothlorien")
     (sd-address "192.168.8.2")
     (sd-port 9108)
     (tls-server
      (bacula-tls-server-settings
       (shared %tls-settings-lothlorien)
       (tls-dh-file "lothlorien.dh4096.pem")))))

   (device
    (bacula-device-resource
     (name "Lothlorien-S3")
     (media-type "Lothlorien-S3")
     (archive-device "/Data/Storage/Bacula/Lothlorien-S3")
     (maximum-volume-size "500g")
     (maximum-part-size "4g")
     (device-type 'cloud)
     (cloud "Lothlorien-S3")))

   (cloud
    (bacula-cloud-resource
     (name "Lothlorien-S3")
     (truncate-cache 'no)
     (upload 'each-part)
     (host-name "ams1.vultrobjects.com")
     (bucket-name "baulig-bacula-lothlorien")
     (access-key "bacula-s3-access-key")
     (secret-key "bacula-s3-secret-key")))))


;;;
;;; External Storage
;;;

(define-public %bacula-storage-lothlorien-external
  (bacula-storage-configuration
   (id 'external)
   (account
    (account-configuration
     (id 'bacula-storage-external)
     (user "bacula-storage-external")
     (group "lothlorien-backup")
     (supplementary-groups '("lothlorien-storage"))
     (create-account? #f)
     (daemon
      (daemon-account-configuration
       (pid-file "bacula-sd.9109.pid")))))

   (messages %default-messages)

   (director
    (bacula-storage-director
     (name "Master-Director")
     (tls %tls-settings-lothlorien-dev)
     (password "lothlorien-storage-external")))

   (storage
    (bacula-storage-resource
     (name "Lothlorien-External")
     (sd-address %lothlorien-vpn-ip-address)
     (sd-port 9109)
     (tls-server
      (bacula-tls-server-settings
       (shared %tls-settings-lothlorien-dev)
       (tls-dh-file "lothlorien-external.dh4096.pem")))))

   (device
    (bacula-device-resource
     (name "Lothlorien-External")
     (media-type "Lothlorien-External")
     (archive-device "/Data/Storage/Bacula/Lothlorien-External")))))
