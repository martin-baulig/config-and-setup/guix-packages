;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config lothlorien bacula director)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (guix gexp)
  #:use-module (baulig config bacula)
  #:use-module (baulig services account)
  #:use-module (baulig services serialization bacula)
  #:use-module (baulig services bacula common)
  #:use-module (baulig services bacula director)
  #:use-module (baulig services bacula pki)
  #:use-module (baulig services bacula tls))


;;;
;;; Master Director
;;;

(define-public %bacula-director-lothlorien
  (bacula-director-configuration
   (id 'master)
   (account
    (account-configuration
     (id 'bacula-director-master)
     (supplementary-groups '("postgres"))))

   (messages (list %default-messages))

   (catalog
    (bacula-catalog-resource
     (name "Lothlorien")
     (db-name "bacula")
     (user "bacula")
     (password "bacula-database")))

   (client
    (list
     (bacula-director-client
      (name "Minas-Tirith")
      (address "minas-tirith.baulig.is")
      (password "bacula-minas-tirith")
      (allow-fd-connections #t)
      (tls %tls-settings-lothlorien)
      (catalog "Lothlorien"))

     (bacula-director-client
      (name "Lothlorien")
      (address "lothlorien.baulig.is")
      (password "bacula-lothlorien")
      (tls %tls-settings-lothlorien)
      (catalog "Lothlorien"))

     (bacula-director-client
      (name "Edoras")
      (address "edoras.baulig.dev")
      (password "bacula-client-edoras-master")
      (tls %tls-settings-lothlorien)
      (catalog "Lothlorien"))

     (bacula-director-client
      (name "Gondor")
      (address "gondor.baulig.is")
      (password "bacula-client-gondor-master")
      (tls %tls-settings-lothlorien)
      (catalog "Lothlorien"))

     (bacula-director-client
      (name "Amsterdam")
      (address "amsterdam.baulig.dev")
      (password "bacula-client-amsterdam-master")
      (tls %tls-settings-lothlorien)
      (catalog "Lothlorien"))

     (bacula-director-client
      (name "Fornost")
      (address "fornost.baulig.dev")
      (password "bacula-client-fornost-master")
      (tls %tls-settings-lothlorien)
      (catalog "Lothlorien"))))

   (console
    (list
     (bacula-director-console
      (name "Master")
      (password "bacula-console-master")
      (job-acl %acl-list-all)
      (catalog-acl %acl-list-all)
      (client-acl %acl-list-all)
      (file-set-acl %acl-list-all)
      (command-acl %acl-list-all)
      (pool-acl %acl-list-all)
      (storage-acl %acl-list-all)
      (tls-server
       (bacula-tls-server-settings
        (shared %tls-settings-lothlorien)
        (tls-dh-file "lothlorien.dh4096.pem"))))

     (bacula-director-console
      (name "External")
      (password "bacula-console-external")
      (job-acl %acl-list-all)
      (catalog-acl %acl-list-all)
      (client-acl %acl-list-all)
      (file-set-acl %acl-list-all)
      (command-acl '("cancel" "list" "llist" "messages" "run"
                     "restart" "resume" "status" "stop" "show"
                     "statistics" "version" "wait"))
      (pool-acl %acl-list-all)
      (storage-acl %acl-list-all)
      (tls-server
       (bacula-tls-server-settings
        (shared %tls-settings-lothlorien)
        (tls-dh-file "lothlorien.dh4096.pem"))))

     (bacula-director-console
      (name "Baculum")
      (password "baculum-console")
      (job-acl %acl-list-all)
      (catalog-acl %acl-list-all)
      (client-acl %acl-list-all)
      (file-set-acl %acl-list-all)
      (command-acl '("autodisplay" "cancel" "estimate" "gui" "list" "llist"
                     "messages" "memory" "query" "run" "restart" "resume"
                     "status" "setdebug" "stop" "show" "statistics"
                     "version" "wait"
                     ".client" ".fileset" ".jobs" ".level" ".messages" ".msgs"
                     ".pool" ".status" ".storage" ".types"))
      (pool-acl %acl-list-all)
      (storage-acl %acl-list-all)
      (tls-server
       (bacula-tls-server-settings
        (shared %tls-settings-lothlorien)
        (tls-dh-file "lothlorien.dh4096.pem"))))))

   (job-defs
    (list
     (bacula-job-defs-resource
      (name "Master-Full")
      (type 'backup)
      (messages "Default")
      (accurate #t)
      (pool "Lothlorien-Master")
      (differential-backup-pool "Lothlorien-Master-Differential")
      (incremental-backup-pool "Lothlorien-Master-Incremental"))

     (bacula-job-defs-resource
      (name "Master-Differential")
      (type 'backup)
      (level 'differential)
      (messages "Default")
      (accurate #t)
      (pool "Lothlorien-Master-Differential")
      (differential-backup-pool "Lothlorien-Master-Differential")
      (incremental-backup-pool "Lothlorien-Master-Incremental"))

     (bacula-job-defs-resource
      (name "Master-Incremental")
      (type 'backup)
      (level 'incremental)
      (messages "Default")
      (accurate #t)
      (pool "Lothlorien-Master-Incremental")
      (differential-backup-pool "Lothlorien-Master-Differential")
      (incremental-backup-pool "Lothlorien-Master-Incremental"))

     (bacula-job-defs-resource
      (name "External-Full")
      (type 'backup)
      (messages "Default")
      (accurate #t)
      (pool "Lothlorien-External")
      (differential-backup-pool "Lothlorien-External-Differential")
      (incremental-backup-pool "Lothlorien-External-Incremental"))

     (bacula-job-defs-resource
      (name "External-Differential")
      (type 'backup)
      (level 'differential)
      (messages "Default")
      (accurate #t)
      (pool "Lothlorien-External-Differential")
      (differential-backup-pool "Lothlorien-External-Differential")
      (incremental-backup-pool "Lothlorien-External-Incremental"))

     (bacula-job-defs-resource
      (name "External-Incremental")
      (type 'backup)
      (level 'incremental)
      (messages "Default")
      (accurate #t)
      (pool "Lothlorien-External-Incremental")
      (differential-backup-pool "Lothlorien-External-Differential")
      (incremental-backup-pool "Lothlorien-External-Incremental"))

     (bacula-job-defs-resource
      (name "Gondor-Full")
      (type 'backup)
      (messages "Default")
      (accurate #t)
      (pool "Lothlorien-Gondor")
      (differential-backup-pool "Lothlorien-Gondor-Differential")
      (incremental-backup-pool "Lothlorien-Gondor-Incremental"))

     (bacula-job-defs-resource
      (name "Gondor-Differential")
      (type 'backup)
      (level 'differential)
      (messages "Default")
      (accurate #t)
      (pool "Lothlorien-Gondor-Differential")
      (differential-backup-pool "Lothlorien-Gondor-Differential")
      (incremental-backup-pool "Lothlorien-Gondor-Incremental"))

     (bacula-job-defs-resource
      (name "Gondor-Incremental")
      (type 'backup)
      (level 'incremental)
      (messages "Default")
      (accurate #t)
      (pool "Lothlorien-Gondor-Incremental")
      (differential-backup-pool "Lothlorien-Gondor-Differential")
      (incremental-backup-pool "Lothlorien-Gondor-Incremental"))

     (bacula-job-defs-resource
      (name "Master-S3")
      (type 'backup)
      (messages "Default")
      (accurate #t)
      (pool "Lothlorien-S3"))))

   (job
    (list
     (bacula-job-resource
      (name "Minas-Tirith")
      (job-defs "Master-Full")
      (client "Minas-Tirith")
      (fileset "Guix-System"))

     (bacula-job-resource
      (name "Minas-Tirith-Differential")
      (job-defs "Master-Differential")
      (client "Minas-Tirith")
      (fileset "Guix-System"))

     (bacula-job-resource
      (name "Minas-Tirith-Incremental")
      (job-defs "Master-Incremental")
      (client "Minas-Tirith")
      (fileset "Guix-System"))

     (bacula-job-resource
      (name "Lothlorien")
      (job-defs "Master-Full")
      (client "Lothlorien")
      (fileset "Guix-System"))

     (bacula-job-resource
      (name "Lothlorien-Differential")
      (job-defs "Master-Differential")
      (client "Lothlorien")
      (fileset "Guix-System"))

     (bacula-job-resource
      (name "Lothlorien-Incremental")
      (job-defs "Master-Incremental")
      (client "Lothlorien")
      (fileset "Guix-System"))

     (bacula-job-resource
      (name "Lothlorien-S3")
      (job-defs "Master-S3")
      (client "Lothlorien")
      (fileset "Guix-System"))

     (bacula-job-resource
      (name "Edoras")
      (job-defs "External-Full")
      (client "Edoras")
      (fileset "Guix-System"))

     (bacula-job-resource
      (name "Edoras-Differential")
      (job-defs "External-Differential")
      (client "Edoras")
      (fileset "Guix-System"))

     (bacula-job-resource
      (name "Edoras-Incremental")
      (job-defs "External-Incremental")
      (client "Edoras")
      (fileset "Guix-System"))

     (bacula-job-resource
      (name "Edoras-Data")
      (job-defs "External-Full")
      (client "Edoras")
      (fileset "Guix-Data"))

     (bacula-job-resource
      (name "Edoras-Data-Differential")
      (job-defs "External-Differential")
      (client "Edoras")
      (fileset "Guix-Data"))

     (bacula-job-resource
      (name "Edoras-Data-Incremental")
      (job-defs "External-Incremental")
      (client "Edoras")
      (fileset "Guix-Data"))

     (bacula-job-resource
      (name "Amsterdam")
      (job-defs "External-Full")
      (client "Amsterdam")
      (fileset "Amsterdam"))

     (bacula-job-resource
      (name "Amsterdam-Differential")
      (job-defs "External-Differential")
      (client "Amsterdam")
      (fileset "Amsterdam"))

     (bacula-job-resource
      (name "Amsterdam-Incremental")
      (job-defs "External-Incremental")
      (client "Amsterdam")
      (fileset "Amsterdam"))

     (bacula-job-resource
      (name "Fornost")
      (job-defs "External-Full")
      (client "Fornost")
      (fileset "Fornost"))

     (bacula-job-resource
      (name "Fornost-Differential")
      (job-defs "External-Differential")
      (client "Fornost")
      (fileset "Fornost"))

     (bacula-job-resource
      (name "Fornost-Incremental")
      (job-defs "External-Incremental")
      (client "Fornost")
      (fileset "Fornost"))

     (bacula-job-resource
      (name "Gondor-Home")
      (job-defs "Gondor-Full")
      (client "Gondor")
      (fileset "Gondor-Home"))

     (bacula-job-resource
      (name "Gondor-Home-Differential")
      (job-defs "Gondor-Differential")
      (client "Gondor")
      (fileset "Gondor-Home"))

     (bacula-job-resource
      (name "Gondor-Home-Incremental")
      (job-defs "Gondor-Incremental")
      (client "Gondor")
      (fileset "Gondor-Home"))

     (bacula-job-resource
      (name "Gondor-Workspace")
      (job-defs "Gondor-Full")
      (client "Gondor")
      (fileset "Gondor-Workspace"))

     (bacula-job-resource
      (name "Gondor-Workspace-Differential")
      (job-defs "Gondor-Differential")
      (client "Gondor")
      (fileset "Gondor-Workspace"))

     (bacula-job-resource
      (name "Gondor-Workspace-Incremental")
      (job-defs "Gondor-Incremental")
      (client "Gondor")
      (fileset "Gondor-Workspace"))

     (bacula-job-resource
      (name "Gondor-Root")
      (job-defs "Gondor-Full")
      (client "Gondor")
      (fileset "Gondor-Root"))

     (bacula-job-resource
      (name "Gondor-Root-Differental")
      (job-defs "Gondor-Differential")
      (client "Gondor")
      (fileset "Gondor-Root"))

     (bacula-job-resource
      (name "Gondor-Root-Incremental")
      (job-defs "Gondor-Incremental")
      (client "Gondor")
      (fileset "Gondor-Root"))

     (bacula-job-resource
      (name "Gondor-Local")
      (job-defs "Gondor-Full")
      (client "Gondor")
      (fileset "Gondor-Local"))

     (bacula-job-resource
      (name "Gondor-Local-Differential")
      (job-defs "Gondor-Differential")
      (client "Gondor")
      (fileset "Gondor-Local"))

     (bacula-job-resource
      (name "Gondor-Local-Incremental")
      (job-defs "Gondor-Incremental")
      (client "Gondor")
      (fileset "Gondor-Local"))

     ;; CAREFUL: Minas-Tirith and Moria must be stopped for this.
     (bacula-job-resource
      (name "Gondor-VM")
      (job-defs "Gondor-Full")
      (client "Gondor")
      (fileset "Gondor-VM"))))

   (pool
    (list
     (bacula-pool-resource
      (name "Lothlorien-Master")
      (storage "Lothlorien-Master")
      (volume-retention "3 years")
      (auto-prune #f)
      (recycle #f)
      (maximum-volume-bytes "100g")
      (maximum-volume-jobs 1))

     (bacula-pool-resource
      (name "Lothlorien-Master-Differential")
      (storage "Lothlorien-Master")
      (volume-retention "3 months")
      (auto-prune #t)
      (recycle #t)
      (maximum-volumes 15)
      (maximum-volume-bytes "100g")
      (maximum-volume-jobs 1))

     (bacula-pool-resource
      (name "Lothlorien-Master-Incremental")
      (storage "Lothlorien-Master")
      (volume-retention "30 minutes")
      (auto-prune #t)
      (recycle #t)
      (maximum-volumes 5)
      (maximum-volume-bytes "100g")
      (maximum-volume-jobs 1))

     (bacula-pool-resource
      (name "Lothlorien-Gondor")
      (storage "Lothlorien-Gondor")
      (volume-retention "3 years")
      (auto-prune #f)
      (recycle #f)
      (maximum-volume-bytes "100g")
      (maximum-volume-jobs 1))

     (bacula-pool-resource
      (name "Lothlorien-Gondor-Differential")
      (storage "Lothlorien-Gondor")
      (volume-retention "3 months")
      (auto-prune #t)
      (recycle #t)
      (maximum-volumes 15)
      (maximum-volume-bytes "100g")
      (maximum-volume-jobs 1))

     (bacula-pool-resource
      (name "Lothlorien-Gondor-Incremental")
      (storage "Lothlorien-Gondor")
      (volume-retention "2 weeks")
      (auto-prune #t)
      (recycle #t)
      (maximum-volumes 15)
      (maximum-volume-bytes "100g")
      (maximum-volume-jobs 1))

     (bacula-pool-resource
      (name "Lothlorien-External")
      (storage "Lothlorien-External")
      (volume-retention "3 years")
      (auto-prune #f)
      (recycle #f)
      (maximum-volume-bytes "100g")
      (maximum-volume-jobs 1))

     (bacula-pool-resource
      (name "Lothlorien-External-Differential")
      (storage "Lothlorien-External")
      (volume-retention "3 months")
      (auto-prune #t)
      (recycle #t)
      (maximum-volumes 15)
      (maximum-volume-bytes "100g")
      (maximum-volume-jobs 1))

     (bacula-pool-resource
      (name "Lothlorien-External-Incremental")
      (storage "Lothlorien-External")
      (volume-retention "2 weeks")
      (auto-prune #t)
      (recycle #t)
      (maximum-volumes 15)
      (maximum-volume-bytes "100g")
      (maximum-volume-jobs 1))

     (bacula-pool-resource
      (name "Lothlorien-S3")
      (storage "Lothlorien-S3")
      (volume-retention "3 years")
      (auto-prune #f)
      (recycle #f)
      (maximum-volume-bytes "100g")
      (maximum-volume-jobs 1))))

   (storage
    (list
     (bacula-director-storage
      (name "Lothlorien-Master")
      (address "lothlorien.baulig.is")
      (sd-port 9103)
      (tls %tls-settings-lothlorien)
      (password "lothlorien-storage-master")
      (device "Lothlorien-Master")
      (media-type "Lothlorien-Master"))

     (bacula-director-storage
      (name "Lothlorien-Gondor")
      (address "lothlorien.baulig.is")
      (sd-port 9104)
      (tls %tls-settings-lothlorien)
      (password "lothlorien-storage-gondor")
      (device "Lothlorien-Gondor")
      (media-type "Lothlorien-Gondor"))

     (bacula-director-storage
      (name "Lothlorien-S3")
      (address "lothlorien.baulig.is")
      (sd-port 9108)
      (tls %tls-settings-lothlorien)
      (password "lothlorien-storage-s3")
      (device "Lothlorien-S3")
      (media-type "Lothlorien-S3"))

     (bacula-director-storage
      (name "Lothlorien-External")
      (address "lothlorien.baulig.dev")
      (sd-port 9109)
      (tls %tls-settings-lothlorien-dev)
      (password "lothlorien-storage-external")
      (device "Lothlorien-External")
      (media-type "Lothlorien-External"))))

   (fileset %bacula-fileset-list)

   (director
    (bacula-director-resource
     (name "Master-Director")
     (tls-server
      (bacula-tls-server-settings
       (shared %tls-settings-lothlorien)
       (tls-dh-file "lothlorien.dh4096.pem")))
     (messages "Default")
     (description "Bacula Master Director")
     (password "bacula-director-master")))))

