;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config lothlorien bacula external)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (baulig config bacula)
  #:use-module (baulig config common)
  #:use-module (baulig services account)
  #:use-module (baulig services bacula common)
  #:use-module (baulig services bacula client)
  #:use-module (baulig services bacula pki)
  #:use-module (baulig services bacula tls))


;;;
;;; Foreign Clients
;;;
;;; These machines are running OpenBSD.
;;;
;;; We only generate the configuration for them here and then copy the entire directory
;;; to the target machine.
;;;
;;; This allows us to have all the Bacula configuration in one place that's under
;;; version control.
;;;

(define-public %bacula-external-client-fornost
  (bacula-client-configuration
   (id 'fornost)
   (foreign? #t)

   (messages %default-messages)

   (director
    (list
     (bacula-client-director
      (name "Master-Director")
      (tls %tls-settings-fornost)
      (password "bacula-client-fornost-master"))
     (bacula-client-director
      (name "Edoras-Director")
      (tls %tls-settings-fornost)
      (password "bacula-client-fornost-external"))))

   (client
    (bacula-client-resource
     (name "Fornost")
     (fd-address %fornost-vpn-ip-address)
     (tls %tls-settings-fornost)
     (pki
      (bacula-pki-settings
       (pki-keypair "bacula-pki-fornost")
       (pki-master-key "bacula-master-key")))))))


(define-public %bacula-external-client-amsterdam
  (bacula-client-configuration
   (id 'amsterdam)
   (foreign? #t)

   (messages %default-messages)

   (director
    (list
     (bacula-client-director
      (name "Master-Director")
      (tls %tls-settings-amsterdam)
      (password "bacula-client-amsterdam-master"))
     (bacula-client-director
      (name "Edoras-Director")
      (tls %tls-settings-amsterdam)
      (password "bacula-client-amsterdam-external"))))

   (client
    (bacula-client-resource
     (name "Amsterdam")
     (fd-address %amsterdam-vpn-ip-address)
     (tls %tls-settings-amsterdam)
     (pki
      (bacula-pki-settings
       (pki-keypair "bacula-pki-amsterdam")
       (pki-master-key "bacula-master-key")))))))


;;;
;;; Gondor (my primary OpenBSD Desktop) is special:
;;;
;;; We create one regular backup to Lothlórien, to have everything saved on the NAS.
;;;
;;; And then, we create an encrypted backup to Edoras, where it's uploaded to S3.
;;;
;;; This requires running two File Daemons on Gondor - and creating some custom
;;; /etc/rc.d script for it.
;;;

(define-public %bacula-external-client-gondor
  (bacula-client-configuration
   (id 'gondor)
   (foreign? #t)

   (messages %default-messages)

   (director
    (list
     (bacula-client-director
      (name "Master-Director")
      (tls %tls-settings-gondor)
      (password "bacula-client-gondor-master"))))

   (client
    (bacula-client-resource
     (name "Gondor")
     (tls %tls-settings-gondor)))))


(define-public %bacula-external-client-gondor-external
  (bacula-client-configuration
   (id 'gondor-external)
   (foreign? #t)

   (messages %default-messages)

   (director
    (list
     (bacula-client-director
      (name "Edoras-Director")
      (tls %tls-settings-gondor)
      (password "bacula-client-gondor-external"))))

   (client
    (bacula-client-resource
     (name "Gondor-External")
     (tls %tls-settings-gondor)
     (pki
      (bacula-pki-settings
       (pki-keypair "bacula-pki-gondor")
       (pki-master-key "bacula-master-key")))))))

