;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config lothlorien nginx)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (baulig services account)
  #:use-module (baulig services nginx)
  #:use-module (baulig services serialization nginx)
  #:use-module (baulig services parameters))


(define %baculum-proxy-location
  (nginx-location "/baculum/"
    (proxy-pass (https #:host "127.0.0.1" #:port 9097 #:path "/")
      ;; Baculum uses libcurl to make requests to the API endpoints
      ;; (https://localhost:9097/api/v2/), using the Basic Auth credentials
      ;; from data/Web/Config/hosts.conf.
      ;;
      ;; We need to secure that endpoint from other applications that might
      ;; be running on the server - without giving Bacula access to any of
      ;; the other web apps (Grafana, Prometheus, etc.).
      ;;
      ;; To do so, we use a different password for Baculum than is used fon
      ;; the main sever - and then explicitly set the Authorization header
      ;; here.
      ;;
      ;; This is computed via
      ;;   echo -n "user:password" | base64
      ;;
      (set-header "Authorization" "\"Basic @text:baculum-authorization@\"")
      (proxy-ssl-name "lothlorien.baulig.dev")
      (proxy-ssl-verify #t)
      (proxy-ssl-trusted-certificate "ca.crt")
      (tls-1.3))))


(define-public %nginx-server-lothlorien
  (nginx-server-configuration
   (id 'lothlorien)
   (server-name "lothlorien.baulig.is")
   (ssl-certificate "baulig.is.crt")
   (ssl-certificate-key "baulig.is.key")
   (root-path "/data/Nginx")
   (basic-auth
    (nginx-basic-auth
     (name "Lothlórien")
     (user "martin")
     (password "lothlorien-web-htpasswd")))
   (location `(,%baculum-proxy-location))))


(define-public %nginx-configuration-lothlorien
  (nginx-configuration
   (error-log
    (nginx-error-log
     (log-level 'notice)))
   (http
    (nginx-http-configuration))))
