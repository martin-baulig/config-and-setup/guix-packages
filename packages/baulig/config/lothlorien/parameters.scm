;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config lothlorien parameters)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (baulig services account)
  #:use-module (baulig services parameters)
  #:use-module (baulig services secrets)
  #:use-module (baulig services bacula console)
  #:use-module (baulig services bacula client)
  #:use-module (baulig services bacula database)
  #:use-module (baulig services bacula director)
  #:use-module (baulig services bacula storage)
  #:use-module (baulig services baculum)
  #:use-module (baulig services nginx)
  #:use-module (baulig services postgresql)
  #:use-module (baulig services vpn)
  #:use-module (baulig config lothlorien bacula)
  #:use-module (baulig config lothlorien baculum)
  #:use-module (baulig config lothlorien nginx)
  #:use-module (baulig config lothlorien postgresql)
  #:use-module (baulig config lothlorien vpn))


(define-public %parameterized-services-lothlorien
  (parameterized-services
   (system-parameters)

   (parameterized-service openvpn-client %openvpn-configuration-lothlorien)

   ;; PostgreSQL
   (parameterized-service postgresql %postgresql-configuration-lothlorien)

   ;; Nginx
   (parameterized-service nginx %nginx-configuration-lothlorien)
   (extend-service nginx %nginx-server-lothlorien)

   ;; Bacula
   (parameterized-service bacula-console %bacula-master-console-lothlorien)

   (parameterized-service bacula-console %bacula-external-console-lothlorien)

   (parameterized-service bacula-client %bacula-client-lothlorien)

   (parameterized-service bacula-storage %bacula-storage-lothlorien-master)

   (parameterized-service bacula-storage %bacula-storage-lothlorien-gondor)

   (parameterized-service bacula-storage %bacula-storage-lothlorien-external)

   (parameterized-service bacula-storage %bacula-storage-lothlorien-s3)

   (parameterized-service bacula-database %bacula-database-lothlorien)

   (parameterized-service bacula-director %bacula-director-lothlorien)

   ;; Baculum
   (parameterized-service baculum %baculum-lothlorien)

   ;; Foreign Bacula Clients
   (parameterized-service bacula-client %bacula-external-client-gondor)

   (parameterized-service bacula-client %bacula-external-client-gondor-external)

   (parameterized-service bacula-client %bacula-external-client-fornost)

   (parameterized-service bacula-client %bacula-external-client-amsterdam)))
