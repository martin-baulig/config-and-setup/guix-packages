;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config lothlorien postgresql)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (gnu packages databases)
  #:use-module (baulig services account)
  #:use-module (baulig services postgresql)
  #:use-module (baulig services serialization postgresql))


(define-public %postgresql-configuration-lothlorien
  (postgresql-service
   ;; Baculum is not compatible with PostgreSQL 15 yet.
   (postgresql postgresql-14)
   ;; /Data/Lothlorien is an NFS mounted encrypted folder from the NAS.
   ;; Set data directory to a subdirectory inside it to store all data
   ;; directly on the NAS and not inside this VM.
   ;;
   (data-directory "/Data/Storage/PostgreSQL-14")
   ;; Root squash is enabled; and we only want to manually create the
   ;; database anyways, so as to not accidentally overwrite things.
   (initialize-database? #f)

   (account
    (account-configuration
     (id 'postgres)
     (user "postgres")
     (group "postgres")
     (supplementary-groups '("lothlorien-storage" "lothlorien-postgres"))
     (create-account? #f)
     (daemon
      (daemon-account-configuration
       (pid-directory "/var/run/postgresql")))))

   (hba-file (plain-file "pg_hba.conf"
                         "\
local     all           all           peer              map=lothlorien
local     replication   postgres      peer              map=lothlorien
hostssl   bacula        bacula        127.0.0.1/32      scram-sha-256
hostssl   bacula        baculum       127.0.0.1/32      scram-sha-256
"))


   (ident-file
    (plain-file "pg_ident.conf"
                "\
lothlorien    root                      postgres
lothlorien    postgres                  postgres
lothlorien    root                      bacula
lothlorien    bacula-director-master    bacula
lothlorien    bacula-database           bacula
"))

   (configuration
    (postgresql-configuration
     (listen-addresses '("127.0.0.1" "192.168.8.2"))
     (tls (postgresql-tls-configuration
           (ssl-ca-file "ca.crt")
           (ssl-cert-file "lothlorien.baulig.is.crt")
           (ssl-key-file "lothlorien.baulig.is.key")
           (ssl-dh-params-file "lothlorien.dh4096.pem")))
     (log-destination 'syslog)
     (log-min-messages 'info)
     (wal-level 'replica)
     (wal-compression #t)
     (wal-recycle #f)
     (archive-mode 'always)
     (archive-command
      (postgresql-archive-command
       (archive-directory "/Data/Storage/PostgreSQL-Backup/WAL-Archive")))))))
