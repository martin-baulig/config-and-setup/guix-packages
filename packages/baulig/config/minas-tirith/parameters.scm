;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config minas-tirith parameters)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (baulig services account)
  #:use-module (baulig services parameters)
  #:use-module (baulig services secrets)
  #:use-module (baulig services bacula console)
  #:use-module (baulig services bacula client)
  #:use-module (baulig services bacula database)
  #:use-module (baulig services baculum)
  #:use-module (baulig services nginx)
  #:use-module (baulig services postgresql)
  #:use-module (baulig services vpn)
  #:use-module (baulig config minas-tirith bacula)
  #:use-module (baulig config minas-tirith nginx)
  #:use-module (baulig config minas-tirith postgresql)
  #:use-module (baulig config minas-tirith vpn))


(define-public %parameterized-services-minas-tirith
  (parameterized-services
   (system-parameters)

   (parameterized-service openvpn-client %openvpn-configuration-minas-tirith)

   (parameterized-service nginx %nginx-configuration-minas-tirith)
   (extend-service nginx %nginx-server-minas-tirith)
   (extend-service nginx %nginx-server-minas-tirith2)

   (parameterized-service postgresql %postgresql-configuration-minas-tirith)

   (parameterized-service bacula-console %bacula-console-minas-tirith)
   (parameterized-service bacula-client %bacula-client-minas-tirith)))
