;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config minas-tirith postgresql)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (gnu packages databases)
  #:use-module (baulig services account)
  #:use-module (baulig services postgresql))


(define-public %postgresql-configuration-minas-tirith
  (postgresql-service
   (data-directory "/data/postgres")

   (account
    (account-configuration
     (id 'postgres)
     (user "postgres")
     (group "postgres")
     (daemon
      (daemon-account-configuration
       (pid-directory "/var/run/postgresql")))))

   (hba-file (plain-file "pg_hba.conf"
                         "\
local     all       all       peer              map=minas-tirith "))


   (ident-file
    (plain-file "pg_ident.conf"
                "\
minas-tirith    root                      postgres
"))

   (configuration
    (postgresql-configuration
     (listen-addresses '("127.0.0.1" "192.168.8.5"))
     (tls (postgresql-tls-configuration
           (ssl-ca-file "ca.crt")
           (ssl-cert-file "minas-tirith.baulig.is.crt")
           (ssl-key-file "minas-tirith.baulig.is.key")
           (ssl-dh-params-file "minas-tirith.dh4096.pem")))))))
