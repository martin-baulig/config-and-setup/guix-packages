;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config minas-tirith bacula)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (guix gexp)
  #:use-module (baulig config bacula)
  #:use-module (baulig services account)
  #:use-module (baulig services bacula common)
  #:use-module (baulig services bacula console)
  #:use-module (baulig services bacula client)
  #:use-module (baulig services bacula tls))


;;;
;;; Bacula Client
;;;

(define-public %bacula-client-minas-tirith
  (bacula-client-configuration
   (messages %default-messages)

   (director
    (list
     (bacula-client-director
      (name "Master-Director")
      (tls %tls-settings-minas-tirith)
      (password "bacula-minas-tirith"))))

   (client
    (bacula-client-resource
     (name "Minas-Tirith")
     (fd-address "192.168.8.5")
     (tls %tls-settings-minas-tirith)))))


;;;
;;; Bacula Console
;;;

(define-public %bacula-console-minas-tirith
  (bacula-console-configuration
   (id 'external)

   (director
    (bacula-console-director
     (name "Master-Director")
     (address "lothlorien.baulig.is")
     (tls %tls-settings-minas-tirith)
     (password "bacula-director-master")))

   (console
    (bacula-console-resource
     (name "External")
     (tls %tls-settings-minas-tirith)
     (director "Master-Director")
     (password "bacula-console-external")))))
