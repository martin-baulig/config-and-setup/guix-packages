;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config minas-tirith nginx)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (baulig services account)
  #:use-module (baulig services nginx)
  #:use-module (baulig services php-fpm)
  #:use-module (baulig services parameters))


(define-public %nginx-configuration-minas-tirith
  (nginx-configuration
   (error-log
    (nginx-error-log
     (log-level 'debug)))
   (http
    (nginx-http-configuration))))


(define-public %nginx-server-minas-tirith
  (nginx-server-configuration
   (server-name "minas-tirith.baulig.is")
   (ssl-certificate "minas-tirith.baulig.is.crt")
   (ssl-certificate-key "minas-tirith.baulig.is.key")
   (root-path "/home/www-main")
   (php-fpm (php-fpm-configuration))))


(define-public %nginx-server-minas-tirith2
  (nginx-server-configuration
   (listen 4443)
   (server-name "minas-tirith.baulig.is")
   (ssl-certificate "minas-tirith.baulig.is.crt")
   (ssl-certificate-key "minas-tirith.baulig.is.key")
   (root-path "/home/www-test")
   (php-fpm (php-fpm-configuration
             (id 'test)))))
