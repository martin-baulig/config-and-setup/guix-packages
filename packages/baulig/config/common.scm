;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config common)
  #:use-module (gnu)
  #:use-module (gnu services admin))


(define-public %osgiliath-vpn-ip-address "10.8.0.1")
(define-public %lothlorien-vpn-ip-address "10.8.0.2")
(define-public %gondor-vpn-ip-address "10.8.0.3")
(define-public %arnor-vpn-ip-address "10.8.0.5")
(define-public %amsterdam-vpn-ip-address "10.8.0.8")
(define-public %fornost-vpn-ip-address "10.8.0.9")
(define-public %edoras-vpn-ip-address "10.8.0.10")
(define-public %pelargir-vpn-ip-address "10.8.0.11")

(define-public %hosts-file
  (plain-file "hosts"
              "\
127.0.0.1 localhost
45.32.167.230 edoras edoras.baulig.is
149.28.98.140 pelargir pelargir.baulig.is
10.8.0.1 osgiliath.baulig.dev
10.8.0.2 lothlorien.baulig.dev
10.8.0.3 gondor.baulig.dev
10.8.0.4 rohan.baulig.dev
10.8.0.5 arnor.baulig.dev
10.8.0.6 minas-tirith.baulig.dev
10.8.0.7 ithilien.baulig.dev
10.8.0.8 amsterdam.baulig.dev
10.8.0.9 fornost.baulig.dev
10.8.0.10 edoras.baulig.dev
10.8.0.11 pelargir.baulig.dev
"))

(define-public %log-rotations
  `(,(log-rotation
      (files '("/var/log/syslog" "/var/log/shepherd" "/var/log/services/*"))
      (options '("rotate 16"
                 "notifempty"
                 "storefile @FILENAME.@COMP_EXT"
                 "storedir log-archive")))))
