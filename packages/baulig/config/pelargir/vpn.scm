;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config pelargir vpn)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (baulig services account)
  #:use-module (baulig services vpn))

(define-public %openvpn-configuration-pelargir
  (openvpn-client-configuration
   (ca "ca.crt")
   (cert "pelargir.baulig.dev.crt")
   (key "pelargir.baulig.dev.key")
   (tls-auth "openvpn-client-tls-auth")
   (remote (list (openvpn-remote-configuration
                  (name "osgiliath.baulig.is"))))))
