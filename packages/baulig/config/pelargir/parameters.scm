;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config pelargir parameters)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (baulig services account)
  #:use-module (baulig services parameters)
  #:use-module (baulig services secrets)
  #:use-module (baulig services bacula client)
  #:use-module (baulig services nginx)
  #:use-module (baulig services vpn)
  #:use-module (baulig config pelargir bacula)
  #:use-module (baulig config pelargir nginx)
  #:use-module (baulig config pelargir vpn))


(define-public %parameterized-services-pelargir
  (parameterized-services
   (system-parameters
    (service-root "/var/services")
    ;; The /home partition is on a Block Storage device that can be
    ;; moved to another instance.  This allows the system to be
    ;; reinstalled / moved to another instance type / size easily.
    (secrets-database "/home/private/secrets.db"))

   ;; OpenVPN
   (parameterized-service openvpn-client %openvpn-configuration-pelargir)

   ;; Bacula
   (parameterized-service bacula-client %bacula-client-pelargir)

   ;; Nginx
   (parameterized-service nginx %nginx-configuration-pelargir)
   (extend-service nginx %nginx-server-pelargir)))
