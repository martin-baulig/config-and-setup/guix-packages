;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config pelargir nginx)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (baulig services account)
  #:use-module (baulig services nginx)
  #:use-module (baulig services serialization nginx)
  #:use-module (baulig config common)
  #:use-module (web uri))


(define-public %nginx-server-pelargir
  (nginx-server-configuration
   (id 'pelargir)
   (server-name "pelargir.baulig.is")
   (ssl-certificate "baulig.is.crt")
   (ssl-certificate-key "baulig.is.key")
   (root-path "/data/Nginx")
   (basic-auth
    (nginx-basic-auth
     (name "Pelargir")
     (user "martin")
     (password "pelargir-web-htpasswd")))))


(define-public %nginx-configuration-pelargir
  (nginx-configuration
   (error-log
    (nginx-error-log
     (log-level 'warn)))
   (http
    (nginx-http-configuration))))
