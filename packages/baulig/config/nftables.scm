;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config nftables)
  #:use-module (gnu))

(define-public %baulig-nftables-ruleset
  (plain-file "nftables.conf"
              "
flush ruleset

define gondor = 192.168.8.3
define int_net = 192.168.8.0/24
define vpn_net = 10.8.0.0/16

table inet firewall {
    chain global {
        # Allow traffic from established and related packets, drop invalid
        ct state vmap { established : accept, related : accept, invalid : drop }

        # Allow loopback traffic.
        iifname lo accept

        # Accept ping with rate limit.
        icmp type echo-request limit rate 5/second accept
    }

    chain internal {
        # Traffic originating from the internal network.

        # Allow SSH, HTTP and HTTPS
        tcp dport { 22, 80, 443} accept

        # Allow Bacula
        tcp dport 9101-9108 accept

	# Allow Baculum
	tcp dport 9097 accept
    }

    chain vpn {
        # Traffic originating from the VPN network.

        # Only allow HTTPS, but not SSH
        tcp dport 443 accept

        # External Bacula Storage Daemon
        tcp dport 9109 accept
    }

    chain inbound {
        type filter hook input priority 0; policy drop;
        jump global

        # Allow all traffic from Gondor.
        ip saddr $gondor accept

        # Local Network
        ip saddr $int_net jump internal

        # VPN Network
        ip saddr $vpn_net jump vpn

        # Log denied packets.
        log prefix \"[nftables] Inbound Denied: \" counter drop
    }

    chain forward {
        # Drop everything.
        type filter hook forward priority 0; policy drop;
    }

    # no need to define output chain, default policy is accept if undefined.
}
"))
