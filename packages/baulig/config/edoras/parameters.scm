;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config edoras parameters)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (baulig services account)
  #:use-module (baulig services parameters)
  #:use-module (baulig services secrets)
  #:use-module (baulig services bacula console)
  #:use-module (baulig services bacula client)
  #:use-module (baulig services bacula database)
  #:use-module (baulig services bacula director)
  #:use-module (baulig services bacula storage)
  #:use-module (baulig services baculum)
  #:use-module (baulig services grafana)
  #:use-module (baulig services loki)
  #:use-module (baulig services nginx)
  #:use-module (baulig services postgresql)
  #:use-module (baulig services prometheus)
  #:use-module (baulig services promtail)
  #:use-module (baulig services vpn)
  #:use-module (baulig config edoras bacula)
  #:use-module (baulig config edoras baculum)
  #:use-module (baulig config edoras loki)
  #:use-module (baulig config edoras nginx)
  #:use-module (baulig config edoras grafana)
  #:use-module (baulig config edoras postgresql)
  #:use-module (baulig config edoras prometheus)
  #:use-module (baulig config edoras promtail)
  #:use-module (baulig config edoras vpn))


(define-public %parameterized-services-edoras
  (parameterized-services
   (system-parameters
    (service-root "/var/services")
    ;; The /home partition is on a Block Storage device that can be
    ;; moved to another instance.  This allows the system to be
    ;; reinstalled / moved to another instance type / size easily.
    (secrets-database "/home/private/secrets.db"))

   ;; OpenVPN
   (parameterized-service openvpn-client %openvpn-configuration-edoras)

   ;; PostgreSQL
   (parameterized-service postgresql %postgresql-configuration-edoras)

   ;; Nginx
   (parameterized-service nginx %nginx-configuration-edoras)
   (extend-service nginx %nginx-server-edoras)

   ;; Baculum
   (parameterized-service baculum %baculum-edoras)

   ;; Loki
   (parameterized-service loki %loki-edoras)

   ;; Promtail
   (parameterized-service promtail %promtail-edoras)

   ;; Prometheus
   (parameterized-service prometheus %prometheus-edoras)

   ;; Grafana
   (parameterized-service grafana %grafana-edoras)

   ;; Bacula
   (parameterized-service bacula-storage %bacula-storage-edoras-full)
   (parameterized-service bacula-storage %bacula-storage-edoras-differential)
   (parameterized-service bacula-storage %bacula-storage-edoras-incremental)

   (parameterized-service bacula-database %bacula-database-edoras)

   (parameterized-service bacula-director %bacula-director-edoras)

   (parameterized-service bacula-console %bacula-console-edoras)

   (parameterized-service bacula-client %bacula-client-edoras)))
