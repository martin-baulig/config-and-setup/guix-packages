;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config edoras promtail)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (baulig config common)
  #:use-module (baulig services promtail)
  #:use-module (baulig services serialization loki)
  #:use-module (baulig services serialization prometheus))


(define (make-static-config name)
  (prometheus-static-configuration
   (labels
    (prometheus-static-label
     (path (format #f "/var/log/services/~a.log" name))
     (extra-labels `((job . ,name)
                     (host . "edoras")))))))


(define (make-bacula-job-config level)
  (prometheus-static-configuration
   (labels
    (prometheus-static-label
     (path (format #f "/var/log/bacula/bacula-~a-jobs.log" level))
     (extra-labels `((job . "bacula")
                     (bacula-level . ,level)
                     (host . "edoras")))))))


(define-public %promtail-edoras
  (promtail-configuration
   (clients
    (list
     (promtail-client-configuration
      (url (loki-push-uri https "edoras.baulig.dev"))
      (tls-config
       (prometheus-tls-config
        (ca-file "ca.crt")
        (server-name "edoras.baulig.dev")
        (cert-file "edoras.baulig.dev.crt")
        (key-file "edoras.baulig.dev.key"))))))

   (scrape-configs
    (list
     (prometheus-scrape-configuration
      (job-name "static")
      (static-configs `(,@(map make-static-config
                               '("bacula-client" "bacula-director"
                                 "bacula-storage-full" "bacula-storage-differential"
                                 "bacula-storage-incremental"
                                 "nginx-access" "nginx-error" "grafana" "loki" "postgres"
                                 "openvpn" "prometheus" "promtail"))
                        ,@(map make-bacula-job-config
                               '("full" "differential" "incremental")))))
     (prometheus-scrape-configuration
      (job-name "syslog")
      (syslog (prometheus-syslog-configuration
               (listen-address "127.0.0.1:1514")
               (idle-timeout (prometheus-duration 12 hours))
               (label-structured-data #t)
               (labels '((job . "syslog")))))
      (relabel-configs (list
                        (prometheus-relabel-configuration
                         (source-labels '("__syslog_message_hostname"))
                         (target-label "host"))
                        (prometheus-relabel-configuration
                         (source-labels '("__syslog_message_severity"))
                         (target-label "level"))
                        (prometheus-relabel-configuration
                         (source-labels '("__syslog_message_facility"))
                         (target-label "facility"))
                        (prometheus-relabel-configuration
                         (source-labels '("__syslog_message_app_name"))
                         (target-label "application"))
                        (prometheus-relabel-configuration
                         (source-labels '("__syslog_connection_hostname"))
                         (target-label "connection")))))))))

