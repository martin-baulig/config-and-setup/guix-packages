;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config edoras loki)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (baulig config common)
  #:use-module (baulig services account)
  #:use-module (baulig services loki)
  #:use-module (baulig services serialization loki)
  #:use-module (baulig build config-utils)
  #:use-module (web uri))


;;;
;;; Loki
;;;

(define-public %loki-edoras
  (loki-configuration
   (data-directory "/storage/Loki")
   (server
    (loki-server-configuration
     (http-listen-address %edoras-vpn-ip-address)
     (http-tls-config
      (loki-tls-server-config
       (cert-file "edoras.baulig.dev.crt")
       (key-file "edoras.baulig.dev.key")
       (client-auth-type 'require-and-verify-client-cert)
       (client-ca-file "ca.crt")))))
   (limits
    (loki-limits-configuration
     (ingestion-rate-mb 16)
     (ingestion-burst-size-mb 64)))))
