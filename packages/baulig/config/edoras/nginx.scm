;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config edoras nginx)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (baulig services account)
  #:use-module (baulig services nginx)
  #:use-module (baulig services serialization nginx)
  #:use-module (baulig config common)
  #:use-module (web uri))


(define %grafana-proxy-location
  (nginx-location "/grafana/"
    (proxy-pass (http #:host "grafana")
      (set-header "Host" "$http_host")
      (remove-header "Authorization"))))


(define %grafana-live-proxy-location
  (nginx-location "/grafana/api/live/"
    (proxy-pass (http #:host "grafana")
      (http-version-1.1)
      (set-header "Upgrade" "$http_upgrade")
      (set-header "Connection" "$connection_upgrade")
      (set-header "Host" "$http_host")
      (remove-header "Authorization"))))


(define %grafana-proxy-upstream
  (nginx-upstream-section
   (path "grafana")
   (unix-socket-group "grafana-socket")
   (configuration
    (nginx-upstream-configuration
     (server (build-uri 'unix #:path "/var/run/services/grafana/grafana.socket"))))))


(define %prometheus-proxy-location
  (nginx-location "/prometheus/"
    (proxy-pass (https #:host %edoras-vpn-ip-address #:port 9090)
      (proxy-ssl-verify #t)
      (proxy-ssl-name "edoras.baulig.dev")
      (proxy-ssl-trusted-certificate "ca.crt")
      (set-header "Host" "$http_host"))))


(define %baculum-proxy-location
  (nginx-location "/baculum/"
    (proxy-pass (https #:host "127.0.0.1" #:port 9097 #:path "/")
      ;; Baculum uses libcurl to make requests to the API endpoints
      ;; (https://localhost:9097/api/v2/), using the Basic Auth credentials
      ;; from data/Web/Config/hosts.conf.
      ;;
      ;; We need to secure that endpoint from other applications that might
      ;; be running on the server - without giving Bacula access to any of
      ;; the other web apps (Grafana, Prometheus, etc.).
      ;;
      ;; To do so, we use a different password for Baculum than is used fon
      ;; the main sever - and then explicitly set the Authorization header
      ;; here.
      ;;
      ;; This is computed via
      ;;   echo -n "user:password" | base64
      ;;
      (set-header "Authorization" "\"Basic @text:baculum-authorization@\"")
      (proxy-ssl-name "edoras.baulig.dev")
      (proxy-ssl-verify #t)
      (proxy-ssl-trusted-certificate "ca.crt")
      (tls-1.3))))


(define %loki-proxy-location
  (nginx-location "/loki/"
    (proxy-pass (https #:host %edoras-vpn-ip-address #:port 3100 #:path "/")
      (proxy-ssl-certificate "edoras.baulig.dev.crt")
      (proxy-ssl-certificate-key "edoras.baulig.dev.key")
      (proxy-ssl-name "edoras.baulig.dev")
      (proxy-ssl-verify #t)
      (proxy-ssl-trusted-certificate "ca.crt")
      (tls-1.3))))


(define-public %nginx-server-edoras
  (nginx-server-configuration
   (id 'edoras)
   (server-name "edoras.baulig.is")
   (ssl-certificate "baulig.is.crt")
   (ssl-certificate-key "baulig.is.key")
   (root-path "/data/Nginx")
   (basic-auth
    (nginx-basic-auth
     (name "Edoras")
     (user "martin")
     (password "edoras-web-htpasswd")))
   (location `(,%grafana-proxy-location
               ,%grafana-live-proxy-location
               ,%baculum-proxy-location
               ,%loki-proxy-location
               ,%prometheus-proxy-location))))


(define-public %nginx-configuration-edoras
  (nginx-configuration
   (error-log
    (nginx-error-log
     (log-level 'warn)))
   (http
    (nginx-http-configuration
     (map-connection-upgrade #t)
     (upstream `(,%grafana-proxy-upstream))))))
