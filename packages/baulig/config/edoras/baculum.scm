;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config edoras baculum)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (guix gexp)
  #:use-module (baulig config bacula)
  #:use-module (baulig config common)
  #:use-module (baulig config edoras bacula)
  #:use-module (baulig services account)
  #:use-module (baulig services baculum)
  #:use-module (baulig services serialization nginx)
  #:use-module (web uri))


;;;
;;; Baculum
;;;

(define-public %baculum-edoras
  (baculum-configuration
   (listen
    (nginx-listen-address
     (port 9097)
     (host "127.0.0.1")))
   (root-url (build-uri 'https #:host "edoras.baulig.is" #:path "/baculum"))
   (server-name "edoras.baulig.dev")
   (ssl-certificate "edoras.baulig.dev.crt")
   (ssl-certificate-key "edoras.baulig.dev.key")
   (web-user "martin")
   (database-user "baculum")
   (database-password "baculum-database-password")
   (database-ip-address "127.0.0.1")
   (database-port 5432)
   (console %baculum-console-edoras)))
