;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config edoras bacula storage)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (baulig config bacula)
  #:use-module (baulig config common)
  #:use-module (baulig services account)
  #:use-module (baulig services bacula common)
  #:use-module (baulig services bacula storage)
  #:use-module (baulig services bacula tls))


;;;
;;; Bakula S3 Storage
;;;
;;; We use different Buckets for Full, Differential and Incremental backups,
;;; so we can recycle the incremental pools every couple of months.
;;;

(define* (make-bacula-storage-edoras #:key id name user group port password bucket)
  (bacula-storage-configuration
   (messages %default-messages)

   (id id)

   (account
    (account-configuration
     (id (string->symbol user))
     (user user)
     (group group)
     (create-account? #f)))

   (director
    (bacula-storage-director
     (name "Edoras-Director")
     (tls %tls-settings-edoras)
     (password password)))

   (storage
    (bacula-storage-resource
     (name name)
     (sd-address %edoras-vpn-ip-address)
     (sd-port port)
     (tls-server
      (bacula-tls-server-settings
       (shared %tls-settings-edoras)
       (tls-dh-file "edoras.dh4096.pem")))))

   (device
    (bacula-device-resource
     (name name)
     (media-type name)
     (archive-device (string-append "/data/Bacula/" name))
     (maximum-volume-size "500g")
     (maximum-part-size "4g")
     (device-type 'cloud)
     (cloud name)))

   (cloud
    (bacula-cloud-resource
     (name name)
     ;; We have limited local disk space, so we upload each part and then delete it.
     (truncate-cache 'after-upload)
     (upload 'each-part)
     (host-name "ams1.vultrobjects.com")
     (bucket-name bucket)
     (access-key "bacula-s3-access-key")
     (secret-key "bacula-s3-secret-key")))))


(define-public %bacula-storage-edoras-full
  (make-bacula-storage-edoras
   #:id 'full
   #:name "Edoras-S3"
   #:user "bacula-storage-full"
   #:group "bacula-storage"
   #:port 9103
   #:password "bacula-storage-full"
   #:bucket "baulig-bacula-edoras-full"))


(define-public %bacula-storage-edoras-differential
  (make-bacula-storage-edoras
   #:id 'differential
   #:name "Edoras-Differential"
   #:user "bacula-storage-differential"
   #:group "bacula-storage"
   #:port 9104
   #:password "bacula-storage-differential"
   #:bucket "baulig-bacula-edoras-differential"))


(define-public %bacula-storage-edoras-incremental
  (make-bacula-storage-edoras
   #:id 'incremental
   #:name "Edoras-Incremental"
   #:user "bacula-storage-incremental"
   #:group "bacula-storage"
   #:port 9105
   #:password "bacula-storage-incremental"
   #:bucket "baulig-bacula-edoras-incremental"))
