;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config edoras bacula director)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (baulig config bacula)
  #:use-module (baulig config common)
  #:use-module (baulig services account)
  #:use-module (baulig services serialization bacula)
  #:use-module (baulig services bacula common)
  #:use-module (baulig services bacula director)
  #:use-module (baulig services bacula tls))


;;;
;;; Edoras Director
;;;

(define-public %bacula-director-edoras
  (bacula-director-configuration
   (id 'edoras)

   (account
    (account-configuration
     (id 'bacula-director)
     (supplementary-groups '("postgres-socket" "syslog"))))

   (messages
    (list
     %default-messages
     %full-job-messages
     %differential-job-messages
     %incremental-job-messages))

   (catalog
    (bacula-catalog-resource
     (name "Edoras")
     (db-name "bacula")
     (user "bacula")
     (password "bacula-database")))

   (client
    (list
     (bacula-director-client
      (name "Edoras")
      (address "edoras.baulig.dev")
      (password "bacula-client-edoras")
      (tls %tls-settings-edoras)
      (catalog "Edoras"))

     (bacula-director-client
      (name "Pelargir")
      (address "pelargir.baulig.dev")
      (password "bacula-client-pelargir")
      (tls %tls-settings-edoras)
      (catalog "Edoras"))

     (bacula-director-client
      (name "Amsterdam")
      (address "amsterdam.baulig.dev")
      (password "bacula-client-amsterdam")
      (tls %tls-settings-edoras)
      (catalog "Edoras"))

     (bacula-director-client
      (name "Fornost")
      (address "fornost.baulig.dev")
      (password "bacula-client-fornost")
      (tls %tls-settings-edoras)
      (catalog "Edoras"))

     (bacula-director-client
      (name "Arnor")
      (address "arnor.baulig.dev")
      (password "bacula-client-arnor")
      (tls %tls-settings-arnor)
      (catalog "Edoras"))

     (bacula-director-client
      (name "Gondor")
      (address "gondor.baulig.dev")
      (fd-port 9109)
      (password "bacula-client-gondor")
      (tls %tls-settings-edoras)
      (catalog "Edoras"))))

   (console
    (list
     (bacula-director-console
      (name "Edoras")
      (password "bacula-console-edoras")
      (job-acl %acl-list-all)
      (catalog-acl %acl-list-all)
      (client-acl %acl-list-all)
      (file-set-acl %acl-list-all)
      (command-acl %acl-list-all)
      (pool-acl %acl-list-all)
      (storage-acl %acl-list-all)
      (tls-server
       (bacula-tls-server-settings
        (shared %tls-settings-edoras)
        (tls-dh-file "edoras.dh4096.pem"))))

     (bacula-director-console
      (name "Baculum")
      (password "baculum-console")
      (job-acl %acl-list-all)
      (catalog-acl %acl-list-all)
      (client-acl %acl-list-all)
      (file-set-acl %acl-list-all)
      (command-acl '("autodisplay" "cancel" "estimate" "gui" "list" "llist"
                     "messages" "memory" "query" "run" "restart" "resume"
                     "status" "setdebug" "stop" "show" "statistics"
                     "version" "wait"
                     ".client" ".fileset" ".jobs" ".level" ".messages" ".msgs"
                     ".pool" ".status" ".storage" ".types"))
      (pool-acl %acl-list-all)
      (storage-acl %acl-list-all)
      (tls-server
       (bacula-tls-server-settings
        (shared %tls-settings-edoras)
        (tls-dh-file "edoras.dh4096.pem"))))))

   (job-defs
    (list
     (bacula-job-defs-resource
      (name "Edoras-Full")
      (type 'backup)
      (messages "Full-Job-Messages")
      (accurate #t)
      (pool "Edoras-S3")
      (differential-backup-pool "Edoras-Differential")
      (incremental-backup-pool "Edoras-Incremental"))

     (bacula-job-defs-resource
      (name "Edoras-Differential")
      (type 'backup)
      (level 'differential)
      (messages "Incremental-Job-Messages")
      (accurate #t)
      (pool "Edoras-Differential")
      (differential-backup-pool "Edoras-Differential")
      (incremental-backup-pool "Edoras-Incremental"))

     (bacula-job-defs-resource
      (name "Edoras-Incremental")
      (type 'backup)
      (level 'incremental)
      (messages "Incremental-Job-Messages")
      (accurate #t)
      (pool "Edoras-Incremental")
      (differential-backup-pool "Edoras-Differential")
      (incremental-backup-pool "Edoras-Incremental"))))

   (job
    (list
     (bacula-job-resource
      (name "Edoras-Root")
      (job-defs "Edoras-Full")
      (schedule "Server-Schedule")
      (client "Edoras")
      (fileset "Guix-System"))

     (bacula-job-resource
      (name "Edoras-Root-Restore")
      (type 'restore)
      (messages "Default")
      (pool "Edoras-S3")
      (client "Edoras")
      (fileset "Guix-System"))

     (bacula-job-resource
      (name "Edoras-Data")
      (job-defs "Edoras-Full")
      (schedule "Server-Schedule")
      (client "Edoras")
      (fileset "Guix-Data"))

     (bacula-job-resource
      (name "Pelargir-Root")
      (job-defs "Edoras-Full")
      (schedule "Server-Schedule")
      (client "Pelargir")
      (fileset "Guix-System"))

     (bacula-job-resource
      (name "Pelargir-Home")
      (job-defs "Edoras-Full")
      (schedule "Server-Schedule")
      (client "Pelargir")
      (fileset "Guix-Home"))

     (bacula-job-resource
      (name "Amsterdam")
      (job-defs "Edoras-Full")
      (schedule "Server-Schedule")
      (client "Amsterdam")
      (fileset "Amsterdam"))

     (bacula-job-resource
      (name "Fornost")
      (job-defs "Edoras-Full")
      (schedule "Server-Schedule")
      (client "Fornost")
      (fileset "Fornost"))

     (bacula-job-resource
      (name "Gondor-Home")
      (job-defs "Edoras-Full")
      (client "Gondor")
      (fileset "Gondor-Home"))

     (bacula-job-resource
      (name "Gondor-Workspace")
      (job-defs "Edoras-Full")
      (client "Gondor")
      (fileset "Gondor-Workspace"))

     (bacula-job-resource
      (name "Gondor-Root")
      (job-defs "Edoras-Full")
      (client "Gondor")
      (fileset "Gondor-Root"))

     (bacula-job-resource
      (name "Gondor-Local")
      (job-defs "Edoras-Full")
      (client "Gondor")
      (fileset "Gondor-Local"))

     ;; CAREFUL: Minas-Tirith and Moria must be stopped for this.
     (bacula-job-resource
      (name "Gondor-VM")
      (job-defs "Edoras-Full")
      (client "Gondor")
      (fileset "Gondor-VM"))

     ;; Arnor
     (bacula-job-resource
      (name "Arnor-Home")
      (job-defs "Edoras-Full")
      (client "Arnor")
      (fileset "Arnor-Home"))

     (bacula-job-resource
      (name "Arnor-Workspace")
      (job-defs "Edoras-Full")
      (client "Arnor")
      (fileset "Arnor-Workspace"))

     (bacula-job-resource
      (name "Arnor-Root")
      (job-defs "Edoras-Full")
      (client "Arnor")
      (fileset "Arnor-Root"))

     (bacula-job-resource
      (name "Arnor-Local")
      (job-defs "Edoras-Full")
      (client "Arnor")
      (fileset "Arnor-Local"))

     (bacula-job-resource
      (name "Arnor-VM")
      (job-defs "Edoras-Full")
      (client "Arnor")
      (fileset "Arnor-VM"))))

   (pool
    (list
     (bacula-pool-resource
      (name "Edoras-S3")
      (storage "Edoras-S3")
      (volume-retention "3 years")
      (auto-prune #f)
      (recycle #f)
      (maximum-volume-bytes "100g")
      (maximum-volume-jobs 1))

     (bacula-pool-resource
      (name "Edoras-Differential")
      (storage "Edoras-Differential")
      (volume-retention "3 months")
      (auto-prune #t)
      (recycle #t)
      (maximum-volumes 15)
      (maximum-volume-bytes "100g")
      (maximum-volume-jobs 1))

     (bacula-pool-resource
      (name "Edoras-Incremental")
      (storage "Edoras-Incremental")
      (volume-retention "3 weeks")
      (auto-prune #t)
      (recycle #t)
      (maximum-volumes 15)
      (maximum-volume-bytes "100g")
      (maximum-volume-jobs 1))))

   (storage
    (list
     (bacula-director-storage
      (name "Edoras-S3")
      (address "edoras.baulig.dev")
      (sd-port 9103)
      (tls %tls-settings-edoras)
      (password "bacula-storage-full")
      (device "Edoras-S3")
      (media-type "Edoras-S3"))

     (bacula-director-storage
      (name "Edoras-Differential")
      (address "edoras.baulig.dev")
      (sd-port 9104)
      (tls %tls-settings-edoras)
      (password "bacula-storage-differential")
      (device "Edoras-Differential")
      (media-type "Edoras-Differential"))

     (bacula-director-storage
      (name "Edoras-Incremental")
      (address "edoras.baulig.dev")
      (sd-port 9105)
      (tls %tls-settings-edoras)
      (password "bacula-storage-incremental")
      (device "Edoras-Incremental")
      (media-type "Edoras-Incremental"))))

   (fileset %bacula-fileset-list)

   (schedule
    (list
     (bacula-schedule "Server-Schedule"
                      (level full "monthly on 1st sun at 0:15")
                      (level differential "mon at 6:30")
                      (level incremental "tue-sun at 6:30"))))

   (director
    (bacula-director-resource
     (name "Edoras-Director")
     (dir-address %edoras-vpn-ip-address)
     (tls-server
      (bacula-tls-server-settings
       (shared %tls-settings-edoras)
       (tls-dh-file "edoras.dh4096.pem")))
     (messages "Default")
     (description "Bacula Edoras Director")
     (password "bacula-director-edoras")))))

