;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config edoras bacula)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (guix gexp)
  #:use-module (baulig config bacula)
  #:use-module (baulig config common)
  #:use-module (baulig config edoras bacula director)
  #:use-module (baulig config edoras bacula storage)
  #:use-module (baulig services account)
  #:use-module (baulig services bacula common)
  #:use-module (baulig services bacula database)
  #:use-module (baulig services bacula console)
  #:use-module (baulig services bacula client)
  #:use-module (baulig services bacula pki)
  #:use-module (baulig services bacula tls)
  #:re-export (%bacula-director-edoras
               %bacula-storage-edoras-full
               %bacula-storage-edoras-differential
               %bacula-storage-edoras-incremental))


;;;
;;; Bacula Database
;;;

(define-public %bacula-database-edoras
  (bacula-database-configuration
   (account
    (account-configuration
     (id 'bacula-database)
     (supplementary-groups '("postgres-socket"))))
   (password-key "bacula-database")))


;;;
;;; Bacula Console
;;;

(define-public %bacula-console-edoras
  (bacula-console-configuration
   (id 'edoras)

   (director
    (bacula-console-director
     (name "Edoras-Director")
     (address "edoras.baulig.dev")
     (tls %tls-settings-edoras)
     (password "bacula-director-edoras")))

   (console
    (bacula-console-resource
     (name "Edoras")
     (tls %tls-settings-edoras)
     (director "Edoras-Director")
     (password "bacula-console-edoras")))))


(define-public %baculum-console-edoras
  (bacula-console-configuration
   (id 'baculum)

   (director
    (bacula-console-director
     (name "Edoras-Director")
     (address "edoras.baulig.dev")
     (tls %tls-settings-edoras)
     (password "bacula-director-edoras")))

   (console
    (bacula-console-resource
     (name "Baculum")
     (tls %tls-settings-edoras)
     (director "Edoras-Director")
     (password "baculum-console")))))


;;;
;;; Bacula Client
;;;

(define-public %bacula-client-edoras
  (bacula-client-configuration
   (messages %default-messages)

   (director
    (list
     (bacula-client-director
      (name "Master-Director")
      (tls %tls-settings-edoras)
      (password "bacula-client-edoras-master"))

     (bacula-client-director
      (name "Edoras-Director")
      (tls %tls-settings-edoras)
      (password "bacula-client-edoras"))))

   (client
    (bacula-client-resource
     (name "Edoras")
     (tls %tls-settings-edoras)
     (fd-address %edoras-vpn-ip-address)
     (pki
      (bacula-pki-settings
       (pki-keypair "bacula-pki-keypair")
       (pki-master-key "bacula-master-key")))))))
