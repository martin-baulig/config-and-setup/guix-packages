;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config edoras prometheus)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (baulig config common)
  #:use-module (baulig services account)
  #:use-module (baulig services prometheus)
  #:use-module (baulig services serialization prometheus)
  #:use-module (baulig build config-utils)
  #:use-module (web uri))


;;;
;;; Prometheus
;;;

(define (scrape-node-exporter name address)
  (prometheus-scrape-configuration
   (job-name name)
   (scrape-interval (prometheus-duration 5 seconds))
   (static-configs
    (list
     (prometheus-static-configuration
      (targets `(,(string-append address ":9100")))
      (labels
       (prometheus-static-label
        (extra-labels `((group . ,name))))))))
   (relabel-configs
    (list
     (prometheus-relabel-configuration
      (source-labels '("job"))
      (target-label "instance"))))))


(define-public %prometheus-edoras
  (prometheus-configuration
   (listen-address %edoras-vpn-ip-address)
   (external-url (build-uri 'https #:host "edoras.baulig.is" #:path "/prometheus/"))
   (data-directory "/storage/Prometheus")
   (storage-retention-time "120d")
   (storage-retention-size "20GB")
   (web
    (prometheus-web-configuration
     (tls-server-config
      (prometheus-tls-server-config
       (cert-file "edoras.baulig.dev.crt")
       (key-file "edoras.baulig.dev.key")))
     (basic-auth-users
      (list
       ;; We need to use same user / password as for the main website for this
       ;; to work via the Nginx Reverse Proxy.
       (prometheus-basic-auth
        (username "martin")
        (password "edoras-web-bcrypt"))
       ;; This is used by the scraper.
       ;; We use a different username here, so we don't have to put the main
       ;; web password into this config file.
       (prometheus-basic-auth
        (username "metrics")
        (password "prometheus-metrics-bcrypt"))))))
   (scrape-configs
    (list
     (scrape-node-exporter "gondor" %gondor-vpn-ip-address)
     (scrape-node-exporter "arnor" %arnor-vpn-ip-address)
     (scrape-node-exporter "amsterdam" %amsterdam-vpn-ip-address)
     (scrape-node-exporter "edoras" %edoras-vpn-ip-address)
     (scrape-node-exporter "pelargir" %pelargir-vpn-ip-address)
     (scrape-node-exporter "fornost" %fornost-vpn-ip-address)))))

