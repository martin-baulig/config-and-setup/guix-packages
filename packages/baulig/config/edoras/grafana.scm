;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config edoras grafana)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (baulig config common)
  #:use-module (baulig services account)
  #:use-module (baulig services grafana)
  #:use-module (web uri))


;;;
;;; Grafana
;;;

(define-public %grafana-edoras
  (grafana-configuration
   (instance-name "edoras.baulig.dev")
   (trusted-ca-file "ca.crt")
   (paths
    (grafana-paths-section
     (data "/storage/Grafana")))
   (server
    (grafana-server-section
     (domain "edoras.baulig.is")
     (root-url (build-uri 'https #:host "edoras.baulig.is" #:path "/grafana/"))
     (http-addr "127.0.0.1")))
   (security
    (grafana-security-section
     (admin-user "martin")
     (admin-password "grafana-admin-password")
     (secret-key "grafana-secret-key")))))
