;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig config bacula)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (baulig services bacula common)
  #:use-module (baulig services bacula director)
  #:use-module (baulig services bacula tls))


;;;
;;; Shared Settings
;;;

(define-public %default-messages
  (bacula-messages-resource
   (name "Default")
   (catalog "all")
   (console "all, !skipped, !saved")
   (syslog "all")))

(define-public %full-job-messages
  (bacula-file-message-resource
   (parent %default-messages)
   "Full-Job-Messages" "bacula-full-jobs.log" "all"))

(define-public %differential-job-messages
  (bacula-file-message-resource
   (parent %default-messages)
   "Differential-Job-Messages" "bacula-differential-jobs.log" "all"))

(define-public %incremental-job-messages
  (bacula-file-message-resource
   (parent %default-messages)
   "Incremental-Job-Messages" "bacula-incremental-jobs.log" "all"))


;;;
;;; Common Settings
;;;

(define-public %tls-settings-minas-tirith
  (bacula-tls-settings
   (tls-ca-certificate-file "ca.crt")
   (tls-certificate "minas-tirith.baulig.is.crt")
   (tls-key "minas-tirith.baulig.is.key")))


(define-public %tls-settings-edoras
  (bacula-tls-settings
   (tls-ca-certificate-file "ca.crt")
   (tls-certificate "edoras.baulig.dev.crt")
   (tls-key "edoras.baulig.dev.key")))


(define-public %tls-settings-pelargir
  (bacula-tls-settings
   (tls-ca-certificate-file "ca.crt")
   (tls-certificate "pelargir.baulig.dev.crt")
   (tls-key "pelargir.baulig.dev.key")))


(define-public %tls-settings-lothlorien
  (bacula-tls-settings
   (tls-ca-certificate-file "ca.crt")
   (tls-certificate "lothlorien.baulig.is.crt")
   (tls-key "lothlorien.baulig.is.key")))


(define-public %tls-settings-lothlorien-dev
  (bacula-tls-settings
   (tls-ca-certificate-file "ca.crt")
   (tls-certificate "lothlorien.baulig.dev.crt")
   (tls-key "lothlorien.baulig.dev.key")))


;; These are paths on Gondor, not Lothlórien.
;; On OpenBSD, /etc/bacula is root:_bacula, mode 750
;; and contains all the Bacula configuration files.
(define-public %tls-settings-gondor
  (bacula-tls-settings
   (tls-ca-certificate-file "ca.crt")
   (tls-certificate "gondor.baulig.is.crt")
   (tls-key "gondor.baulig.is.key")))


(define-public %tls-settings-arnor
  (bacula-tls-settings
   (tls-ca-certificate-file "ca.crt")
   (tls-certificate "arnor.baulig.dev.crt")
   (tls-key "arnor.baulig.dev.key")))


(define-public %tls-settings-amsterdam
  (bacula-tls-settings
   (tls-ca-certificate-file "ca.crt")
   (tls-certificate "amsterdam.baulig.dev.crt")
   (tls-key "amsterdam.baulig.dev.key")))


(define-public %tls-settings-fornost
  (bacula-tls-settings
   (tls-ca-certificate-file "ca.crt")
   (tls-certificate "fornost.baulig.dev.crt")
   (tls-key "fornost.baulig.dev.key")))


;;;
;;; Bacula File Sets
;;;

(define-public %bacula-fileset-list
  (list
   (bacula-fileset-resource
    (name "Guix-System")
    (include-list
     (bacula-fileset-include
      (options
       (bacula-fileset-options
        (compression 'gzip9)
        (signature 'sha1)))
      (file "/")))
    (exclude-list
     (bacula-fileset-exclude
      (file '("/dev" "/proc" "/sys" "/tmp" "/run" "/var/run" "/var/lock" "/gnu" "/swapfile")))))

   (bacula-fileset-resource
    (name "Guix-Data")
    (include-list
     (bacula-fileset-include
      (options
       (bacula-fileset-options
        (compression 'gzip9)
        (signature 'sha1)))
      (file '("/backup" "/home" "/storage")))))

   (bacula-fileset-resource
    (name "Guix-Home")
    (include-list
     (bacula-fileset-include
      (options
       (bacula-fileset-options
        (compression 'gzip9)
        (signature 'sha1)))
      (file "/home"))))

   (bacula-fileset-resource
    (name "Gondor-Home")
    (include-list
     (bacula-fileset-include
      (options
       (bacula-fileset-options
        (compression 'gzip9)
        (signature 'sha1)))
      (file "/home"))))

   (bacula-fileset-resource
    (name "Gondor-Workspace")
    (include-list
     (bacula-fileset-include
      (options
       (bacula-fileset-options
        (compression 'gzip9)
        (signature 'sha1)))
      (file "/Workspace"))))

   (bacula-fileset-resource
    (name "Gondor-Root")
    (include-list
     (bacula-fileset-include
      (options
       (bacula-fileset-options
        (compression 'gzip9)
        (signature 'sha1)))
      (file '("/" "/var")))))

   (bacula-fileset-resource
    (name "Gondor-Local")
    (include-list
     (bacula-fileset-include
      (options
       (bacula-fileset-options
        (compression 'gzip9)
        (signature 'sha1)))
      (file "/usr/local"))))

   (bacula-fileset-resource
    (name "Gondor-VM")
    (include-list
     (bacula-fileset-include
      (options
       (bacula-fileset-options
        (compression 'gzip9)
        (signature 'sha1)))
      (file "/VM"))))

   (bacula-fileset-resource
    (name "Arnor-Home")
    (include-list
     (bacula-fileset-include
      (options
       (bacula-fileset-options
        (compression 'gzip9)
        (signature 'sha1)))
      (file "/home"))))

   (bacula-fileset-resource
    (name "Arnor-Workspace")
    (include-list
     (bacula-fileset-include
      (options
       (bacula-fileset-options
        (compression 'gzip9)
        (signature 'sha1)))
      (file "/Workspace"))))

   (bacula-fileset-resource
    (name "Arnor-Root")
    (include-list
     (bacula-fileset-include
      (options
       (bacula-fileset-options
        (compression 'gzip9)
        (signature 'sha1)))
      (file '("/" "/usr" "/var")))))

   (bacula-fileset-resource
    (name "Arnor-Local")
    (include-list
     (bacula-fileset-include
      (options
       (bacula-fileset-options
        (compression 'gzip9)
        (signature 'sha1)))
      (file '("/usr/src" "/usr/obj" "/usr/local")))))

   (bacula-fileset-resource
    (name "Arnor-VM")
    (include-list
     (bacula-fileset-include
      (options
       (bacula-fileset-options
        (compression 'gzip9)
        (signature 'sha1)))
      (file "/VM"))))
   (bacula-fileset-resource
    (name "Amsterdam")
    (include-list
     (bacula-fileset-include
      (options
       (bacula-fileset-options
        (compression 'gzip9)
        (signature 'sha1)))
      (file '("/" "/home" "/usr" "/usr/X11R6" "/usr/local"
              "/usr/obj" "/usr/src" "/var")))))

   (bacula-fileset-resource
    (name "Fornost")
    (include-list
     (bacula-fileset-include
      (options
       (bacula-fileset-options
        (compression 'gzip9)
        (signature 'sha1)))
      (file '("/" "/usr/local")))))))
