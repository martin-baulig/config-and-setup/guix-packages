;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig build-system ksh)
  #:use-module (baulig build ksh-build-system)
  #:use-module (guix store)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix monads)
  #:use-module (guix search-paths)
  #:use-module (guix build-system)
  #:use-module (guix build-system gnu)
  #:export (%ksh-build-system-modules
            ksh-build
            ksh-build-system))

;; Commentary:
;;
;; Custom build system for the AT&T Korn Shell.
;; This is implemented as an extension of 'gnu-build-system'.
;;
;; Code:

(define %ksh-build-system-modules
  ;; Build-side modules imported by default.
  `((baulig build ksh-build-system)
    ,@%gnu-build-system-modules))

(define* (lower name
                #:key root source inputs native-inputs outputs system target
                #:allow-other-keys
                #:rest arguments)
  "Return a bag for NAME."
  (define private-keywords
    '(#:target #:inputs #:native-inputs))

  (and (not target)                               ;XXX: no cross-compilation
       (bag
         (name name)
         (system system)
         (host-inputs `(,@(if source
                              `(("source" ,source))
                              '())
                        ,@inputs

                        ;; Keep the standard inputs of 'gnu-build-system'.
                        ,@(standard-packages)))
         (build-inputs `(,@native-inputs))
         (outputs outputs)
         (build ksh-build)
         (arguments (strip-keyword-arguments private-keywords arguments)))))

(define* (ksh-build name inputs
                    #:key source root install-etc?
                    (phases '%standard-phases)
                    (outputs '("out"))
                    (search-paths '())
                    (system (%current-system))
                    (guile #f)
                    (imported-modules %ksh-build-system-modules)
                    (modules '((baulig build ksh-build-system)
                               (guix build utils))))
  "Build SOURCE using the bin/package script."
  (define builder
    (with-imported-modules imported-modules
      #~(begin
          (use-modules #$@(sexp->gexp modules))
          (ksh-build #:name #$name
                     #:source #+source
                     #:system #$system
                     #:root #$root
                     #:install-etc? #$install-etc?
                     #:phases #$phases
                     #:outputs #$(outputs->gexp outputs)
                     #:search-paths '#$(sexp->gexp
                                        (map search-path-specification->sexp
                                             search-paths))
                     #:inputs #$(input-tuples->gexp inputs)))))

  (mlet %store-monad ((guile (package->derivation (or guile (default-guile))
                                                  system #:graft? #f)))
    (gexp->derivation name builder
                      #:system system
                      #:guile-for-build guile)))

(define ksh-build-system
  (build-system
    (name 'ksh)
    (description "The build system for the AT&T Korn Shell.")
    (lower lower)))

;;; ksh.scm ends here
