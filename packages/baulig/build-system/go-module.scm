;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig build-system go-module)
  #:use-module (baulig build go-module-build-system)
  #:use-module (guix store)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix monads)
  #:use-module (guix search-paths)
  #:use-module (guix build-system)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages golang)
  #:export (%go-module-build-system-modules
            go-module-build
            go-module-build-system))

;; Commentary:
;;
;; Building Go modules that have been fetched via 'multi-fetch'.
;;
;; Code:

(define %go-module-build-system-modules
  ;; Build-side modules imported by default.
  `((baulig build go-module-build-system)
    ,@%gnu-build-system-modules))

(define* (lower name
                #:key source inputs native-inputs outputs system target
                (go-commands '())
                (environment-variables '())
                #:allow-other-keys
                #:rest arguments)
  "Return a bag for NAME."
  (define private-keywords
    '(#:target #:inputs #:native-inputs))

  (if target
      (error "No cross-compilation for go-module-build-system: LOWER" target)
        (bag
          (name name)
          (system system)
          (host-inputs `(,@(if source
                               `(("source" ,source))
                               '())
                         ,@inputs

                         ;; Keep the standard inputs of 'gnu-build-system'.
                         ,@(standard-packages)))
          (build-inputs `(("go" ,go-1.19)
                          ,@native-inputs))
          (outputs outputs)
          (build go-module-build)
          (arguments (strip-keyword-arguments private-keywords arguments)))))

(define* (go-module-build name inputs
                          #:key source
                          (phases '%standard-phases)
                          (outputs '("out"))
                          (search-paths '())
                          (environment-variables '())
                          (go-commands '())
                          (verbose? #t)
                          (debug? #f)
                          (system (%current-system))
                          (guile #f)
                          (imported-modules %go-module-build-system-modules)
                          (modules '((baulig build go-module-build-system)
                                     (guix build utils))))
  "Build SOURCE using the bin/package script."
  (define builder
    (with-imported-modules imported-modules
      #~(begin
          (use-modules #$@(sexp->gexp modules))
          (go-module-build #:name #$name
                           #:source #+source
                           #:system #$system
                           #:environment-variables '#$environment-variables
                           #:go-commands '#$go-commands
                           #:verbose? #$verbose?
                           #:debug? #$debug?
                           #:phases #$phases
                           #:outputs #$(outputs->gexp outputs)
                           #:search-paths '#$(sexp->gexp
                                              (map search-path-specification->sexp
                                                   search-paths))
                           #:inputs #$(input-tuples->gexp inputs)))))

  (mlet %store-monad ((guile (package->derivation (or guile (default-guile))
                                                  system #:graft? #f)))
    (gexp->derivation name builder
                      #:system system
                      #:guile-for-build guile)))

(define go-module-build-system
  (build-system
    (name 'go-module)
    (description "Go Module Build System")
    (lower lower)))

;;; go-module.scm ends here
