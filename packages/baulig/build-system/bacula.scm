;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig build-system bacula)
  #:use-module (baulig build bacula-build-system)
  #:use-module (guix store)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix monads)
  #:use-module (guix search-paths)
  #:use-module (guix build-system)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages readline)
  #:use-module (gnu packages tls)
  #:export (%bacula-build-system-modules
            bacula-build
            bacula-build-system))

;; Commentary:
;;
;; Custom build system for Bacula.
;; This is implemented as an extension of 'gnu-build-system'.
;;
;; Code:

(define %bacula-build-system-modules
  ;; Build-side modules imported by default.
  `((baulig build bacula-build-system)
    ,@%gnu-build-system-modules))

(define* (lower name
                #:key source inputs native-inputs outputs system target client-only?
                #:allow-other-keys
                #:rest arguments)
  "Return a bag for NAME."
  (define private-keywords
    '(#:target #:inputs #:native-inputs))

  (and (not target)                               ;XXX: no cross-compilation
       (bag
         (name name)
         (system system)
         (host-inputs `(,@(if source
                              `(("source" ,source))
                              '())
                        ,@inputs

                        ("bc" ,bc)
                        ("readline" ,readline)

                        ;; Keep the standard inputs of 'gnu-build-system'.
                        ,@(standard-packages)))
         (build-inputs `(,@native-inputs
                         ,@(if client-only?
                               `()
                               `(("inetutils" ,inetutils)
                                 ("openssl" ,openssl)
                                 ("postgresql" ,postgresql-15)))))
         (outputs outputs)
         (build bacula-build)
         (arguments (strip-keyword-arguments private-keywords arguments)))))

(define* (bacula-build name inputs
                       #:key source client-only?
                       (phases '%standard-phases)
                       (outputs '("out"))
                       (search-paths '())
                       (system (%current-system))
                       (guile #f)
                       (imported-modules %bacula-build-system-modules)
                       (modules '((baulig build bacula-build-system)
                                  (guix build utils))))
  "Build SOURCE using the bin/package script."
  (define builder
    (with-imported-modules imported-modules
      #~(begin
          (use-modules #$@(sexp->gexp modules))
          (bacula-build #:name #$name
                        #:source #+source
                        #:system #$system
                        #:phases #$phases
                        #:outputs #$(outputs->gexp outputs)
                        #:client-only? #$client-only?
                        #:configure-flags `()
                        #:search-paths '#$(sexp->gexp
                                           (map search-path-specification->sexp
                                                search-paths))
                        #:inputs #$(input-tuples->gexp inputs)))))

  (mlet %store-monad ((guile (package->derivation (or guile (default-guile))
                                                  system #:graft? #f)))
    (gexp->derivation name builder
                      #:system system
                      #:guile-for-build guile)))

(define bacula-build-system
  (build-system
    (name 'bacula)
    (description "The build system for Bacula.")
    (lower lower)))

;;; bacula.scm ends here
