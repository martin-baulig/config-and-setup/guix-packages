;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services baculum)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (guix build utils)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages php)
  #:use-module (gnu packages web)
  #:use-module (gnu services)
  #:use-module (baulig build config-utils)
  #:use-module (baulig packages bacula)
  #:use-module (baulig packages baculum)
  #:use-module (baulig services account)
  #:use-module (baulig services activation)
  #:use-module (baulig services parameters)
  #:use-module (baulig services secrets)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization syntax-macros)
  #:use-module (baulig services serialization underscore-space-semicolon)
  #:use-module (baulig services serialization nginx)
  #:use-module (baulig services bacula console)
  #:use-module (baulig services nginx)
  #:use-module (baulig services php-fpm)
  #:use-module (rnrs enums)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (web uri))


;;;
;;; Baculum Service
;;;

(define-public-configuration/no-serialization baculum-configuration
  (listen
   (nginx-listen-port-or-address 9097)
   "Port to listen on.")

  (server-name
   host-name
   "Server name.")

  (root-url
   uri
   "Full URL used to access Baculum from a web browser.")

  (ssl-certificate
   secrets-service-blob
   "Secrets Service Blob containing the PEM encoded TLS certificate.")

  (ssl-certificate-key
   secrets-service-blob
   "Secrets Service Blob containing the PEM encoded TLS certificate key.")

  (database-name
   (string "bacula")
   "Name of database to connect to.")

  (database-user
   string
   "User name to log into the database.")

  (database-password
   secrets-service-password
   "Database password key.")

  (database-ip-address
   ipv4-address
   "IP address of database to connect to.")

  (database-port
   integer
   "Port number of database to connect to.")

  (web-user
   (string "baculum")
   "User name for the web interface.")

  (console
   bacula-console-configuration
   "Bacula Console configuration."))


(define-record-type* <baculum-service>
  baculum-service
  make-baculum-service
  baculum-service?

  (configuration        baculum-service-configuration)
  (secrets              baculum-service-secrets))



;;;
;;; Interal APIs
;;;

(define %root-directory "@parameter:service-root@/php-fpm-baculum")
(define %data-directory "@parameter:service-root@/php-fpm-baculum/data")
(define %document-root "@parameter:baculum@/www")

(define* (resolve-baculum-server service #:key parameters)
  (match-records* ((service <baculum-configuration> listen server-name
                            root-url ssl-certificate ssl-certificate-key))
      (nginx-server-configuration
       (id 'baculum)
       (listen listen)
       (server-name server-name)
       (ssl-certificate ssl-certificate)
       (ssl-certificate-key ssl-certificate-key)
       (root-path %document-root)
       (index "index.php")

       (rewrite-log #t)

       (extra-directives '("charset utf-8"
                           "set $baculum_index index.php"
                           "auth_basic \"Baculum\""
                           "auth_basic_user_file @parameter:service-root@/php-fpm-baculum/htpasswd"))

       (extra-parameters #~((baculum . #$baculum)))

       (php-fpm
        (php-fpm-configuration
         (id 'baculum)
         (extra-directives
          `("set $furi /$baculum_index"
            ,(nginx-conditional
              (condition "-f $document_root$fastcgi_script_name")
              (directives `("set $furi $fastcgi_script_name")))
            "set $script_file_name $document_root$furi"
            "set $real_file_name   $document_root$furi"
            ,(nginx-conditional
              (condition "$fastcgi_script_name ~ index.php")
              (directives `(,(string-append "set $script_file_name " %root-directory "/index.php")
                            "set $real_file_name   $document_root/index.template")))
            "fastcgi_intercept_errors on"
            "fastcgi_param SCRIPT_FILENAME       $script_file_name"
            "fastcgi_param REAL_SCRIPT_FILENAME  $real_file_name"
            "fastcgi_param SERVER_NAME           $host"
            ,(string-append "fastcgi_param REQUEST_URI           " (uri->string root-url) "$request_uri")
            ,(string-append "fastcgi_param ORIG_SCRIPT_NAME      " (uri->string root-url) "$furi")
            ,(string-append "fastcgi_param SCRIPT_NAME           " "$script_file_name")
            "fastcgi_param PATH_INFO             $fastcgi_path_info"
            "fastcgi_param PATH_TRANSLATED       $document_root$furi"))))

       (location
        (list
         (nginx-location-section
          (path "/")
          (configuration
           (nginx-location-configuration
            (extra-directives '("index $baculum_index"
                                "try_files $uri $uri/ $baculum_index?$args"
                                "rewrite ^/api/$ /panel/"
                                "rewrite ^/api,(.+)$ /panel/$1"
                                "rewrite ^/(.+)$ /index.php/$1 last"
                                "rewrite ^/$ /web/ permanent")))))
         (nginx-location-section
          (type 'regex)
          (path "^/assets/")
          (configuration
           (nginx-location-configuration
            (extra-directives `(,(string-append "root " %data-directory))))))
         (nginx-location-section
          (type 'regex)
          (path "^/(protected|framework)/")
          (configuration
           (nginx-location-configuration
            (extra-directives '("deny all")))))
         (nginx-location-section
          (type 'regex)
          (path "\\.(js|css|png|jpg|gif|ico|woff|woff2)")
          (configuration
           (nginx-location-configuration
            (extra-directives '("try_files $uri = 404"))))))))))

(define (map-extra-parameters configuration)
  (match-records* ((configuration <baculum-configuration>
                                  database-name database-user database-password
                                  database-ip-address database-port
                                  web-user))
      `((baculum-database-name . ,database-name)
        (baculum-database-user . ,database-user)
        (baculum-database-ip-address . ,database-ip-address)
        (baculum-database-port . ,database-port)
        (baculum-web-user . ,web-user))))

(define* (resolve-baculum-secrets configuration #:key parameters)
  (match-records* ((configuration <baculum-configuration> listen console
                                  database-name database-user database-password)
                   (parameters <system-parameters> service-root))
      (let* ((working-directory (string-append service-root "/php-fpm-baculum"))
             (private-directory (string-append working-directory "/private"))
             (data-directory (string-append working-directory "/data"))
             (config-sections
              (map (lambda (item)
                     (let ((output (string-append "data/" item))
                           (template (file-append baculum "/etc/baculum/" (basename item))))
                       (create-secrets-section (store-file output template)
                           (options group-readable place-in-root-directory))))
                   '("API/Config/api.conf" "Web/Config/hosts.conf" "Web/Config/users.conf"
                     "Web/Config/settings.conf")))
             (auth-user-file (create-secrets-section
                                 (template "htpasswd"
                                           "@parameter:baculum-web-user@:@text:baculum-htpasswd@\n")
                                 (options group-readable place-in-root-directory)))
             (role-file (create-secrets-section
                            (template "create-baculum-role.sql"
                                      (string-append
                                       "CREATE ROLE " database-user " WITH login "
                                       "PASSWORD '@password:" database-password "@';\n"
                                       "GRANT CONNECT ON DATABASE " database-name " TO " database-user ";\n"
                                       "GRANT USAGE ON SCHEMA public TO " database-user ";\n"
                                       "GRANT SELECT ON ALL TABLES IN SCHEMA public TO " database-user ";\n"))))
             (console-section (create-secrets-section
                                  (serialize "bconsole.conf" bacula-console console))))
        (create-secrets
            (account-reference baculum php-fpm-baculum)
            (main-section
             (store-file "index.php" (file-append baculum "/www/index.template"))
             (options place-in-root-directory parameters-only group-readable))
          (extra-sections `(,console-section ,auth-user-file ,role-file ,@config-sections))
          (extra-parameters gexp
                            #~((bacula . #$bacula)
                               (baculum . #$baculum)
                               (application-directory . #$(file-append baculum "/www"))
                               (application-data-directory . #$data-directory)
                               (private-directory . #$private-directory)
                               (data-directory . #$data-directory)
                               #$@(map-extra-parameters configuration)))))))


(define (resolve-baculum-activation service)
  (match-records* ((service <baculum-service> configuration secrets)
                   (configuration <baculum-configuration> database-name database-user)
                   (secrets <secrets-service> account output-file requirements)
                   (account <service-account> user group working-directory))
      (activation-task
       (id 'baculum-activation)
       (requirements requirements)
       (action
        #~(begin
            (format #t "Baculum activation.~%")
            (let* ((uid (passwd:uid (getpw #$user)))
                   (gid (group:gid (getgr #$group)))
                   (psql #$(file-append postgresql "/bin/psql"))
                   (data-dir (string-append #$working-directory "/data"))
                   (assets-dir (string-append data-dir "/assets"))
                   (role-file (string-append #$working-directory "/private/create-baculum-role.sql")))
              (unless (postgresql-role-exists? psql #$database-user)
                (format #t "Need to create Bacula database role: ~a~%" #$database-user)
                (invoke psql "-d" #$database-name "-U" "postgres" "-f" role-file))
              (create-directory-with-owner data-dir uid gid #o750)
              (create-directory-with-owner assets-dir uid gid #o750)
              (for-each
               (lambda (dir)
                 (create-directory-with-owner
                  (string-append data-dir "/" dir)
                  uid gid #o700))
               '("runtime" "API" "Web" "API/Logs" "Web/Logs"))))))))


;;;
;;; Baculum Service
;;;

(define-public baculum-service-type
  (service-type (name 'baculum)
                (extensions
                 (list (service-extension activation-task-service-type
                                          resolve-baculum-activation)))
                (description "Baculum Service")))


(define* (resolve-baculum configuration #:key secrets parameters)
  (match-records* ((configuration <baculum-configuration> listen))
      (baculum-service
       (configuration configuration)
       (secrets secrets))))



(define-parameterized-service baculum
  (record-type <baculum-configuration>)
  (parent secrets resolve-baculum-secrets)
  (parent nginx resolve-baculum-server)
  (resolve resolve-baculum))
