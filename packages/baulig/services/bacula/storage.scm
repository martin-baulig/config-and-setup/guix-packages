;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services bacula storage)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (baulig packages bacula)
  #:use-module (baulig services account)
  #:use-module (baulig services parameters)
  #:use-module (baulig services secrets)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization syntax-macros)
  #:use-module (baulig services serialization key-value)
  #:use-module (baulig services bacula common)
  #:use-module (baulig services bacula tls)
  #:use-module (baulig build config-utils)
  #:use-module (rnrs enums)
  #:use-module (srfi srfi-1))


;;;
;;; Director Resource
;;;

(define-public-configuration bacula-storage-director
  (name
   resource-name
   "Name of the Director allowed to connect to this Storage daemon.")

  (tls
   bacula-tls-settings
   "TLS Settings.")

  (password
   secrets-service-password
   "Director password.")

  (monitor
   maybe-boolean
   "When set to true, this director can only monitor the status of this Storage daemon."))

(define-section-serializer key-value bacula-storage-director "director")


;;;
;;; Truncate Cache
;;;

(define-public bacula-cloud-truncate-cache
  (make-enumeration '(no after-upload)))

(define-enum-serializer key-value bacula-cloud-truncate-cache
  (lambda (value) (string-delete #\- (symbol->string value))))

(define-public-maybe bacula-cloud-truncate-cache)


;;;
;;; Upload
;;;

(define-public bacula-cloud-upload
  (make-enumeration '(no each-part)))

(define-enum-serializer key-value bacula-cloud-upload
  (lambda (value) (string-delete #\- (symbol->string value))))

(define-public-maybe bacula-cloud-upload)


;;;
;;; Cloud Resource
;;;

(define-string-constant key-value bacula-cloud-driver "S3")

(define-public-configuration bacula-cloud-resource
  (name
   resource-name
   "Name of this Cloud resource.")

  (driver
   (bacula-cloud-driver-constant %bacula-cloud-driver)
   "Cloud driver; we only support S3 at the moment.")

  (truncate-cache
   maybe-bacula-cloud-truncate-cache
   "This directive specifies when Bacula should automatically remove the local cache parts.")

  (upload
   maybe-bacula-cloud-upload
   "This directive specifies when local cache parts will be uploaded to the cloud.")

  (host-name
   host-name
   "S3 host name.")

  (bucket-name
   string
   "S3 Bucket name.")

  (access-key
   secrets-service-quoted-text
   "S3 Access key.")

  (secret-key
   secrets-service-quoted-text
   "S3 Secret key."))

(define-section-serializer key-value bacula-cloud-resource "cloud")

(define-public-maybe bacula-cloud-resource)


;;;
;;; Storage Resource
;;;

(define-public-configuration bacula-storage-resource
  (name
   resource-name
   "Name of this Storage resource.")

  (working-directory
   (bacula-working-directory-constant %bacula-working-directory)
   "Bacula working directory.")

  (pid-directory
   (bacula-pid-directory-constant %bacula-pid-directory)
   "Bacula PID directory.")

  (plugin-directory
   (bacula-plugin-directory-constant %bacula-plugin-directory)
   "Bacula Plugin directory.")

  (sd-address
   maybe-ipv4-address
   "IP Address to bind to.")

  (sd-port
   maybe-integer
   "Port number to listen on.")

  (tls-server
   bacula-tls-server-settings
   "TLS Settings."))


(define-section-serializer key-value bacula-storage-resource "storage")

(define-public-maybe bacula-storage-resource)


;;;
;;; Device Type
;;;

(define-public bacula-device-type
  (make-enumeration '(file cloud)))

(define-enum-serializer key-value bacula-device-type)

(define-public-maybe bacula-device-type)


;;;
;;; Device Resource
;;;

(define-public-configuration bacula-device-resource
  (name
   resource-name
   "Name of this Device resource.")

  (archive-device
   string
   "Storage device to use.")

  (cloud
   maybe-resource-name
   "Name of the Cloud resource to use.")

  (device-type
   maybe-bacula-device-type
   "Storage device type.")

  (media-type
   string
   "Media type name.")

  (maximum-volume-size
   maybe-size
   "Maximum number of bytes that can be written to each volume.")

  (maximum-file-size
   maybe-size
   "Maximum number of bytes that can be written to a single file on the volume.")

  (maximum-part-size
   maybe-size
   "Maximum size for each part.")

  (automatic-mount
   true-constant
   "Automatically mount the media.")

  (random-access
   (boolean #t)
   "Medium allows random access.")

  (removable-media
   (boolean #f)
   "Whether this is a removable media.")

  (always-open
   (boolean #f)
   "Always keep this media open.")

  (label-media
   (boolean #t)
   "Automatically label the media."))

(define-section-serializer key-value bacula-device-resource "device")


;;;
;;; Bacula Storage Configuration
;;;

(define-public-configuration bacula-storage-configuration
  (messages
   bacula-messages-resource
   "Bacula Messages Configuration")

  (director
   bacula-storage-director
   "The Director resource.")

  (storage
   bacula-storage-resource
   "The Storage resource.")

  (device
   bacula-device-resource
   "The Device resource.")

  (cloud
   maybe-bacula-cloud-resource
   "Optional Cloud resource.")

  (id
   maybe-account-id
   "Optional.  Unique ID identifying this storage."
   (serializer empty-serializer))

  (account
   maybe-account-configuration
   "Optional.  Service account configuration."
   (serializer empty-serializer)))


;;;
;;; Bacula Storage Service
;;;


(define-record-type* <bacula-storage-service>
  bacula-storage-service
  make-bacula-storage-service
  bacula-storage-service?

  (id                   bacula-storage-service-id)
  (secrets              bacula-storage-service-secrets)
  (configuration        bacula-storage-service-configuration))


(define* (resolve-bacula-storage configuration #:key secrets parameters)
  (match-records* ((configuration <bacula-storage-configuration> id account))
      (bacula-storage-service
       (configuration configuration)
       (id (cond ((maybe-value-set? id)
                  (symbol-append 'bacula-storage- id))
                 (else 'bacula-storage)))
       (secrets secrets))))


(define* (resolve-bacula-storage-secrets configuration #:key parameters)
  (match-records* ((configuration <bacula-storage-configuration> id account storage)
                   (storage <bacula-storage-resource> sd-port))
      (let* ((default (cond ((maybe-value-set? id)
                             (symbol-append 'bacula-storage- id))
                            (else 'bacula-storage)))
             (output-file (string-append (symbol->string default) ".conf"))
             (real-account (with-default
                            account
                            (account-configuration (id default))))
             (daemon-defaults (daemon-account-configuration
                               (pid-file (format #f "bacula-sd.~a.pid"
                                                 (with-default sd-port 9103)))))
             (daemon-account (make-daemon-account real-account daemon-defaults)))
        (create-secrets
            (daemon-account-expression daemon-account)
            (serialize output-file bacula-storage configuration)
          (extra-parameters gexp #~((bacula . #$bacula)))))))


(define (resolve-bacula-storage-shepherd service)
  (match-records* ((service <bacula-storage-service> id secrets configuration)
                   (secrets <secrets-service> account output-file requirements)
                   (account <service-account> user group supplementary-groups
                            working-directory log-file-path pid-file-path))
      (list
       (shepherd-service
        (provision `(,id))
        (documentation "Bacula Storage Service")
        (requirement `(networking ,@requirements))
        (respawn? #f)
        (start #~(make-forkexec-constructor
                  (list #$(file-append bacula "/sbin/bacula-sd")
                        "-c" #$output-file)
                  #:directory #$working-directory
                  #:user #$user
                  #:group #$group
                  #:supplementary-groups (list #$@supplementary-groups)
                  #:log-file #$log-file-path
                  #:pid-file #$pid-file-path))
        (stop #~(make-kill-destructor))))))


(define-public bacula-storage-service-type
  (service-type (name 'bacula-storage)
                (extensions
                 (list (service-extension shepherd-root-service-type
                                          resolve-bacula-storage-shepherd)))
                (compose identity)
                (description "Bacula Storage Service")))


(define-parameterized-service bacula-storage
  (record-type <bacula-storage-configuration>)
  (parent secrets resolve-bacula-storage-secrets)
  (compose list)
  (extend list)
  (resolve resolve-bacula-storage))
