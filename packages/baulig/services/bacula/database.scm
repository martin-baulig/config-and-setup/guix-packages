;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services bacula database)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages gawk)
  #:use-module (baulig packages bacula)
  #:use-module (baulig services account)
  #:use-module (baulig services activation)
  #:use-module (baulig services parameters)
  #:use-module (baulig services secrets)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization syntax-macros)
  #:use-module (baulig services serialization key-value)
  #:use-module (baulig services bacula common)
  #:use-module (baulig services bacula tls)
  #:use-module (baulig build config-utils)
  #:use-module (srfi srfi-1))


;;;
;;; Bacula Database Service
;;;

(define-record-type* <bacula-database-configuration>
  bacula-database-configuration
  make-bacula-database-configuration
  bacula-database-configuration?

  (account              bacula-database-configuration-account)
  (postgresql           bacula-database-configuration-postgresql    (default postgresql-14))
  (role                 bacula-database-configuration-role          (default "bacula"))
  (database             bacula-database-configuration-database      (default "bacula"))
  (password-key         bacula-database-configuration-password-key  (default #f)))

(export bacula-database-configuration)


(define-record-type* <bacula-database-service>
  bacula-database-service
  make-bacula-database-service
  bacula-database-service?

  (secrets              bacula-database-service-secrets)
  (configuration        bacula-database-service-configuration))


(define* (resolve-bacula-database-secrets configuration #:key parameters)
  (match-records* ((configuration <bacula-database-configuration>
                                  account role database password-key))
      (create-secrets (account-expression account)
          (template "create-bacula-database.sql"
                    (string-append
                     "CREATE ROLE " role " WITH login createdb "
                     "PASSWORD '@password:" password-key "@';\n")))))


(define (resolve-bacula-database-activation service)
  (match-records* ((service <bacula-database-service> configuration secrets)
                   (configuration <bacula-database-configuration> postgresql role)
                   (secrets <secrets-service> account output-file requirements)
                   (account <service-account> user group))

    (define create-script
      (mixed-text-file
       "create-bacula-database.sh"
       "#!/bin/sh\n"
       "echo $PATH\n"
       "export PATH=" gawk "/bin:" coreutils "/bin:" grep "/bin:$PATH"
       "id\n"
       "export PGUSER=" role "\n"
       bacula "/etc/create_postgresql_database\n"
       bacula "/etc/make_postgresql_tables"))

    (define activation-action
      #~(begin
          (let ((psql #$(file-append postgresql "/bin/psql"))
                (user #$(service-account-user account)))
            (format #t "Bacula setup script.~%")
            (unless (postgresql-role-exists? psql #$role)
              (format #t "Need to create Bacula role: ~a~%" #$output-file)
              (invoke psql "-d" "postgres" "-U" "postgres" "-f" #$output-file))
            (unless (postgresql-database-exists? psql "bacula")
              (format #t "Need to create Bacula database: ~a - ~a~%" user #$create-script)
              ;; Drop privileges to the bacula user prior to invoking the scripts.
              (invoke-as-user user #$(file-append bash "/bin/sh") #$create-script)))))

    (activation-task
     (id 'bacula-database)
     (requirements requirements)
     (action activation-action))))



(define-public bacula-database-service-type
  (service-type (name 'bacula-database)
                (extensions
                 (list (service-extension activation-task-service-type
                                          resolve-bacula-database-activation)))
                (description "Bacula Database Service")))


(define* (resolve-bacula-database configuration #:key secrets parameters)
  (match-records* ((configuration <bacula-database-configuration>))
      (bacula-database-service
       (configuration configuration)
       (secrets secrets))))



(define-parameterized-service bacula-database
  (record-type <bacula-database-configuration>)
  (parent secrets resolve-bacula-database-secrets)
  (resolve resolve-bacula-database))
