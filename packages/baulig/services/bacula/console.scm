;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services bacula console)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (gnu packages bash)
  #:use-module (gnu services)
  #:use-module (baulig packages bacula)
  #:use-module (baulig services account)
  #:use-module (baulig services activation)
  #:use-module (baulig services parameters)
  #:use-module (baulig services secrets)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization syntax-macros)
  #:use-module (baulig services serialization key-value)
  #:use-module (baulig services bacula common)
  #:use-module (baulig services bacula tls)
  #:use-module (baulig build utils)
  #:use-module (baulig build config-utils)
  #:use-module (srfi srfi-1))


;;;
;;; Director Resource
;;;

(define-public-configuration bacula-console-director
  (name
   resource-name
   "Director name.")

  (address
   string
   "Address of this Director.")

  (tls
   maybe-bacula-tls-settings
   "TLS Settings.")

  (password
   secrets-service-password
   "Director password."))

(define-section-serializer key-value bacula-console-director "director")

;;;
;;; Console Resource
;;;

(define-public-configuration bacula-console-resource
  (name
   resource-name
   "Name of this Console.")

  (director
   resource-name
   "Name of the Director resource to use.")

  (tls
   maybe-bacula-tls-settings
   "TLS Settings.")

  (password
   secrets-service-password
   "Director password."))

(define-section-serializer key-value bacula-console-resource "console")

(define-public-maybe bacula-console-resource)

;;;
;;; Bacula Console Configuration
;;;

(define-public-configuration bacula-console-configuration
  (director
   bacula-console-director
   "The Director resource.")

  (console
   maybe-bacula-console-resource
   "The Console resource.")

  (id
   maybe-account-id
   "Optional.  Name of this console."
   (serializer empty-serializer))

  (account
   maybe-account-configuration
   "Optional.  Console Account."
   (serializer empty-serializer)))


;;;
;;; Bacula Console Service
;;;

(define-record-type* <bacula-console-service>
  bacula-console-service
  make-bacula-console-service
  bacula-console-service?

  (id                   bacula-console-service-id)
  (secrets              bacula-console-service-secrets)
  (configuration        bacula-console-service-configuration))


(define (resolve-bacula-console-activation console)
  (match-records* ((console <bacula-console-service> id secrets)
                   (secrets <secrets-service> account output-file requirements)
                   (account <service-account> user group working-directory))
      (activation-task
       (id id)
       (requirements requirements)
       (action
        #~(let ((script-path #$(string-append working-directory "/bconsole"))
                (real-script-path #$(string-append working-directory "/.bconsole-real"))
                (shell #$(file-append bash "/bin/sh"))
                (bconsole #$(file-append bacula "/sbin/bconsole"))
                (uid (passwd:uid (getpw #$user)))
                (gid (group:gid (getgr #$group))))
            (call-with-output-file real-script-path
              (lambda (port)
                (format port "#!~a~%" shell)
                (format port "~a -c ~a~%" bconsole #$output-file)))
            (call-with-output-file script-path
              (lambda (port)
                (format port "#!~a~%" shell)
                (format port "/run/setuid-programs/sudo -u ~a ~a~%" #$user real-script-path)))
            (set-owner-and-mode real-script-path uid gid 448)
            (set-owner-and-mode script-path uid gid 493))))))


(define (resolve-bacula-console-id configuration)
  (match-records* ((configuration <bacula-console-configuration> id))
      (if (maybe-value-set? id)
          (symbol-append 'bacula-console- id)
          'bacula-console)))


(define* (resolve-bacula-console-secrets configuration #:key parameters)
  (match-records* ((configuration <bacula-console-configuration> id))
      (let* ((name (resolve-bacula-console-id configuration))
             (output-file (string-append (symbol->string name) ".conf")))
        (create-secrets
            (account-configuration name)
            (serialize output-file bacula-console configuration)))))


(define-public bacula-console-service-type
  (service-type (name 'bacula-console)
                (extensions
                 (list (service-extension activation-task-service-type
                                          resolve-bacula-console-activation)))
                (compose concatenate)
                (description "Bacula Console.")))


(define-parameterized-service bacula-console
  (record-type <bacula-console-configuration>)
  (parent secrets resolve-bacula-console-secrets)
  (compose list)
  (extend list)
  (resolve
   (lambda* (configuration #:key secrets parameters)
     (bacula-console-service
      (id (resolve-bacula-console-id configuration))
      (configuration configuration)
      (secrets secrets)))))
