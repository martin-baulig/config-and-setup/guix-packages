;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services bacula common)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 regex)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization key-value)
  #:use-module (baulig utils syntax-macros)
  #:use-module (baulig build config-utils)
  #:use-module (baulig build secrets-utils)
  #:export (list-of-bacula-messages-resource?
            serialize-list-of-bacula-messages-resource)
  #:export-syntax (bacula-file-message-resource))

;;;
;;; Constants

(define-string-constant key-value bacula-plugin-directory "@parameter:bacula@/lib")
(define-string-constant key-value bacula-working-directory "@parameter:working-directory@")
(define-string-constant key-value bacula-pid-directory "@parameter:pid-directory@")
(define-string-constant key-value bacula-query-file "@BACULA-ROOT@/etc/query.sql")


;;;
;;; Resource Name
;;;

(define-public (serialize-resource-name field-name value)
  (let ((field (config-field-name space field-name)))
    (format #f "~a = ~a~%" field value)))

(define-public (resource-name? value)
  (and (string? value)
       (string-match "^[A-Za-z]([A-Za-z0-9$_-]*)$" value)))

(define-public-maybe resource-name)


;;;
;;; Size
;;;

(define-public (serialize-size field-name value)
  (let ((field (config-field-name space field-name)))
    (format #f "~a = ~a~%" field value)))

(define-public (size? value)
  (or (integer? value)
      (and (string? value)
           (string-match "^[0-9]+(k|kb|m|mb|g|gb)?$" value))))

(define-public-maybe size)


;;;
;;; Time
;;;

(define-public (serialize-time field-name value)
  (let ((field (config-field-name space field-name)))
    (format #f "~a = ~a~%" field value)))

(define-public (time? value)
  (or (integer? value)
      (and (string? value)
           (or
            (eq? "0" value)
            (string-match "^1 (hour|day|week|month|quarter|year)$" value)
            (string-match "^[0-9]+ (seconds|minutes|hours|days|weeks|months|quarters|years)$" value)))))

(define-public-maybe time)


;;;
;;; ACL List
;;;

(define-public %acl-list-all "*all*")

(define-public (serialize-acl-list field-name value)
  (let ((field (config-field-name space field-name)))
    (format #f "~a = ~a~%" field
              (if (list? value)
                    (string-join value ", ")
                    value))))

(define-public (acl-list? value)
  (or (equal? value %acl-list-all)
      (and (list? value)
           (every (lambda (item)
                    (string-match "^\\.?[A-Za-z]([A-Za-z0-9$_\\*-]*)$" item))
                  value))))

(define-public-maybe acl-list)


;;;
;;; File List
;;;

(define-public (serialize-file-list field-name value)
  (let ((field (config-field-name space field-name)))
    (let* ((item-list (if (list? value) value (list value)))
             (items (map (lambda (item) (format #f "~a = ~s~%" field item)) item-list)))
        (string-join items "\n"))))

(define-public (file-list? value)
  (or (string? value)
      (and (list? value)
           (every string? value))))

(define-public-maybe file-list)


;;;
;;; Bacula Messages
;;;

(define-public (serialize-bacula-message-type-list field-name value)
  (let ((field (config-field-name space field-name)))
    (format #f "~a = ~a~%" field value)))

(define-public (bacula-message-type-list? value)
  (string? value))

(define-public-maybe bacula-message-type-list)


(define-public-configuration bacula-file-message
  (append?
   (boolean #t)
   "Whether to append to the file.")

  (file-name
   string
   "File name.")

  (type-list
   bacula-message-type-list
   "Message types."))

(define-public (serialize-bacula-file-message _ value)
  (match-records* ((value <bacula-file-message> append? file-name type-list))
      (format #f "~a = /var/log/bacula/~a = ~a~%"
              (if append? "append" "file")
              file-name type-list)))

(define-public-maybe bacula-file-message)


(define-public-configuration bacula-messages-resource
  (name
   resource-name
   "Messages resource name")

  (stdout
   maybe-bacula-message-type-list
   "Send the messages to standard output.")

  (stderr
   maybe-bacula-message-type-list
   "Send the messages to standard error.")

  (console
   maybe-bacula-message-type-list
   "Send the messages to the Bacula Console.
These messages will be held until the console program connects
to the Director.")

  (director
   maybe-bacula-message-type-list
   "Send the messages to Director that started the Job.")

  (syslog
   maybe-bacula-message-type-list
   "Send the messages to syslog using LOG_DAEMON and LOG_ERR.")

  (catalog
   maybe-bacula-message-type-list
   "Send the messages to the Catalog database.")

  (file
   maybe-bacula-file-message
   "Send the messages to a file."))


(define-section-serializer key-value bacula-messages-resource "messages")

(define-array-section-serializer key-value bacula-messages-resource "messages")

(define-syntax bacula-file-message-resource
  (syntax-rules (parent)
    ((_ (parent %parent) %name %file-name %type-list)
     (bacula-messages-resource
      (inherit %parent)
      (name %name)
      (file
       (bacula-file-message
        (file-name %file-name)
        (type-list %type-list)))))
    ((_ %name %file-name %type-list)
     (bacula-messages-resource
      (name %name)
      (file
       (bacula-file-message
        (file-name %file-name)
        (type-list %type-list)))))))
