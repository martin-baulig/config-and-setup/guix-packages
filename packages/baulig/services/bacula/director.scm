;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services bacula director)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (baulig packages bacula)
  #:use-module (baulig services account)
  #:use-module (baulig services parameters)
  #:use-module (baulig services secrets)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization syntax-macros)
  #:use-module (baulig services serialization key-value)
  #:use-module (baulig services serialization bacula)
  #:use-module (baulig services bacula common)
  #:use-module (baulig services bacula tls)
  #:use-module (baulig build config-utils)
  #:use-module (rnrs enums)
  #:use-module (srfi srfi-1))


;;;
;;; Bacula FileSet Options
;;;

(define-public-configuration bacula-fileset-options
  (compression
   maybe-bacula-compression-type
   "Compression type.")

  (signature
   maybe-bacula-signature-type
   "Signature type.")

  (sparse
   maybe-boolean
   "Whether to check for sparse files.")

  (noatime
   maybe-boolean
   "Whether to use O_NOATIME.")

  (wilddir
   maybe-file-list
   "Wild-card string to be applied to directory names only.")

  (wildfile
   maybe-file-list
   "Wild-card string to be applied to non-directories.")

  (regex
   maybe-file-list
   "POSIX extended regular expression to be applied to the full path.")

  (regexdir
   maybe-file-list
   "POSIX extended regular expression to be applied to directory names only.")

  (regexfile
   maybe-file-list
   "POSIX extended regular expression to be applied to non-directories.")

  (exclude
   maybe-boolean
   "When enabled, any files matched within the Options will be ignored."))


(define-section-serializer key-value bacula-fileset-options "options")

(define-public-maybe bacula-fileset-options)


;;;
;;; Bacula FileSet Include
;;;

(define-public-configuration bacula-fileset-include
  (options
   maybe-bacula-fileset-options
   "Options.")

  (file
   file-list
   "File or directory name."))

(define-section-serializer key-value bacula-fileset-include "include")


;;;
;;; Bacula FileSet Exclude
;;;

(define-public-configuration bacula-fileset-exclude
  (file
   file-list
   "File or directory name."))

(define-section-serializer key-value bacula-fileset-exclude "exclude")

(define-public-maybe bacula-fileset-exclude)


;;;
;;; Bacula FileSet
;;;

(define-public-configuration bacula-fileset-resource
  (name
   string
   "Name of this FileSet.")

  (include-list
   bacula-fileset-include
   "List of files to back up.")

  (exclude-list
   maybe-bacula-fileset-exclude
   "List of files to ignore."))


(define-section-serializer key-value bacula-fileset-resource "fileset")

(define-array-section-serializer key-value bacula-fileset-resource "fileset")

;;;
;;; Bacula Catalog
;;;

(define-public-configuration bacula-catalog-resource
  (name
   resource-name
   "Name of this Catalog.")

  (db-name
   string
   "Name of the Database.")

  (user
   string
   "User name to log into the database.")

  (password
   secrets-service-password
   "Catalog password."))


(define-section-serializer key-value bacula-catalog-resource "catalog")


;;;
;;; Bacula Pool
;;;

(define-string-constant key-value bacula-pool-type "Backup")

(define-public-configuration bacula-pool-resource
  (name
   resource-name
   "Name of this Pool.")

  (storage
   maybe-resource-name
   "Storage resource name.")

  (pool-type
   (bacula-pool-type-constant %bacula-pool-type)
   "Pool type.")

  (maximum-volumes
   maybe-integer
   "Maximum number of Volumes in the pool.")

  (maximum-volume-bytes
   maybe-size
   "Maximum number of bytes that can be written to each volume.")

  (maximum-volume-jobs
   maybe-integer
   "Maximum number of jobs each volume can be used for.")

  (auto-prune
   maybe-boolean
   "It true (default), Bacula will automatically prune Volumes after their Retention period.")

  (volume-retention
   maybe-time
   "The longest amount of time that Bacula will keep records associated with this Volume
in the Catalog database.")

  (recycle
   maybe-boolean
   "If true (default), Bacula will recycle (reuse) Purged Volumes.")

  (file-retention
   maybe-time
   "The length of time that Bacula will keep File records in the Catalog database.")

  (job-retention
   maybe-time
   "The length of time that Bacula will keep Job records in the Catalog database.")

  (recycle-oldest-volume
   maybe-boolean
   "Instruct the Director to recycle the oldest used Volume in the Pool when needed.")

  (purge-oldest-volume
   maybe-boolean
   "Instruct the Director to purge the oldest used Volume when needed, irrespective of retention periods.")

  (label-format
   (maybe-string
    "${Pool}-${Client}-${Level}-${JobId}-${NumVols}-${Year}_${Month:p/2/0/r}_${Day:p/2/0/r}")
   "Format of labels contained in the pool."))


(define-section-serializer key-value bacula-pool-resource "pool")

(define-array-section-serializer key-value bacula-pool-resource "pool")


;;;
;;; Bacula Storage
;;;

(define-public-configuration bacula-director-storage
  (name
   resource-name
   "Name of this Storage.")

  (address
   string
   "Address of the Storage daemon.")

  (sd-port
   maybe-integer
   "Port to use to contact the Storage daemon.")

  (device
   string
   "Device resource name.")

  (media-type
   string
   "Media type name.")

  (password
   secrets-service-password
   "Storage password.")

  (tls
   bacula-tls-settings
   "TLS Settings."))


(define-section-serializer key-value bacula-director-storage "storage")

(define-array-section-serializer key-value bacula-director-storage "storage")


;;;
;;; Bacula Client
;;;


(define-public-configuration bacula-director-client
  (name
   resource-name
   "Name of this Client.")

  (address
   string
   "Address of the File server daemon.")

  (fd-port
   maybe-integer
   "Port to use to contact the File daemon.")

  (allow-fd-connections
   maybe-boolean
   "When true, then the Director will accept incoming connections from the Client.")

  (catalog
   resource-name
   "Name of the Catalog Resource to be used for this Client.")

  (password
   secrets-service-password
   "Client password.")

  (tls
   bacula-tls-settings
   "TLS Settings."))


(define-section-serializer key-value bacula-director-client "client")

(define-array-section-serializer key-value bacula-director-client "client")


;;;
;;; Bacula Console
;;;


(define-public-configuration bacula-director-console
  (name
   resource-name
   "Name of this Console.")

  (job-acl
   maybe-acl-list
   "List of Job resource names that can be accessed by the Console.")

  (client-acl
   maybe-acl-list
   "List of Client resource names that can be accessed by the Console.")

  (storage-acl
   maybe-acl-list
   "List of Storage resource names that can be accessed by the Console.")

  (pool-acl
   maybe-acl-list
   "List of Pool resource names that can be accessed by the Console.")

  (file-set-acl
   maybe-acl-list
   "List of FileSet resource names that can be accessed by the Console.")

  (catalog-acl
   maybe-acl-list
   "List of Catalog resource names that can be accessed by the Console.")

  (command-acl
   maybe-acl-list
   "List of commands that can be executed by the Console.")

  (password
   secrets-service-password
   "Director password.")

  (tls-server
   bacula-tls-server-settings
   "TLS Settings."))


(define-section-serializer key-value bacula-director-console "console")

(define-array-section-serializer key-value bacula-director-console "console")


;;;
;;; Bacula Job
;;;

(define-public-configuration bacula-job-resource
  (name
   resource-name
   "The Job name.")

  (type
   maybe-bacula-job-type
   "The Job type.")

  (level
   maybe-bacula-job-level
   "The Job level.")

  (job-defs
   maybe-resource-name
   "JobDefs resource name.")

  (messages
   maybe-resource-name
   "Name of the Messages resource to be used.")

  (schedule
   maybe-resource-name
   "Name of the Schedule resource to be used.")

  (run-script
   maybe-bacula-run-script
   "Experimental.  Run Script.")

  (client
   resource-name
   "Name of the Client resource to be used.")

  (fileset
   resource-name
   "Name of the FileSet resource to be used.")

  (accurate
   maybe-boolean
   "When true, the File daemon knows exactly which files were present after
the last backup.  So it is able to handle deleted or renamed files.")

  (pool
   maybe-resource-name
   "Pool of Volumes where your data can be backed up.")

  (full-backup-pool
   maybe-resource-name
   "Optional.  Pool to be used for Full backups.")

  (differential-backup-pool
   maybe-resource-name
   "Optional.  Pool to be used for Differential backups.")

  (incremental-backup-pool
   maybe-resource-name
   "Optional.  Pool to be used for Incremental backups.")

  (virtual-backup-pool
   maybe-resource-name
   "Optional.  Pool to be used for Virtual backups."))


(define-section-serializer key-value bacula-job-resource "job")

(define-array-section-serializer key-value bacula-job-resource "job")

;;;
;;; Bacula JobDefs Resource
;;;

(define-public-configuration bacula-job-defs-resource
  (name
   resource-name
   "The Job name.")

  (type
   bacula-job-type
   "The Job type.")

  (level
   maybe-bacula-job-level
   "The Job level.")

  (messages
   resource-name
   "Name of the Messages resource to be used.")

  (client
   maybe-resource-name
   "Name of the Client resource to be used.")

  (fileset
   maybe-resource-name
   "Name of the FileSet resource to be used.")

  (accurate
   maybe-boolean
   "When true, the File daemon knows exactly which files were present after
the last backup.  So it is able to handle deleted or renamed files.")

  (pool
   maybe-resource-name
   "Pool of Volumes where your data can be backed up.")

  (full-backup-pool
   maybe-resource-name
   "Optional.  Pool to be used for Full backups.")

  (differential-backup-pool
   maybe-resource-name
   "Optional.  Pool to be used for Differential backups.")

  (incremental-backup-pool
   maybe-resource-name
   "Optional.  Pool to be used for Incremental backups.")

  (virtual-backup-pool
   maybe-resource-name
   "Optional.  Pool to be used for Virtual backups."))


(define-section-serializer key-value bacula-job-defs-resource "jobdefs")

(define-array-section-serializer key-value bacula-job-defs-resource "jobdefs")


;;;
;;; Bacula Director
;;;

(define-public-configuration bacula-director-resource
  (name
   resource-name
   "Director name.")

  (tls-server
   bacula-tls-server-settings
   "TLS Settings.")

  (dir-address
   maybe-ipv4-address
   "IP Address to bind to.")

  (dir-port
   maybe-integer
   "Port number to listen on.")

  (password
   secrets-service-password
   "Director password.")

  (description
   maybe-string
   "Description of the Director that will be displayed in the GUI.")

  (working-directory
   (bacula-working-directory-constant %bacula-working-directory)
   "Bacula working directory.")

  (pid-directory
   (bacula-pid-directory-constant %bacula-pid-directory)
   "Bacula PID directory.")

  (query-file
   (bacula-query-file-constant %bacula-query-file)
   "Bacula query file.")

  (messages
   resource-name
   "Name of the Messages resource to be used."))

(define-section-serializer key-value bacula-director-resource "director")


;;;
;;; Bacula Configuration
;;;

(define-public-configuration bacula-director-configuration
  (messages
   list-of-bacula-messages-resource
   "Bacula Messages Configuration")

  (catalog
   bacula-catalog-resource
   "Bacula Catalog Configuration")

  (client
   list-of-bacula-director-client
   "Bacula Client Configuration")

  (console
   list-of-bacula-director-console
   "Bacula Console Configuration")

  (job
   list-of-bacula-job-resource
   "Bacula Job Resources")

  (job-defs
   (list-of-bacula-job-defs-resource '())
   "Bacula JobDefs Resources")

  (schedule
   (list-of-bacula-schedule '())
   "Bacula Schedule Resources")

  (pool
   list-of-bacula-pool-resource
   "Bacula Pool Configuration")

  (storage
   list-of-bacula-director-storage
   "Bacula Storage Configuration")

  (fileset
   list-of-bacula-fileset-resource
   "Bacula FileSet Configuration")

  (director
   bacula-director-resource
   "Bacula Director Configuration")

  (id
   maybe-account-id
   "Optional.  Unique ID identifying this storage."
   (serializer empty-serializer))

  (account
   maybe-account-configuration
   "Optional.  Service account configuration."
   (serializer empty-serializer)))


;;;
;;; Bacula Director Service
;;;

(define-record-type* <bacula-director-service>
  bacula-director-service
  make-bacula-director-service
  bacula-director-service?

  (id                   bacula-director-service-id)
  (secrets              bacula-director-service-secrets)
  (configuration        bacula-director-service-configuration))


(define* (resolve-bacula-director configuration #:key secrets parameters)
  (match-records* ((configuration <bacula-director-configuration> id account))
      (bacula-director-service
       (configuration configuration)
       (id (cond ((maybe-value-set? id)
                  (symbol-append 'bacula-director- id))
                 (else 'bacula-director)))
       (secrets secrets))))


(define* (resolve-bacula-director-secrets configuration #:key parameters)
  (match-records* ((configuration <bacula-director-configuration> id account))
      (let* ((default (cond ((maybe-value-set? id)
                             (symbol-append 'bacula-director- id))
                            (else 'bacula-director)))
             (output-file (string-append (symbol->string default) ".conf"))
             (real-account (with-default
                            account
                            (account-configuration (id default) (create-account? #f))))
             (daemon-defaults (daemon-account-configuration
                               (pid-file "bacula-dir.9101.pid")))
             (daemon-account (make-daemon-account real-account daemon-defaults)))
        (create-secrets
            (daemon-account-expression daemon-account)
            (serialize output-file bacula-director configuration)))))


(define (resolve-bacula-director-shepherd service)
  (match-records* ((service <bacula-director-service> secrets configuration)
                   (secrets <secrets-service> account output-file requirements)
                   (account <service-account> user group supplementary-groups
                            working-directory log-file-path pid-file-path))
      (list
       (shepherd-service
        (provision '(bacula-director))
        (documentation "Bacula Director Service")
        (requirement `(networking ,@requirements))
        (respawn? #f)
        (start #~(make-forkexec-constructor
                  (list #$(file-append bacula "/sbin/bacula-dir")
                        "-c" #$output-file "-v")
                  #:directory #$working-directory
                  #:user #$user
                  #:group #$group
                  #:supplementary-groups (list #$@supplementary-groups)
                  #:log-file #$log-file-path
                  #:pid-file #$pid-file-path))
        (stop #~(make-kill-destructor))))))


(define-public bacula-director-service-type
  (service-type (name 'bacula-director)
                (extensions
                 (list (service-extension shepherd-root-service-type
                                          resolve-bacula-director-shepherd)))
                (compose concatenate)
                (description "Bacula Director Service")))


(define-parameterized-service bacula-director
  (record-type <bacula-director-configuration>)
  (parent secrets resolve-bacula-director-secrets)
  (resolve resolve-bacula-director))
