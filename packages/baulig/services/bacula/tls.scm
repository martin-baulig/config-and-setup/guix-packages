;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services bacula tls)
  #:use-module (baulig services bacula common)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization syntax-macros)
  #:use-module (baulig services serialization key-value))


;;;
;;; Shared TLS Settings
;;;

(define-public-configuration bacula-tls-settings
  (tls-enable
   true-constant
   "Enable TLS Support.")

  (tls-require
   true-constant
   "Require TLS Encryption.")

  (tls-certificate
   secrets-service-blob
   "Full path name of a PEM encoded TLS certificate.")

  (tls-key
   secrets-service-blob
   "Full path name of a PEM encoded TLS private key.")

  (tls-ca-certificate-file
   maybe-secrets-service-blob
   "Full path name of a PEM encoded TLS CA certificate."))

(define-section-serializer key-value-fragment bacula-tls-settings)

(define-public-maybe bacula-tls-settings)


;;;
;;; TLS Server Settings
;;;

(define-public-configuration bacula-tls-server-settings
  (shared
   bacula-tls-settings
   "Shared TLS Settings.")

  (tls-verify-peer
   true-constant
   "Verify client certificate.")

  (tls-dh-file
   secrets-service-blob
   "Path to PEM encoded Diffie-Hellman parameter file."))


(define-section-serializer key-value-fragment bacula-tls-server-settings)
