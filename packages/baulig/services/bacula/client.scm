;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services bacula client)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (gnu packages base)
  #:use-module (baulig packages bacula)
  #:use-module (baulig services account)
  #:use-module (baulig services activation)
  #:use-module (baulig services parameters)
  #:use-module (baulig services secrets)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization syntax-macros)
  #:use-module (baulig services serialization key-value)
  #:use-module (baulig services bacula common)
  #:use-module (baulig services bacula pki)
  #:use-module (baulig services bacula tls)
  #:use-module (baulig build config-utils)
  #:use-module (srfi srfi-1))


;;;
;;; Director Resource
;;;

(define-public-configuration bacula-client-director
  (name
   resource-name
   "Name of the Director allowed to connect to this File daemon.")

  (tls
   bacula-tls-settings
   "TLS Settings.")

  (password
   secrets-service-password
   "Director password.")

  (connect-to-director
   maybe-boolean
   "When set to true, the Client will contact the director according to the Schedule rules."))

(define-section-serializer key-value bacula-client-director "director")

(define-array-section-serializer key-value bacula-client-director "director")


;;;
;;; Client Resource
;;;

(define-public-configuration bacula-client-resource
  (name
   resource-name
   "Name of this Client resource.")

  (working-directory
   (bacula-working-directory-constant %bacula-working-directory)
   "Bacula working directory.")

  (pid-directory
   (bacula-pid-directory-constant %bacula-pid-directory)
   "Bacula PID directory.")

  (fd-address
   maybe-ipv4-address
   "IP Address to bind to.")

  (fd-port
   maybe-integer
   "Port number to listen on.")

  (auto-prune
   maybe-boolean
   "It true (default), Bacula will automatically prune Volumes after their Retention period.")

  (volume-retention
   maybe-time
   "The longest amount of time that Bacula will keep records associated with this Volume
in the Catalog database.")

  (file-retention
   maybe-time
   "The length of time that Bacula will keep File records in the Catalog database.")

  (job-retention
   maybe-time
   "The length of time that Bacula will keep Job records in the Catalog database.")

  (tls
   bacula-tls-settings
   "TLS Settings.")

  (pki
   maybe-bacula-pki-settings
   "Optional.  Data Encryption."))

(define-section-serializer key-value bacula-client-resource "client")

;;;
;;; Bacula Client Configuration
;;;

(define-public-configuration bacula-client-configuration
  (messages
   bacula-messages-resource
   "Bacula Messages Configuration")

  (director
   list-of-bacula-client-director
   "The Director resource.")

  (client
   bacula-client-resource
   "The Client resource.")

  (id
   maybe-account-id
   "Optional.  Unique ID identifying this client."
   (serializer empty-serializer))

  (foreign?
   (boolean #f)
   "If true, then we only generate the configuration, to be copied to another machine."
   (serializer empty-serializer)))


;;;
;;; Bacula Client Service
;;;

(define-record-type* <bacula-client-service>
  bacula-client-service
  make-bacula-client-service
  bacula-client-service?

  (name                 bacula-client-service-name)
  (foreign?             bacula-client-service-foreign?)
  (secrets              bacula-client-service-secrets)
  (configuration        bacula-client-service-configuration))


(define (resolve-bacula-client-name configuration)
  (match-records* ((configuration <bacula-client-configuration> id foreign?))
      (cond (foreign?
             symbol-append 'bacula-external- id)
            ((maybe-value-set? id)
             (symbol-append 'bacula-client- id))
            (else 'bacula-client))))


(define* (resolve-bacula-client configuration #:key secrets parameters)
  (match-records* ((configuration <bacula-client-configuration> id foreign?))
      (bacula-client-service
       (configuration configuration)
       (name (resolve-bacula-client-name configuration))
       (foreign? foreign?)
       (secrets secrets))))


(define* (resolve-bacula-client-secrets configuration #:key parameters)
  (match-records* ((configuration <bacula-client-configuration> id foreign?))
      (let* ((template (serialize-configuration
                        configuration bacula-client-configuration-fields))
             (name (resolve-bacula-client-name configuration)))
        (if foreign?
            (create-secrets
                (account-configuration name (create-account? #f))
                (serialize "bacula-fd.conf" bacula-client configuration)
              (options root-owned-secrets)
              (extra-parameters (external-blob-path "/etc/bacula")
                                (pid-directory "/var/run")
                                (working-directory "/var/bacula")))
            (create-secrets
                (daemon-account name
                                (create-account? #f)
                                (daemon (pid-file "bacula-fd.9102.pid")))
                (serialize (string-append (symbol->string name) ".conf") bacula-client configuration)
              (options root-owned-secrets))))))


(define (resolve-bacula-client-shepherd service)
  (match-records* ((service <bacula-client-service> secrets foreign? configuration)
                   (secrets <secrets-service> account output-file requirements)
                   (account <service-account> working-directory log-file-path pid-file-path))
      (if foreign?
          '()
          (list
           (shepherd-service
            (provision '(bacula-client))
            (documentation "Bacula Client Service")
            (requirement `(networking ,@requirements))
            (respawn? #f)
            (start #~(make-forkexec-constructor
                      (list #$(file-append bacula "/sbin/bacula-fd")
                            "-config" #$output-file)
                      #:directory #$working-directory
                      #:log-file #$log-file-path
                      #:pid-file #$pid-file-path))
            (stop #~(make-kill-destructor)))))))


(define-public bacula-client-service-type
  (service-type (name 'bacula-client)
                (extensions
                 (list (service-extension shepherd-root-service-type
                                          resolve-bacula-client-shepherd)))
                (compose concatenate)
                (description "Bacula Client Service")))


(define-parameterized-service bacula-client
  (record-type <bacula-client-configuration>)
  (parent secrets resolve-bacula-client-secrets)
  (compose list)
  (extend list)
  (resolve resolve-bacula-client))
