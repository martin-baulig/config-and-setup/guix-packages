;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services bacula pki)
  #:use-module (baulig services bacula common)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization syntax-macros)
  #:use-module (baulig services serialization key-value))


;;;
;;; Data Encryption
;;;

(define-public-configuration bacula-pki-settings
  (pki-signatures
   true-constant
   "Enable Data Signing.")

  (pki-encryption
   true-constant
   "Enable Data Encryption.")

  (pki-keypair
   secrets-service-blob
   "Secrets Service Blob containing the Public and Private key pair.")

  (pki-master-key
   secrets-service-blob
   "Secrets Service Blob containing the Master Public Key"))

(define-section-serializer key-value-fragment bacula-pki-settings)

(define-public-maybe bacula-pki-settings)
