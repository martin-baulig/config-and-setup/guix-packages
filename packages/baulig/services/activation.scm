;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services activation)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (baulig utils syntax-macros)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:use-module (srfi srfi-1)
  #:use-module (rnrs enums)
  #:export (activation-task))


;;;
;;; Record types
;;;


(define-record-type* <activation-task-service>
  activation-task-service
  make-activation-task-service
  activation-task-service?

  (tasks                activation-service-tasks          (default '())))


(define activation-kind)
(make-enumeration '(action external script))


(define-record-type* <activation-task>
  activation-task
  make-activation-task
  activation-task?

  (id                   activation-task-id)
  (extensions           activation-task-extensions        (default '()))
  (modules              activation-task-modules           (default '()))
  (requirements         activation-task-requirements      (default '()))
  (kind                 activation-task-kind              (default 'action))
  (action               activation-task-action))



(define (extend-activation-tasks config new)
  (match-records* ((config <activation-task-service> tasks))
      (activation-task-service
       (tasks
        (fold-right
         (lambda (task current)
           (match-records* ((task <activation-task> id))
               (when (assoc-ref current id)
                 (error
                  (format #f
                          "Activation task with id '~a' already exists: ~a"
                          id task)))
             (acons id task current)))
         '() new)))))


(define (compose-activation-tasks value)
  (cond ((activation-task? value)
         (list value))
        ((list? value)
         (fold-right
          (lambda (item current)
            (cond ((activation-task? item)
                   (cons item current))
                  ((and (list? item) (every activation-task? item))
                   (append current item))
                  (else
                   (throw 'wrong-type-arg item))))
          '() value))
        (else
         (throw 'wrong-type-arg value))))


(define (resolve-shepherd-services configuration)
  (match-records* ((configuration <activation-task-service> tasks))

    (define (make-shepherd task)
      (match-records* ((task <activation-task> id action extensions modules
                             requirements kind))

        (define script
          (program-file
           (format #f "activation-action-~a" id)
           (with-extensions extensions
             (with-imported-modules modules
               #~(begin
                   (use-modules #$@modules)
                   (format #t "Executing activation action: ~a~%" '#$id)
                   #$action
                   (format #t "Done executing activation action: ~a~%" '#$id))))))

        (define execute
          (with-extensions extensions
            (with-imported-modules modules
              #~(begin
                  (use-modules #$@modules)
                  (lambda ()
                    (format #t "Executing activation action: ~a~%" '#$id)
                    #$action
                    (format #t "Done executing activation action: ~a~%" '#$id))))))

        (define constructor
          (case kind
            ((action) execute)
            ((external)
             #~(make-forkexec-constructor
                (list #$script)
                #:log-file "/var/log/activation.log"))
            ((script)
             #~(make-forkexec-constructor
                (list #$action)
                #:log-file "/var/log/activation.log"))
            (else
             (throw 'wrong-type-arg kind))))

        (let* ((provision `(,(symbol-append 'activation-task- id)))
               (requirement `(user-processes ,@requirements)))
          (shepherd-service
           (provision provision)
           (documentation (format #f "Activation Task ~a" id))
           (requirement requirement)
           (respawn? #f)
           (one-shot? #t)
           (start constructor)))))

    (map make-shepherd (map cdr tasks))))


(define-public activation-task-service-type
  (service-type (name 'activation-taks-service)
                (extensions
                 (list
                  (service-extension shepherd-root-service-type
                                     resolve-shepherd-services)))
                ;; Accept either a single <activation-task> instance
                ;; or a list of such.
                (compose compose-activation-tasks)
                (extend extend-activation-tasks)
                (description
                 "Activation Task Service")
                (default-value (activation-task-service))))

