;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services prometheus)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (baulig packages prometheus)
  #:use-module (baulig services account)
  #:use-module (baulig services parameters)
  #:use-module (baulig services secrets)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization syntax-macros)
  #:use-module (baulig services serialization yaml)
  #:use-module (baulig services serialization prometheus)
  #:use-module (baulig build config-utils)
  #:use-module (rnrs enums)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (web uri))


;;;
;;; Prometheus Global Section
;;;

(define-public-configuration prometheus-global-section
  (scrape-interval
   maybe-prometheus-duration
   "How frequently to scrape targets by default."))

(define-section-serializer yaml prometheus-global-section "global")

(define-public-maybe prometheus-global-section)


;;;
;;; Prometheus Configuration
;;;

(define-public-configuration prometheus-configuration
  (global
   maybe-prometheus-global-section
   "Global section.")

  (scrape-configs
   (list-of-prometheus-scrape-configuration '())
   "Prometheus scrape configurations.")

  (web
   prometheus-web-configuration
   "Web configuration."
   (serializer empty-serializer))

  (listen-address
   maybe-ipv4-address
   "Address to listen on."
   (serializer empty-serializer))

  (listen-port
   (integer 9090)
   "Port to listen on."
   (serializer empty-serializer))

  (external-url
   maybe-uri
   "The URL under which Prometheus is externally reachable."
   (serializer empty-serializer))

  (data-directory
   path-name
   "Prometheus Storage Directory."
   (serializer empty-serializer))

  (storage-retention-time
   maybe-string
   "When to remove old data."
   (serializer empty-serializer))

  (storage-retention-size
   maybe-string
   "Maximum number of bytes of storage to retain."
   (serializer empty-serializer)))

(define-section-serializer yaml prometheus-configuration "prometheus")


;;;
;;; Prometheus Service
;;;


(define-record-type* <prometheus-service>
  prometheus-service
  make-prometheus-service
  prometheus-service?

  (secrets              prometheus-service-secrets)
  (configuration        prometheus-service-configuration))


(define* (resolve-prometheus configuration #:key secrets parameters)
  (match-records* ((configuration <prometheus-configuration>))
      (prometheus-service
       (configuration configuration)
       (secrets secrets))))


(define* (resolve-prometheus-secrets configuration #:key parameters)
  (match-records* ((configuration <prometheus-configuration> web))
      (create-secrets
          (daemon-account 'prometheus)
          (serialize "prometheus.yaml" prometheus configuration)
        (extra-sections
         (list
          (create-secrets-section
              (serialize "prometheus-web.yaml" prometheus-web web))))
        (extra-parameters gexp #~((prometheus . #$prometheus))))))


(define (resolve-prometheus-shepherd service)
  (match-records* ((service <prometheus-service> secrets configuration)
                   (configuration <prometheus-configuration> external-url
                                  listen-address listen-port data-directory
                                  storage-retention-time storage-retention-size)
                   (secrets <secrets-service> account private-directory output-file requirements)
                   (account <service-account> user group supplementary-groups
                            working-directory log-file-path pid-file-path))
      (let* ((args `("--config.file" ,output-file
                     "--storage.tsdb.path" ,data-directory
                     ,@(with-default-list storage-retention-time
                                          (string-append "--storage.tsdb.retention.time=" storage-retention-time))
                     ,@(with-default-list storage-retention-size
                                          (string-append "--storage.tsdb.retention.size=" storage-retention-size))
                     "--web.config.file" ,(string-append private-directory "/prometheus-web.yaml")
                     ,@(with-default-list external-url
                                          (format #f "--web.external-url=~a" (uri->string external-url)))
                     ,@(with-default-list listen-address function
                                          (cut format #f "--web.listen-address=~a:~a" <> listen-port)))))
        (list
         (shepherd-service
          (provision '(prometheus))
          (documentation "Prometheus Service")
          (requirement `(networking openvpn ,@requirements))
          (respawn? #f)
          (start #~(make-forkexec-constructor
                    (list #$(file-append prometheus "/prometheus") #$@args)
                    #:directory #$working-directory
                    #:user #$user
                    #:group #$group
                    #:supplementary-groups (list #$@supplementary-groups)
                    #:log-file #$log-file-path))
          (stop #~(make-kill-destructor)))))))


(define-public prometheus-service-type
  (service-type (name 'prometheus)
                (extensions
                 (list (service-extension shepherd-root-service-type
                                          resolve-prometheus-shepherd)))
                (compose identity)
                (description "Prometheus Service")))


(define-parameterized-service prometheus
  (record-type <prometheus-configuration>)
  (parent secrets resolve-prometheus-secrets)
  (resolve resolve-prometheus))
