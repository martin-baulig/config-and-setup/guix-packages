 ;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
 ;;;
 ;;; This file is NOT part of GNU Guix.
 ;;;
 ;;; This program is free software: you can redistribute it and/or modify
 ;;; it under the terms of the GNU Affero General Public License as
 ;;; published by the Free Software Foundation, either version 3 of the
 ;;; License, or (at your option) any later version.

 ;;; This program is distributed in the hope that it will be useful,
 ;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;; GNU Affero General Public License for more details.

 ;;; You should have received a copy of the GNU Affero General Public License
 ;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services serialization syntax-macros)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (guix derivations)
  #:use-module (guix monads)
  #:use-module (guix records)
  #:use-module (guix build utils)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-26)
  #:use-module (web uri)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig build config-utils)
  #:use-module (baulig build secrets-utils)
  #:export (define-standard-serializers))


(define* (make-serializer-functions x #:key separator line-ending make-field-name
                                    format-string)
  (with-syntax ((serialize-boolean (datum->syntax x 'serialize-boolean))
                (serialize-boolean-yes-no (datum->syntax x 'serialize-boolean-yes-no))
                (serialize-boolean-on-off (datum->syntax x 'serialize-boolean-on-off))
                (serialize-true-or-nothing (datum->syntax x 'serialize-true-or-nothing))
                (serialize-string-name (datum->syntax x 'serialize-string))
                (serialize-integer-name (datum->syntax x 'serialize-integer))
                (serialize-false-constant (datum->syntax x 'serialize-false-constant))
                (serialize-true-constant (datum->syntax x 'serialize-true-constant))
                (serialize-account-id (datum->syntax x 'serialize-account-id))
                (serialize-user-name (datum->syntax x 'serialize-user-name))
                (serialize-path-name (datum->syntax x 'serialize-path-name))
                (serialize-file-name (datum->syntax x 'serialize-file-name))
                (serialize-host-name (datum->syntax x 'serialize-host-name))
                (serialize-ipv4-address (datum->syntax x 'serialize-ipv4-address))
                (serialize-uri (datum->syntax x 'serialize-uri))
                (serialize-secrets-service-password (datum->syntax x 'serialize-secrets-service-password))
                (serialize-secrets-service-quoted-text (datum->syntax x 'serialize-secrets-service-quoted-text))
                (serialize-secrets-service-text (datum->syntax x 'serialize-secrets-service-text))
                (serialize-secrets-service-blob (datum->syntax x 'serialize-secrets-service-blob)))
    #`(begin
        (define-public (serialize-boolean field-name value)
          (let ((field (#,make-field-name field-name))
                (value (if value "true" "false")))
            (format #f "~a ~a~a~a~%" field #,separator value #,line-ending)))

        (define-public (serialize-boolean-yes-no field-name value)
          (let ((field (#,make-field-name field-name))
                (value (if value "yes" "no")))
            (format #f "~a ~a~a~a~%" field #,separator value #,line-ending)))

        (define-public (serialize-boolean-on-off field-name value)
          (let ((field (#,make-field-name field-name))
                (value (if value "on" "off")))
            (format #f "~a ~a~a~a~%" field #,separator value #,line-ending)))

        (define-public (serialize-true-or-nothing field-name value)
          (let ((field (#,make-field-name field-name)))
            (if value
                (format #f "~a~a~%" field #,line-ending)
                "")))

        (define-public (serialize-string-name field-name value)
          (let ((field (#,make-field-name field-name)))
            (format #f "~a ~a~a~a~%" field #,separator
                    (#,format-string value #:quote? #t)
                    #,line-ending)))

        (define-public (serialize-integer-name field-name value)
          (let ((field (#,make-field-name field-name)))
            (format #f "~a ~a~a~a~%" field #,separator (number->string value)
                    #,line-ending)))

        (define-public (serialize-false-constant field-name value)
          (let ((field (#,make-field-name field-name)))
            (format #f "~a ~afalse~a~%" field #,separator #,line-ending)))

        (define-public (serialize-true-constant field-name value)
          (let ((field (#,make-field-name field-name)))
            (format #f "~a ~atrue~a~%" field #,separator #,line-ending)))

        (define-public (serialize-account-id field-name value)
          (let ((field (#,make-field-name field-name)))
            (format #f "~a ~a~a~a~%" field #,separator (#,format-string value) #,line-ending)))

        (define-public (serialize-user-name field-name value)
          (let ((field (#,make-field-name field-name)))
            (format #f "~a ~a~a~a~%" field #,separator (#,format-string value) #,line-ending)))

        (define-public (serialize-path-name field-name value)
          (let ((field (#,make-field-name field-name)))
            (format #f "~a ~a~a~a~%" field #,separator (#,format-string value) #,line-ending)))

        (define-public (serialize-file-name field-name value)
          (let ((field (#,make-field-name field-name)))
            (format #f "~a ~a~a~a~%" field #,separator (#,format-string value) #,line-ending)))

        (define-public (serialize-host-name field-name value)
          (let ((field (#,make-field-name field-name)))
            (format #f "~a ~a~a~a~%" field #,separator (#,format-string value) #,line-ending)))

        (define-public (serialize-uri field-name value)
          (let ((field (#,make-field-name field-name)))
            (format #f "~a ~a~a~a~%" field #,separator
                    (#,format-string (uri->string value))
                    #,line-ending)))

        (define-public (serialize-ipv4-address field-name value)
          (let ((field (#,make-field-name field-name)))
            (format #f "~a ~a~a~a~%" field #,separator (#,format-string value) #,line-ending)))

        (define-public (serialize-secrets-service-password field-name value)
          (let ((field (#,make-field-name field-name)))
            (format #f "~a ~a~a~a~%" field #,separator
                    (#,format-string (format #f "@password:~a@" value) #:quote? #t)
                    #,line-ending)))

        (define-public (serialize-secrets-service-quoted-text field-name value)
          (let ((field (#,make-field-name field-name)))
            (format #f "~a ~a~a~a~%" field #,separator
                    (#,format-string (format #f "@text:~a@" value) #:quote? #t)
                    #,line-ending)))

        (define-public (serialize-secrets-service-text field-name value)
          (let ((field (#,make-field-name field-name)))
            (format #f "~a ~a~a~a~%" field #,separator
                    (#,format-string (format #f "@text:~a@" value))
                    #,line-ending)))

        (define-public (serialize-secrets-service-blob field-name value)
          (let ((field (#,make-field-name field-name)))
            (format #f "~a ~a~a~a~%" field #,separator
                    (#,format-string (format #f "@blob:~a@" value))
                    #,line-ending))))))


(define-syntax define-standard-serializers
  (lambda (x)
    (let* ((format-string #'(lambda* (value #:key quote?)
                             (if quote?
                                 (format #f "~s" value)
                                 (format #f "~a" value))))
           (format-single #'(lambda* (value #:key quote?)
                             (format #f "'~a'" value))))

      (syntax-case x (key-value
                      dash-space
                      underscore-key-value
                      underscore-single-quotes
                      underscore-space-semicolon
                      yaml)
        ((_ key-value)
         (make-serializer-functions x
                                    #:separator "= "
                                    #:line-ending ""
                                    #:format-string format-string
                                    #:make-field-name
                                    #'(lambda (name) (config-field-name space name))))
        ((_ dash-space)
         (make-serializer-functions x
                                    #:separator ""
                                    #:line-ending ""
                                    #:format-string format-string
                                    #:make-field-name
                                    #'(lambda (name) (config-field-name dash name))))
        ((_ underscore-key-value)
         (make-serializer-functions x
                                    #:separator "= "
                                    #:line-ending ""
                                    #:format-string format-string
                                    #:make-field-name
                                    #'(lambda (name) (config-field-name underscore name))))
        ((_ underscore-single-quotes)
         (make-serializer-functions x
                                    #:separator ""
                                    #:line-ending ""
                                    #:format-string format-single
                                    #:make-field-name
                                    #'(lambda (name) (config-field-name underscore name))))
        ((_ underscore-space-semicolon)
         (make-serializer-functions x
                                    #:separator ""
                                    #:line-ending ";"
                                    #:format-string format-string
                                    #:make-field-name
                                    #'(lambda (name) (config-field-name underscore name))))
        ((_ yaml)
         (make-serializer-functions x
                                    #:separator ": "
                                    #:line-ending ""
                                    #:format-string format-string
                                    #:make-field-name
                                    #'(lambda (name) (config-field-name yaml name))))))))


(define-public (boolean-yes-no? value)
  (boolean? value))

(define-public (boolean-on-off? value)
  (boolean? value))

(define-public (true-or-nothing? value)
  (boolean? value))

(define-public (false-constant? value)
  (or (eq? value #f)
      (not (maybe-value-set? value))))

(define-public (true-constant? value)
  (or (eq? value #t)
      (not (maybe-value-set? value))))


;;;
;;; Account Names and Paths
;;;

(define-public (account-id? value)
  (and (symbol? value)
       (string-match %name-regex (symbol->string value))))

(define-public (user-name? value)
  (and (string? value)
       (string-match %name-regex value)))

(define-public (path-name? value)
  (and (string? value)
       (string-match %path-regex value)))

(define-public (file-name? value)
  (and (string? value)
       (string-match %file-regex value)))

(define-public (host-name? value)
  (and (string? value)
       (string-match %file-regex value)))

(define-public (ipv4-address? value)
  (and (string? value)
       (string-match %ipv4-address-regex value)))


(define %name-regex "^[A-Za-z][A-Za-z0-9]*(-[A-Za-z0-9]+)*$")
(define %path-regex "^(/[A-Za-z][A-Za-z0-9]*(-[A-Za-z0-9]+)*)+$")
(define %file-regex "^([A-Za-z][A-Za-z0-9]*([.-][A-Za-z0-9]+)*)+$")
(define %ipv4-address-regex "^([0-9]+)\\.([0-9]+)\\.([0-9]+)\\.([0-9]+)$")


;;;
;;; Secrets Service
;;;

(define-public (secrets-service-password? value)
  (secrets-file-key? value))


(define-public (secrets-service-quoted-text? value)
  (secrets-file-key? value))


(define-public (secrets-service-text? value)
  (secrets-file-key? value))


(define-public (secrets-service-blob? value)
  (secrets-file-key? value))

