 ;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
 ;;;
 ;;; This file is NOT part of GNU Guix.
 ;;;
 ;;; This program is free software: you can redistribute it and/or modify
 ;;; it under the terms of the GNU Affero General Public License as
 ;;; published by the Free Software Foundation, either version 3 of the
 ;;; License, or (at your option) any later version.

 ;;; This program is distributed in the hope that it will be useful,
 ;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;; GNU Affero General Public License for more details.

 ;;; You should have received a copy of the GNU Affero General Public License
 ;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services serialization key-value)
  #:use-module (guix build utils)
  #:use-module (web uri)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization syntax-macros)
  #:use-module (baulig build config-utils)
  #:use-module (baulig build secrets-utils)
  #:re-export (boolean?
               boolean-yes-no?
               integer?
               string?
               user-name?
               path-name?
               file-name?
               account-id?
               secrets-service-blob?
               ipv4-address?
               uri?))


(define-standard-serializers key-value)

(define-public-maybe boolean)
(define-public-maybe boolean-yes-no)
(define-public-maybe integer)
(define-public-maybe string)

(define-public-maybe user-name)
(define-public-maybe path-name)
(define-public-maybe file-name)

(define-public-maybe account-id)

(define-public-maybe secrets-service-blob)

(define-public-maybe ipv4-address)
(define-public-maybe uri)
