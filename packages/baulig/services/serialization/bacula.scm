;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services serialization bacula)
  #:use-module (guix records)
  #:use-module (gnu services)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization syntax-macros)
  #:use-module (baulig services serialization key-value)
  #:use-module (baulig build config-utils)
  #:use-module (baulig utils syntax-macros)
  #:use-module (rnrs enums)
  #:use-module (srfi srfi-1)
  #:export-syntax (bacula-schedule))


;;;
;;; Bacula Compression
;;;

(define-public bacula-compression-type
  (make-enumeration '(lzo gzip gzip1 gzip2 gzip3 gzip4 gzip5 gzip6 gzip7 gzip8 gzip9)))

(define-enum-serializer key-value bacula-compression-type)

(define-public-maybe bacula-compression-type)


;;;
;;; Bacula Signature
;;;

(define-public bacula-signature-type
  (make-enumeration '(sha1 sha256 sha512 md5)))

(define-enum-serializer key-value bacula-signature-type)

(define-public-maybe bacula-signature-type)


;;;
;;; Bacula Job Type
;;;

(define-public bacula-job-type (make-enumeration '(backup restore verify admin)))

(define-enum-serializer key-value bacula-job-type)

(define-public-maybe bacula-job-type)


;;;
;;; Bacula Job Level
;;;

(define-public bacula-job-level
  (make-enumeration '(full incremental differential virtual-full
                      init-catalog catalog data volume-to-catalog
                      disk-to-catalog)))

(define-enum-serializer key-value bacula-job-level)

(define-public-maybe bacula-job-level)


;;;
;;; Bacula Schedule
;;;

(define-record-type* <bacula-run-spec>
  bacula-run-spec
  make-bacula-run-spec
  bacula-run-spec?

  (level                bacula-run-spec-level)
  (interval             bacula-run-spec-interval))


(define (format-bacula-run-spec value)
  (match-records* ((value <bacula-run-spec> level interval))
      (if level
          (format #f "    Run = Level=~a ~a~%" level interval)
          (format #f "    Run = ~a~%" interval))))


(define-public (serialize-bacula-run-spec _ value)
  (format-bacula-run-spec value))


(define-syntax bacula-schedule-run
  (syntax-rules (level)
    ((_ (level override-level interval))
     (make-bacula-run-spec 'override-level interval))
    ((_ (interval))
     (make-bacula-run-spec #f interval))))


(define-record-type* <bacula-schedule-spec>
  bacula-schedule-spec
  make-bacula-schedule-spec
  bacula-schedule-spec?

  (name                 bacula-schedule-spec-name)
  (run-specs            bacula-schedule-spec-run-specs))


(define-syntax bacula-schedule
  (syntax-rules ()
    ((_ name run-specs ...)
     (make-bacula-schedule-spec name (list (bacula-schedule-run run-specs) ...)))))


(define-public (bacula-schedule? value)
  (bacula-schedule-spec? value))


(define (format-bacula-schedule value)
  (match-records* ((value <bacula-schedule-spec> name run-specs))
      (format #f "Schedule {~%    Name = ~s~%~a~%}~%"
              name (string-join
                    (map format-bacula-run-spec run-specs)
                    ""))))

(define-public (serialize-bacula-schedule _ value)
  (format-bacula-schedule value))


(define-public (list-of-bacula-schedule? value)
  (and (list? value)
       (every bacula-schedule? value)))


(define-public (serialize-list-of-bacula-schedule _ value)
  (string-join (map format-bacula-schedule value) "\n" 'suffix))


;;;
;;; Run Script
;;;

(define-public (bacula-run-script? value)
  (string? value))


(define-public (serialize-bacula-run-script field-name value)
  (let ((field (config-field-name space field-name)))
    (format #f "Run Before Job = /gnu/store/yr39rh6wihd1wv6gzf7w4w687dwzf3vb-coreutils-9.1/bin/false~%")
    ;; (format #f "~a {~%    RunsWhen = Before~%    FailJobOnError = yes~%    Console = ~s~%}~%"
    ;;           field value)
    ))

(define-public-maybe bacula-run-script)
