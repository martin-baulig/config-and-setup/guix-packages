;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services serialization loki)
  #:use-module (guix records)
  #:use-module (baulig build config-utils)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization syntax-macros)
  #:use-module (baulig services serialization yaml)
  #:use-module (baulig utils syntax-macros)
  #:use-module (rnrs enums)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (web uri)
  #:export-syntax (loki-push-uri))


;;;
;;; Log Level
;;;

(define-public loki-log-level
  (make-enumeration '(debug info warn error)))

(define-enum-serializer yaml loki-log-level)

(define-public-maybe loki-log-level)


;;;
;;; Loki Push API
;;;

(define-syntax loki-push-uri
  (syntax-rules ()
    ((_ proto host)
     (build-uri 'proto #:host host #:port 3100 #:path "/loki/api/v1/push"))))


;;;
;;; Client Authentication
;;;

(define-public loki-client-auth
  (make-enumeration '(no-client-cert require-and-verify-client-cert)))

(define-enum-serializer yaml loki-client-auth
  (lambda (mode)
    (case mode
      ((no-client-cert) "NoClientCert")
      ((require-and-verify-client-cert) "RequireAndVerifyClientCert"))))

(define-public-maybe loki-client-auth)


;;;
;;; Loki TLS Section
;;;

(define-public-configuration loki-tls-server-config
  (cert-file
   secrets-service-blob
   "Certificate.")

  (key-file
   secrets-service-blob
   "Certificate key.")

  (client-auth-type
   maybe-loki-client-auth
   "HTTP TLS Client Auth type.")

  (client-ca-file
   maybe-secrets-service-blob
   "HTTP TLS Client CA path."))

(define-section-serializer yaml loki-tls-server-config "http_tls_config")
