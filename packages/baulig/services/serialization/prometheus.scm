;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services serialization prometheus)
  #:use-module (guix records)
  #:use-module (baulig build config-utils)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization syntax-macros)
  #:use-module (baulig services serialization yaml)
  #:use-module (baulig utils syntax-macros)
  #:use-module (rnrs enums)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (web uri)
  #:export (list-of-prometheus-scrape-configuration?
            serialize-list-of-prometheus-scrape-configuration)
  #:export-syntax (prometheus-duration))


;;;
;;; Array of strings
;;;

(define-public (array-of-strings? value)
  (and (list? value)
       (every (lambda (item)
                (string? item))
              value)))

(define-public (serialize-array-of-strings field-name value)
  (let ((field (config-field-name yaml field-name)))
    (format #f "~a: [~a]~%" field
              (string-join (map (lambda (item)
                                    (format #f "'~a'" item))
                                  value)
                             ", "))))

(define-public-maybe array-of-strings)


;;;
;;; Wrap a string with double underscores
;;;

(define-public (prefix-underscore-label? value)
  (string? value))

(define-public (serialize-prefix-underscore-label field-name value)
  (let ((field (config-field-name yaml field-name)))
    (format #f "__~a: ~s~%" field value)))

(define-public-maybe prefix-underscore-label)

(define-public (wrap-underscore-label? value)
  (string? value))

(define-public (serialize-wrap-underscore-label field-name value)
  (let ((field (config-field-name yaml field-name)))
    (format #f "__~a__: ~s~%" field value)))

(define-public-maybe wrap-underscore-label)


;;;
;;; Key-Value list
;;;

(define-public (raw-key-value-list? value)
  (and (list? value)
       (every (lambda (item)
                (and (pair? item)
                     (symbol? (car item))
                     (string? (cdr item))))
              value)))

(define-public (section-key-value-list? value)
  (raw-key-value-list? value))

(define-public (serialize-raw-key-value-list field-name value)
  (format #f "~a"
            (string-join
               (map (lambda (item)
                      (format #f "~a: ~s" (config-field-name yaml (car item)) (cdr item)))
                    value)
               "\n")))

(define-public (serialize-section-key-value-list field-name value)
  (let ((field (config-field-name yaml field-name)))
    (format #f "~a:~%~a" field
              (string-join
                 (map (lambda (item)
                        (format #f "  ~a: ~s~%" (config-field-name yaml (car item)) (cdr item)))
                      value)
                 "\n"))))


;;;
;;; Duration
;;;

(define-record-type* <prometheus-duration-record>
  prometheus-duration-record
  make-prometheus-duration-record
  prometheus-duration-record?

  (value prometheus-duration-record-value)
  (unit prometheus-duration-record-unit))

(define-public prometheus-duration-interval
  (make-enumeration '(milliseconds seconds minutes hours days weeks years)))

(define-public (prometheus-duration-interval? value)
  (enum-set-member? value prometheus-duration-interval))

(define (prometheus-duration-interval-suffix interval)
  (case interval
    ((milliseconds) "ms")
    ((seconds) "s")
    ((minutes) "s")
    ((hours) "h")
    ((days) "d")
    ((weeks) "w")
    ((years) "y")))


(define-syntax prometheus-duration
  (syntax-rules (milliseconds seconds minutes hours days weeys years)
    ((_ count milliseconds)
     (prometheus-duration-record (value count) (unit 'milliseconds)))
    ((_ count seconds)
     (prometheus-duration-record (value count) (unit 'seconds)))
    ((_ count minutes)
     (prometheus-duration-record (value count) (unit 'minutes)))
    ((_ count hours)
     (prometheus-duration-record (value count) (unit 'hours)))
    ((_ count days)
     (prometheus-duration-record (value count) (unit 'days)))
    ((_ count weeks)
     (prometheus-duration-record (value count) (unit 'weeks)))
    ((_ count years)
     (prometheus-duration-record (value count) (unit 'years)))))


(define-public (prometheus-duration? value)
  (and (prometheus-duration-record? value)
       (integer? (prometheus-duration-record-value value))
       (prometheus-duration-interval? (prometheus-duration-record-unit value))))

(define-public (serialize-prometheus-duration field-name value)
  (let ((field (config-field-name yaml field-name))
        (unit (prometheus-duration-record-unit value))
        (value (prometheus-duration-record-value value)))
    (format #f "~a: ~a~a~%" field value (prometheus-duration-interval-suffix unit))))

(define-public-maybe prometheus-duration)


;;;
;;; Syslog
;;;

(define-public-configuration prometheus-syslog-configuration
  (listen-address
   string
   "TCP address to listen on (host:port)")

  (idle-timeout
   maybe-prometheus-duration
   "The idle timeout for tcp syslog connections, default is 120 seconds")

  (label-structured-data
   boolean
   "Whether to convert syslog structured data to labels")

  (use-incoming-timestamp
   (boolean #f)
   "Use timestamp from incoming message")

  (max-message-length
   maybe-integer
   "Sets the maximum limit to the lenght of syslog messages")

  (labels
   (section-key-value-list '())
   "Label map to add to every log message"))

(define-public-maybe prometheus-syslog-configuration)

(define-section-serializer yaml prometheus-syslog-configuration "syslog")


;;;
;;; Relabel Configs
;;;

(define-public-configuration prometheus-relabel-configuration
  (source-labels
   array-of-strings
   "The source labels select values from existing labels")

  (separator
   maybe-string
   "Separator placed between concatenated source value labels (default ;)")

  (target-label
   string
   "Label to which the resulting action is written in replace action"))

(define (list-of-prometheus-relabel-configurations? lst)
  (every prometheus-relabel-configuration? lst))

(define-array-section-serializer yaml prometheus-relabel-configuration "relabel_configs")


;;;
;;; HTTP Scheme
;;;

(define-public prometheus-http-scheme
  (make-enumeration '(http https)))

(define-enum-serializer yaml prometheus-http-scheme)

(define-public-maybe prometheus-http-scheme)


;;;
;;; Prometheus TLS Section
;;;

(define-public-configuration prometheus-tls-config
  (ca-file
   secrets-service-blob
   "CA certificate to validate API server certificate with.")

  (server-name
   maybe-string
   "ServerName extension to indicate the name of the server.")

  (cert-file
   maybe-secrets-service-blob
   "The cert file to send to the server for client auth.")

  (key-file
   maybe-secrets-service-blob
   "The key file to send to the server for client auth."))

(define-section-serializer yaml prometheus-tls-config "tls_config")

(define-public-maybe prometheus-tls-config)


;;;
;;; Basic Authentication
;;;

(define-public-configuration prometheus-basic-auth
  (username
   user-name
   "Basic Auth user name.")

  (password
   secrets-service-password
   "Basic Auth password."))

(define-section-serializer yaml prometheus-basic-auth "basic_auth")

(define-public-maybe prometheus-basic-auth)


;; When used is 'basic_auth_user' in the --web.config.file, it is
;; serialized differently.

(define-public (list-of-prometheus-basic-auth-user? value)
  (and (list? value)
       (every prometheus-basic-auth? value)))

(define-public (serialize-list-of-prometheus-basic-auth-user field-name value)
  (if (null? value)
      ""
      (format #f "~a:~%~a"
              (config-field-name yaml field-name)
              (apply string-append
                     (map (lambda (item)
                            (match-records* ((item <prometheus-basic-auth> username password))
                                (format #f "    ~a: @text:~a@~%" username password)))
                          value)))))

(define-public-maybe list-of-prometheus-basic-auth-user)

;;;
;;; Static Configs
;;;

(define-public-configuration prometheus-static-label
  (path
   maybe-wrap-underscore-label
   "The path to load logs from.  Can use glob patterns.")

  (path-exclude
   maybe-wrap-underscore-label
   "Used to exclude files from being loaded. Can also contain glob patterns.")

  (extra-labels
   raw-key-value-list
   "Additional labels to assign to the logs"))

(define-section-serializer yaml prometheus-static-label "labels")

(define-public-maybe prometheus-static-label)


(define-public-configuration prometheus-static-configuration
  (targets
   maybe-array-of-strings
   "Target list.")

  (labels
   maybe-prometheus-static-label
   "Defines a file to scrape and and optional set of additional labels"))

(define (list-of-prometheus-static-configurations? lst)
  (every prometheus-static-configuration? lst))

(define-array-section-serializer yaml prometheus-static-configuration "static_configs")


;;;
;;; Prometheus Scrape Config
;;;

(define-public-configuration prometheus-scrape-configuration
  (job-name
   string
   "Name to identify this scrape config in the Prometheus UI")

  (scrape-interval
   maybe-prometheus-duration
   "Override global scrape interval.")

  (scheme
   maybe-prometheus-http-scheme
   "Configures the protocol scheme used for requests.")

  (tls-config
   maybe-prometheus-tls-config
   "Configure the scrape request's TLS settings.")

  (basic-auth
   maybe-prometheus-basic-auth
   "Basic Auth to use for the scrape requests.")

  (metrics-path
   maybe-string
   "The HTTP resource path on which to fetch metrics from targets.")

  (syslog
   maybe-prometheus-syslog-configuration
   "Describe how to receive logs from syslog")

  (relabel-configs
   (list-of-prometheus-relabel-configuration '())
   "Describes how to relabel targets to determine if they should be processed")

  (static-configs
   (list-of-prometheus-static-configuration '())
   "Static targets to scrape"))


(define-array-section-serializer yaml prometheus-scrape-configuration "scrape_configs")


;;;
;;; Prometheus TLS Section
;;;

(define-public-configuration prometheus-tls-server-config
  (cert-file
   secrets-service-blob
   "Certificate.")

  (key-file
   secrets-service-blob
   "Certificate key."))

(define-section-serializer yaml prometheus-tls-server-config "tls_server_config")


;;;
;;; Prometheus Web Configuration
;;;

(define-public-configuration prometheus-web-configuration
  (tls-server-config
   prometheus-tls-server-config
   "TLS Server Configuration section.")

  (basic-auth-users
   maybe-list-of-prometheus-basic-auth-user
   "Basic Authentication section."))

(define-section-serializer yaml prometheus-web-configuration "web")
