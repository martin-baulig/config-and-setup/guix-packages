;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services serialization postgresql)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (gnu packages base)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig build config-utils)
  #:use-module (baulig utils syntax-macros)
  #:use-module (rnrs enums)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:export (postgresql-archive-command))


;;;
;;; Log Destination
;;;

(define-public postgresql-log-destination
  (make-enumeration '(stderr syslog)))

(define-enum-serializer underscore-space postgresql-log-destination)

(define-public-maybe postgresql-log-destination)


;;;
;;; Log Level
;;;

(define-public postgresql-log-level
  (make-enumeration '(debug5 debug4 debug3 debug2 debug1
                      info notice warning error log fatal panic)))

(define-enum-serializer underscore-space postgresql-log-level)

(define-public-maybe postgresql-log-level)


;;;
;;; WAL Level
;;;

(define-public postgresql-wal-level
  (make-enumeration '(minimal replica logical)))

(define-enum-serializer underscore-space postgresql-wal-level)

(define-public-maybe postgresql-wal-level)


;;;
;;; Archive Mode
;;;

(define-public postgresql-archive-mode
  (make-enumeration '(off on always)))

(define-enum-serializer underscore-space postgresql-archive-mode)

(define-public-maybe postgresql-archive-mode)


;;;
;;; Archive Command
;;;

(define-record-type* <postgresql-archive-command>
  postgresql-archive-command
  make-postgresql-archive-command
  postgresql-archive-command?

  (archive-directory    postgresql-archive-command-archive-directory))


(define-public (serialize-postgresql-archive-command field-name value)
  (let ((field (config-field-name underscore field-name))
        (directory (postgresql-archive-command-archive-directory value)))
    (format #f "~a 'test ! -f ~a/%f && ~a %p ~a/%f'~%"
            field directory "@parameter:coreutils@/bin/cp"
            directory)))


(define-public-maybe postgresql-archive-command)
