;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services serialization nginx)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (baulig build config-utils)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization syntax-macros)
  #:use-module (baulig services serialization underscore-space-semicolon)
  #:use-module (baulig utils syntax-macros)
  #:use-module (rnrs enums)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (web uri)
  #:export (nginx-conditional
            nginx-listen-address
            list-of-nginx-upstream-configuration?
            serialize-list-of-nginx-upstream-configuration
            nginx-upstream-section-unix-socket-group)
  #:export-syntax (nginx-location
                   nginx-location-directive))


;;;
;;; Nginx Listen Address.
;;;

(define-record-type* <nginx-listen-address>
  nginx-listen-address
  make-nginx-listen-address
  nginx-listen-address?

  (port                 nginx-listen-address-port)
  (host                 nginx-listen-address-host)
  (add-loopback?        nginx-listen-add-loopback?
                        (default #f)))


(define-public (serialize-nginx-listen-address _ value)
  (match-records* ((value <nginx-listen-address> port host add-loopback?))
      (let ((loopback (if add-loopback?
                          (format #f "listen 127.0.0.1:~a ssl;~%" port)
                          "")))
        (format #f "listen ~a:~a ssl;~%~a" host port loopback))))


(define-public (nginx-listen-port-or-address? value)
  (or (integer? value)
      (nginx-listen-address? value)))

(define-public (serialize-nginx-listen-port-or-address name value)
  (if (integer? value)
      (format #f "listen ~a ssl;~%" value)
      (serialize-nginx-listen-address name value)))

(define-public (nginx-listen-port value)
  (if (integer? value)
      value
      (nginx-listen-address-port value)))

;;;
;;; Extra Parameters
;;;

(define-public (nginx-server-extra-parameters? value)
  (gexp? value))

(define-public-maybe/no-serialization nginx-server-extra-parameters)


;;;
;;; Error Logging
;;;

(define-public nginx-log-level
  (make-enumeration '(debug info notice warn error crit alert emerg)))

(define-enum-serializer underscore-space-semicolon nginx-log-level)

(define-public-maybe nginx-log-level)


;;;
;;; Verify Client Certificate
;;;

(define-public nginx-verify-client
  (make-enumeration '(on off optional)))

(define-enum-serializer underscore-space-semicolon nginx-verify-client)

(define-public-maybe nginx-verify-client)


;;;
;;; Extra Configuration Directives
;;;

(define-record-type* <nginx-conditional>
  nginx-conditional
  make-nginx-conditional
  nginx-conditional?

  (condition            nginx-conditional-condition)
  (directives           nginx-conditional-directives))

(define-public-maybe nginx-conditional)


(define-public (serialize-nginx-conditional name value)
  (match-record value <nginx-conditional>
    (condition directives)
    (config-section-key-value
     (format #f "if (~a)" condition)
     (serialize-list-of-nginx-directives name directives))))

(define-public (nginx-directive? value)
  (or (string? value)
      (nginx-conditional? value)))

(define-public (serialize-nginx-directive name value)
  (cond ((string? value)
         (format #f "~a;~%" value))
        ((nginx-conditional? value)
         (serialize-nginx-conditional name value))))


(define-public (list-of-nginx-directives? value)
  (and (list? value)
       (every nginx-directive? value)))

(define-public (serialize-list-of-nginx-directives name items)
  (string-join
   (map (cut serialize-nginx-directive name <>)
        items)
   "\n"))


;;;
;;; Nginx Proxy Pass
;;;

(define-public-configuration nginx-proxy-pass
  (target
   uri
   "URI of proxied server to which a location should be mapped."
   (serializer
    (lambda (_ value) (serialize-uri 'proxy-pass value))))

  (proxy-socket-keepalive
   maybe-boolean-on-off
   "Configures TCP keepalive behavior for the proxied connection.")

  (proxy-ssl-certificate
   maybe-secrets-service-blob
   "Client certificate used for authentication to a proxied HTTPS server.")

  (proxy-ssl-certificate-key
   maybe-secrets-service-blob
   "Client certificate key used for authentication to a proxied HTTPS server.")

  (proxy-ssl-name
   maybe-host-name
   "Allows overriding the server name used to verify the certificate of the proxied HTTPS server.")

  (proxy-ssl-verify
   maybe-boolean-on-off
   "Enables verification of the proxied HTTPS server certificate.")

  (proxy-ssl-trusted-certificate
   maybe-secrets-service-blob
   "Trusted CA certificate used to verify the proxied HTTPS server certificate.")

  (extra-directives
   (list-of-nginx-directives '())
   "Extra configuration directives."))

(define-section-serializer key-value-fragment nginx-proxy-pass)

(define-public-maybe nginx-proxy-pass)


;;;
;;; Nginx Location Configuration
;;;

(define-public-configuration nginx-location-configuration
  (proxy-pass-section
   maybe-nginx-proxy-pass
   "Proxy-pass section.")

  (extra-directives
   (list-of-nginx-directives '())
   "Extra configuration directives."))

(define-section-serializer key-value nginx-location-configuration "location")

(define-array-section-serializer key-value nginx-location-configuration "location")


;;;
;;; Nginx Location Section
;;;

(define-public nginx-location-type
  (make-enumeration '(path exact-match no-regex regex regex-case-insensitive)))

(define-public (nginx-location-type? value)
  (enum-set-member? value nginx-location-type))

(define (location-type-symbol type)
  (case type
    ((path) "")
    ((exact-match) "= ")
    ((no-regex) "^~ ")
    ((regex) "~ ")
    ((rexex-case-insensitive) "~* ")))

(define-public (serialize-nginx-location-type _ type)
  (location-type-symbol type))


(define-public-configuration nginx-location-section
  (type
   (nginx-location-type 'path)
   "Location type.")

  (path
   string
   "Path of this location section.")

  (configuration
   nginx-location-configuration
   "The actual configuration."))


(define-public (serialize-nginx-location-section _ value)
  (match-record value <nginx-location-section>
    (type path configuration)

    (config-section-key-value
     (format #f "location ~a~a" (location-type-symbol type) path)
     (serialize-configuration configuration nginx-location-configuration-fields))))

(define-public (list-of-nginx-location-section? value)
  (and (list? value)
       (every nginx-location-section? value)))

(define-public (serialize-list-of-nginx-location-section _ items)
  (string-join
   (map (cut serialize-nginx-location-section "" <>)
        items)
   ""))


;;;
;;; Location Macros
;;;

(define-syntax %proxy-pass-directive
  (syntax-rules (set-header remove-header http-version-1.1 tls-1.3 custom)
    ((_ instance (http-version-1.1))
     (%proxy-pass-directive instance (custom "proxy_http_version 1.1")))
    ((_ instance (tls-1.3))
     (%proxy-pass-directive instance (custom "proxy_ssl_protocols TLSv1.3")))
    ((_ instance (set-header name value))
     (%proxy-pass-directive instance (custom (format #f "proxy_set_header ~a ~a" name value))))
    ((_ instance (remove-header name))
     (%proxy-pass-directive instance (custom (format #f "proxy_set_header ~a \"\"" name))))
    ((_ instance (custom directive))
     (nginx-proxy-pass
      (inherit instance)
      (extra-directives (cons directive (nginx-proxy-pass-extra-directives instance)))))
    ((_ instance directive)
     (nginx-proxy-pass
      (inherit instance)
      directive))
    ((_ instance directive directives ...)
     (let ((new-instance (%proxy-pass-directive instance directive)))
       (%proxy-pass-directive new-instance directives ...)))
    ((_ instance)
     instance)))


(define-syntax %nginx-location-directive
  (lambda (x)
    (syntax-case x (proxy-pass)
      ((_ instance (proxy-pass (proto uri-args ...) args ...))
       (identifier? #'proto)
       #'(let* ((uri (build-uri 'proto uri-args ...))
                (proxy-instance (nginx-proxy-pass (target uri))))
           (nginx-location-configuration
            (inherit instance)
            (proxy-pass-section (%proxy-pass-directive proxy-instance args ...))))))))


(define-syntax nginx-location
  (lambda (x)
    (syntax-case x ()
      ((_ %type %path directive ...)
       (and (identifier? #'%type)
            (nginx-location-type? (syntax->datum #'%type))
            (string? (syntax->datum #'%path)))
       #'(nginx-location-section
          (type %type)
          (path %path)
          (configuration
           (%nginx-location-directive (nginx-location-configuration) directive ...))))
      ((_ %path directive ...)
       (string? (syntax->datum #'%path))
       #'(nginx-location-section
          (path %path)
          (configuration
           (%nginx-location-directive (nginx-location-configuration) directive ...)))))))


;;;
;;; Nginx Upstream Configuration
;;;

(define-public-configuration nginx-upstream-configuration
  (server
   maybe-uri
   "URI of upstream server.")

  (extra-directives
   (list-of-nginx-directives '())
   "Extra configuration directives."))

(define-section-serializer key-value nginx-upstream-configuration "upstream")

(define-array-section-serializer key-value nginx-upstream-configuration "upstream")


(define-public-configuration nginx-upstream-section
  (path
   string
   "Path of this upstream section.")

  (unix-socket-group
   maybe-user-name
   "When using Unix domain sockets, add Nginx user to this group.")

  (configuration
   nginx-upstream-configuration
   "The actual configuration."))


(define-public (serialize-nginx-upstream-section _ value)
  (match-record value <nginx-upstream-section>
    (path configuration)

    (config-section-key-value
     (format #f "upstream ~a" path)
     (serialize-configuration configuration nginx-upstream-configuration-fields))))

(define-public (list-of-nginx-upstream-section? value)
  (and (list? value)
       (every nginx-upstream-section? value)))

(define-public (serialize-list-of-nginx-upstream-section _ items)
  (string-join
   (map (cut serialize-nginx-upstream-section "" <>)
        items)
   ""))
