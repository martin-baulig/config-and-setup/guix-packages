;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services nginx)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (guix build utils)
  #:use-module (gnu packages php)
  #:use-module (gnu packages web)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (baulig build config-utils)
  #:use-module (baulig services account)
  #:use-module (baulig services activation)
  #:use-module (baulig services secrets)
  #:use-module (baulig services parameters)
  #:use-module (baulig services php-fpm)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization syntax-macros)
  #:use-module (baulig services serialization underscore-space-semicolon)
  #:use-module (baulig services serialization nginx)
  #:use-module (rnrs enums)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 match)
  #:use-module (web uri)
  #:re-export (nginx-conditional
               nginx-location-configuration
               nginx-location-section
               nginx-upstream-configuration
               nginx-upstream-section
               nginx-log-level))


;;;
;;; Configuration Serialization
;;;

(define-syntax define-parameter-constant
  (syntax-rules (pid-directory pid-file log-directory)
    ((_ name pid-file)
     (define-string-constant underscore-space-semicolon name "@parameter:pid-file@"))
    ((_ name log-directory file)
     (define-string-constant
       underscore-space-semicolon
       name
       (format #f "@parameter:log-directory@/~a" file)))
    ((_ name pid-directory file)
     (define-string-constant
       underscore-space-semicolon
       name
       (format #f "@parameter:pid-directory@/~a" file)))
    ((_ name value)
     (define-string-constant underscore-space-semicolon name value))))


(define-parameter-constant nginx-user "@parameter:user@ @parameter:group@")
(define-parameter-constant mime-types "@parameter:nginx@/share/nginx/conf/mime.types")

(define-parameter-constant nginx-pid-file pid-file)
(define-parameter-constant client-body-temp-path pid-directory "client_body_temp")
(define-parameter-constant proxy-temp-path pid-directory "proxy_temp")
(define-parameter-constant fastcgi-temp-path pid-directory "fastcgi_temp")
(define-parameter-constant uwsgi-temp-path pid-directory "uwsgi_temp")
(define-parameter-constant scgi-temp-path pid-directory "scgi_temp")

(define-parameter-constant access-log log-directory "nginx-access.log")

(define-parameter-constant nginx-ssl-protocols "TLSv1.3")


;;;
;;; Error Logging
;;;

(define-public-configuration nginx-error-log
  (log-level
   maybe-nginx-log-level
   "Log level."))


(define-public (serialize-nginx-error-log _ error-log)
  (match-records* ((error-log <nginx-error-log> log-level))
      (let ((log-file "@parameter:log-directory@/nginx-error.log")
            (level-string (if (maybe-value-set? log-level)
                              (string-append " " (symbol->string log-level))
                              "")))
        (format #f "error_log ~a~a;~%" log-file level-string))))


;;;
;;; PHP-FPM Support
;;;

(define-public (serialize-nginx-php-fpm-section _ configuration)
  (let* ((id (resolve-php-fpm-id configuration))
         (name (symbol->string id)))
    (let* ((socket (format #f "unix:@parameter:service-run-root@/~a/php-fpm.sock" name))
           (location (serialize-php-fpm-configuration #f configuration))
           (lines (string-join
                   `(,(format #f "fastcgi_pass ~a;" socket)
                     "include @parameter:nginx@/share/nginx/conf/fastcgi.conf;"
                     "fastcgi_split_path_info ^(.+\\.php)(/.+)$;"
                     ,location)
                   "\n")))
      (config-section-key-value "location ~ \\.php" lines))))

(define-public (nginx-php-fpm-section? value)
  (php-fpm-configuration? value))

(define-public-maybe nginx-php-fpm-section)


;;;
;;; Nginx Basic Authentication
;;;

(define-public-configuration/no-serialization nginx-basic-auth
  (name
   string
   "Basic Auth name.")

  (user
   user-name
   "Basic Auth user name.")

  (password
   secrets-service-text
   "Basic Auth password."))

(define-public-maybe/no-serialization nginx-basic-auth)


;;;
;;; Nginx Server Configuration
;;;

(define-public-configuration nginx-server-configuration
  (id
   maybe-account-id
   "Optional.  Unique ID identifying this server."
   (serializer empty-serializer))

  (listen
   (nginx-listen-port-or-address 443)
   "Port to listen on.")

  (server-name
   host-name
   "Server name.")

  (ssl-certificate
   secrets-service-blob
   "Secrets Service Blob containing the PEM encoded TLS certificate.")

  (ssl-certificate-key
   secrets-service-blob
   "Secrets Service Blob containing the PEM encoded TLS certificate key.")

  (ssl-client-certificate
   maybe-secrets-service-blob
   "Trusted CA used to verify client certificates.")

  (ssl-trusted-certificate
   maybe-secrets-service-blob
   "Trusted CA used to verify client certificates.
In contrast to 'ssl-client-certificate', this will not be sent to clients.")

  (ssl-verify-client
   maybe-nginx-verify-client
   "Enables verification of client certificates.")

  (ssl-protocols
   (nginx-ssl-protocols-constant %nginx-ssl-protocols)
   "Supported TLS protocols.")

  (root-path
   string
   "Server root path."
   (serializer
    (lambda (_ value) (serialize-path-name 'root value))))

  (index
   (file-name "index.html")
   "Index file name.")

  (server-tokens
   (boolean-on-off #f)
   "Whether to enable server tokens.")

  (rewrite-log
   (boolean-on-off #f)
   "Turn on rewrite log.")

  (extra-directives
   (list-of-nginx-directives '())
   "Extra configuration directives.")

  (php-fpm
   maybe-nginx-php-fpm-section
   "PHP-FPM configuration.")

  (location
   (list-of-nginx-location-section '())
   "Location sections.")

  (basic-auth
   maybe-nginx-basic-auth
   "Basic Auth section."
   (serializer empty-serializer))

  (extra-parameters
   maybe-nginx-server-extra-parameters
   "G-Exp returnig an alist of extra parameters for the secrets-service."
   (serializer empty-serializer)))

(define-section-serializer key-value nginx-server-configuration "server")

(define-array-section-serializer key-value nginx-server-configuration "server")


;; We create a custom config file for each server.
(define* (nginx-server-config-file server #:key (suffix ".conf"))
  (match-records* ((server <nginx-server-configuration> id server-name listen))
      (let ((name-string (if (maybe-value-set? id)
                             (symbol->string id)
                             (format #f "~a-~a" server-name (nginx-listen-port listen)))))
        (format #f "nginx-server-~a~a" name-string suffix))))

(define-public (serialize-nginx-server-sections _ sections)
  (string-join
   (map (lambda (server)
          (let* ((output-file (nginx-server-config-file server))
                 (basic-auth (nginx-server-configuration-basic-auth server))
                 (auth-section (if (maybe-value-set? basic-auth)
                                   (format #f "    auth_basic ~s;~%    auth_basic_user_file ~a;~%"
                                           (nginx-basic-auth-name basic-auth)
                                           (nginx-server-config-file server #:suffix ".htpasswd"))
                                   "")))
            (format #f "server {~%    include ~a;~%~a}~%~%" output-file auth-section)))
        sections)
   "\n" 'suffix))


;;;
;;; Nginx Events Configuration
;;;
(define-public-configuration nginx-events-configuration
  (worker-connections
   (integer 64)
   "Maximum of simultaneous connections that can be opened by a worker process."))

(define-section-serializer key-value nginx-events-configuration "events")


;;;
;;; Nginx Http Configuration
;;;

(define-public-configuration nginx-http-configuration
  (client-body-temp-path
   (client-body-temp-path-constant %client-body-temp-path)
   "client-body-temp")

  (proxy-temp-path
   (proxy-temp-path-constant %proxy-temp-path)
   "proxy-temp")

  (fastcgi-temp-path
   (fastcgi-temp-path-constant %fastcgi-temp-path)
   "fastcgi-temp")

  (uwsgi-temp-path
   (uwsgi-temp-path-constant %uwsgi-temp-path)
   "uwsgi-temp")

  (scgi-temp-path
   (scgi-temp-path-constant %scgi-temp-path)
   "scgi-temp")

  (access-log
   (access-log-constant %access-log)
   "Access log path.")

  (mime-types
   (mime-types-constant %mime-types)
   "Including mime.types."
   (serializer
    (lambda (_ value) (serialize-mime-types-constant 'include value))))

  (map-connection-upgrade
   (boolean #f)
   "Add connection upgrade map for Grafana Like WebSocket."
   (serializer
    (lambda (_ value)
      (if value
          (string-join
           '("map $http_upgrade $connection_upgrade {"
             "    default upgrade;"
             "    '' close;"
             "}")
           "\n" 'suffix)
          ""))))

  (server
   (list-of-nginx-server-configuration '())
   "Server configuration sections."
   (serializer serialize-nginx-server-sections))

  (upstream
   (list-of-nginx-upstream-section '())
   "Upstream configuration sections."))

(define-section-serializer key-value nginx-http-configuration "http")


;;;
;;; Nginx Configuration
;;;

(define-public-configuration nginx-configuration
  (user
   (nginx-user-constant %nginx-user)
   "User and group.")

  (pid
   (nginx-pid-file-constant %nginx-pid-file)
   "PID file.")

  (error-log
   (nginx-error-log (nginx-error-log))
   "Error log path.")

  (events
   (nginx-events-configuration (nginx-events-configuration))
   "Events configuration section.")

  (http
   nginx-http-configuration
   "Http configuration section."))


;;;
;;; Nginx Service
;;;

(define-record-type* <nginx-service>
  nginx-service
  make-nginx-service
  nginx-service?

  (configuration        nginx-service-configuration)
  (account              nginx-service-account)
  (secrets              nginx-service-secrets)
  (php-fpm              nginx-service-php-fpm)
  (servers              nginx-service-servers))

(export nginx-service)


;;;
;;; Internal APIs
;;;

(define (resolve-nginx-account configuration php-fpm-list)
  (match-records* ((configuration <nginx-configuration> http)
                   (http <nginx-http-configuration> upstream))
      (let* ((upstream-groups (append-map
                               (lambda (item)
                                 (let ((group (nginx-upstream-section-unix-socket-group item)))
                                   (if (maybe-value-set? group) (list group) '())))
                               upstream))
             (php-fpm-groups (map resolve-php-fpm-group php-fpm-list)))
        (make-daemon-account
         (account-configuration
          (id 'nginx)
          (supplementary-groups
           (append php-fpm-groups upstream-groups)))))))


(define (resolve-nginx-activation service)
  (match-records* ((service <nginx-service> account secrets)
                   (account <service-account> user log-directory log-group))
      (activation-task
       (id 'nginx)
       (requirements (secrets-service-requirements secrets))
       (action
        #~(begin
            (format #t "Nginx activation callback~%")
            (let* ((access-log (string-append #$log-directory "/nginx-access.log"))
                   (error-log (string-append #$log-directory "/nginx-error.log"))
                   (uid (passwd:uid (getpw #$user)))
                   (gid (passwd:gid (getpw #$log-group))))
              (create-file-with-owner access-log uid gid #o640)
              (create-file-with-owner error-log uid gid #o640)))))))


(define (resolve-nginx-shepherd service)
  (match-records* ((service <nginx-service> secrets configuration)
                   (secrets <secrets-service> account output-file requirements)
                   (account <service-account>
                            working-directory user group
                            log-file-path log-directory pid-file-path))
      (list
       (shepherd-service
        (provision '(nginx))
        (documentation "Nginx with Secrets Service integration.")
        (requirement `(networking activation-task-nginx ,@requirements))
        (respawn? #f)
        (start #~(make-forkexec-constructor
                  (list #$(file-append nginx "/sbin/nginx")
                        "-c" #$output-file
                        "-e" #$(string-append log-directory "/nginx-error.log")
                        "-p" #$working-directory)
                  #:directory #$working-directory
                  #:log-file #$log-file-path
                  #:pid-file #$pid-file-path))
        (stop #~(make-kill-destructor))))))


(define-public nginx-service-type
  (service-type (name 'nginx)
                (extensions
                 (list (service-extension activation-task-service-type
                                          resolve-nginx-activation)
                       (service-extension shepherd-root-service-type
                                          resolve-nginx-shepherd)))
                (description "Nginx with Secrets Service integration.")))


(define-record-type* <nginx-service-template>
  nginx-service-template
  make-nginx-service-template
  nginx-service-template?

  (configuration        nginx-service-template-configuration)
  (servers              nginx-service-template-servers))


(define (resolve-nginx-servers service-template)
  (match-records* ((service-template <nginx-service-template> configuration servers)
                   (configuration <nginx-configuration> http)
                   (http <nginx-http-configuration> server))
      (append server servers)))


(define* (resolve-nginx-php-fpm service-template #:key parameters)
  (fold (match-lambda* ((extension iter)
                        (let ((php-fpm (nginx-server-configuration-php-fpm extension)))
                          (if (maybe-value-set? php-fpm)
                              (cons php-fpm iter)
                              iter))))
        '() (resolve-nginx-servers service-template)))


(define (resolve-nginx-server-secrets server)
  (match-records* ((server <nginx-server-configuration> server-name listen
                           basic-auth extra-parameters))
      (let ((output-file (nginx-server-config-file server))
            (htpasswd-file (nginx-server-config-file server #:suffix ".htpasswd")))
        `(,(create-secrets-section
               (serialize output-file nginx-server server)
               (extra-parameters gexp (with-default extra-parameters '())))
          ,@(if (maybe-value-set? basic-auth)
                (list
                 (match-records* ((basic-auth <nginx-basic-auth> user password))
                     (create-secrets-section
                         (template htpasswd-file (format #f "~a:@text:~a@~%" user password)))))
                '())))))


(define* (resolve-nginx-secrets service-template #:key parameters)
  (match-records* ((service-template <nginx-service-template> configuration servers)
                   (configuration <nginx-configuration> http)
                   (http <nginx-http-configuration> server))
      (let* ((servers (resolve-nginx-servers service-template))
             (server-secrets (append-map resolve-nginx-server-secrets servers))
             (extended-http (nginx-http-configuration
                             (inherit http)
                             (server servers)))
             (extended-configuration (nginx-configuration
                                      (inherit configuration)
                                      (http extended-http)))
             (php-fpm (resolve-nginx-php-fpm service-template))
             (template (serialize-configuration extended-configuration nginx-configuration-fields))
             (account (resolve-nginx-account extended-configuration php-fpm)))

        (create-secrets
            (account-expression account)
            (serialize "nginx.conf" nginx extended-configuration)
          (extra-sections server-secrets)
          (extra-parameters gexp #~((nginx . #$nginx)))))))


(define-parameterized-service nginx
  (record-type <nginx-configuration>)
  (extend-type-predicate nginx-server-configuration?)
  (parent secrets resolve-nginx-secrets)
  (parent-list php-fpm resolve-nginx-php-fpm)
  (constructor
   (lambda* (configuration #:key parameters)
     (nginx-service-template
      (configuration configuration)
      (servers '()))))
  (resolve
   (lambda* (configuration #:key secrets php-fpm parameters)
     (match-records* ((configuration <nginx-service-template> servers))
         (nginx-service
          (configuration configuration)
          (account (secrets-service-account secrets))
          (php-fpm php-fpm)
          (secrets secrets)
          (servers servers)))))
  ;; We want to have one <nginx-configuration> instance - and then extend it
  ;; with <nginx-server-configuration>.
  (compose singleton)
  (extend merge
          (lambda (configuration value)
            (match-records* ((configuration <nginx-service-template> servers)
                             (value <nginx-server-configuration>))
                (nginx-service-template
                 (inherit configuration)
                 (servers (cons value servers)))))))
