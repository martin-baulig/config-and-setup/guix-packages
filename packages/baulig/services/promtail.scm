;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services promtail)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services shepherd)
  #:use-module (gnu system shadow)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages base)
  #:use-module (guix gexp)
  #:use-module (guix derivations)
  #:use-module (guix monads)
  #:use-module (guix records)
  #:use-module (guix store)
  #:use-module (guix build utils)
  #:use-module (ice-9 match)
  #:use-module (ice-9 textual-ports)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-26)
  #:use-module (baulig packages loki)
  #:use-module (baulig services account)
  #:use-module (baulig services parameters)
  #:use-module (baulig services secrets)
  #:use-module (baulig services rsyslog)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization syntax-macros)
  #:use-module (baulig services serialization yaml)
  #:use-module (baulig services serialization loki)
  #:use-module (baulig services serialization prometheus)
  #:use-module (baulig build utils)
  #:use-module (baulig build config-utils))


;;;
;;; Promtail Server
;;;

(define-public-configuration promtail-server-configuration
  (disable?
   true-constant
   "Disable the HTTP and GPRP server"))

(define-section-serializer yaml promtail-server-configuration "server")


;;;
;;; Basic Auth
;;;

(define-public-configuration promtail-basic-auth
  (username
   string
   "The username to use for basic auth")

  (password-file
   secrets-service-blob
   "The file containing the password for basic auth"))

(define-section-serializer yaml promtail-basic-auth "basic_auth")

(define-public-maybe promtail-basic-auth)


;;;
;;; Promtail Positions
;;;

(define-string-constant yaml promtail-positions-file
  "@parameter:working-directory@/positions.yaml")

(define-public-configuration promtail-positions-configuration
  (filename
   (promtail-positions-file-constant %promtail-positions-file)
   "Location of positions file"))

(define-section-serializer yaml promtail-positions-configuration "positions")


;;;
;;; Promtail Client
;;;

(define-public-configuration promtail-client-configuration
  (url
   uri
   "The URL where Loki is listening")

  (basic-auth
   maybe-promtail-basic-auth
   "Use basic auth for authentication")

  (tls-config
   maybe-prometheus-tls-config
   "TLS configuration"))


(define (list-of-promtail-client-configurations? lst)
  (every promtail-client-configuration? lst))

(define-array-section-serializer yaml promtail-client-configuration "clients")


;;;
;;; Promtail
;;;

(define-public-configuration promtail-configuration
  (server
   (promtail-server-configuration (promtail-server-configuration))
   "Promtail server configuration")

  (positions
   (promtail-positions-configuration (promtail-positions-configuration))
   "Promtail positions configuration")

  (clients
   (list-of-promtail-client-configuration '())
   "Promtail client configuration")

  (scrape-configs
   (list-of-prometheus-scrape-configuration '())
   "Promtail scrape configurations"))

(define-section-serializer yaml promtail-configuration "promtail")

(export promtail-configuration-fields)


;;;
;;; Promtail Service
;;;

(define-record-type* <promtail-service>
  promtail-service
  make-promtail-service
  promtail-service?

  (secrets              promtail-service-secrets)
  (configuration        promtail-service-configuration))


(define (promtail-print-config config-file)
  (shepherd-action
   (name 'config)
   (documentation "Print Promtail configuration.")
   (procedure #~(lambda (running . args)
                  (let ((output (call-with-input-file #$config-file get-string-all)))
                    (format #t "Using configuration file ~a:~%~%~a~%" #$config-file output))))))


(define* (resolve-promtail-secrets configuration #:key parameters)
  (match-records* ((configuration <promtail-configuration>))
      (create-secrets
          (daemon-account 'promtail
                          (supplementary-groups (list %rsyslog-group)))
          (serialize "promtail.conf" promtail configuration))))


(define (resolve-promtail-shepherd service)
  (match-records* ((service <promtail-service> secrets)
                   (secrets <secrets-service> account output-file requirements)
                   (account <service-account> user group supplementary-groups
                            working-directory log-file-path))
      (list (shepherd-service
             (provision '(promtail))
             (documentation "Run the Promtail daemon.")
             (requirement `(networking ,@requirements))
             (actions (list (promtail-print-config output-file)))
             (start #~(make-forkexec-constructor
                       (list (string-append #$loki "/bin/promtail")
                             "-log.level" "debug"
                             "-inspect"
                             "-config.file" #$output-file)
                       #:directory #$working-directory
                       #:user #$user
                       #:group #$group
                       #:supplementary-groups (list #$@supplementary-groups)
                       #:log-file #$log-file-path))
             (stop #~(make-kill-destructor))))))


(define* (resolve-promtail configuration #:key secrets parameters)
  (match-records* ((configuration <promtail-configuration>))
      (promtail-service
       (configuration configuration)
       (secrets secrets))))


;;;
;;; Promtail Service
;;;

(define-public promtail-service-type
  (service-type (name 'promtail-service)
                (extensions
                 (list (service-extension shepherd-root-service-type
                                          resolve-promtail-shepherd)))
                (description
                 "Run the the @command{promtail-serviced} daemon. ")))


(define-parameterized-service promtail
  (record-type <promtail-configuration>)
  (parent secrets resolve-promtail-secrets)
  (resolve resolve-promtail))
