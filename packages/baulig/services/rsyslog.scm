;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services rsyslog)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services shepherd)
  #:use-module (gnu system shadow)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages base)
  #:use-module (gnu packages logging)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (guix store)
  #:use-module (guix build utils)
  #:use-module (baulig build utils)
  #:use-module (baulig config common)
  #:use-module (srfi srfi-1)
  #:export (rsyslog-configuration
            rsyslog-address))

;;;
;;; Default settings
;;;

(define-public %rsyslog-user "syslog")
(define-public %rsyslog-group "syslog")
(define %rsyslog-working-directory "/var/empty")
(define %rsyslog-log-file "/var/log/rsyslog.log")
(define %rsyslog-pid-file "/var/run/rsyslog.pid")

(define-record-type* <rsyslog-address>
  rsyslog-address
  make-rsyslog-address
  rsyslog-address?

  (ip rsyslog-address-ip)
  (port rsyslog-address-port))

;;;
;;; Rsyslog configuration
;;;
(define-record-type* <rsyslog-configuration>
  rsyslog-configuration
  make-rsyslog-configuration
  rsyslog-configuration?

  (promtail rsyslog-configuration-promtail
            (default #f))

  (forward rsyslog-configuration-forward
           (default #f))

  (server rsyslog-configuration-server
          (default #f)))


;;;
;;; Rsyslog Service
;;;

(define-syntax-rule (statement-rule name (parameter argument) ...)
  (let* ((fields (list (format #f "~a=\"~a\"" 'parameter argument) ...)))
    (format #f "~a ~a~%" 'name fields)))

(define-syntax-rule (string-template-statement template-name template-value)
  (statement-rule template
                  (name 'template-name)
                  (type "string")
                  (value template-value)))

(define-syntax list-template-statement
  (syntax-rules ()
    ((_ name args ...)
     (letrec-syntax
         ((parameter
           (lambda (s)
             (syntax-case s (date-format from to relative-to-end fixed-width spifno1stsp)
               ((_ (date-format date))
                (format #f "dateformat=\"~a\"" (syntax->datum #'date)))
               ((_ (from pos))
                (format #f "position.from=\"~a\"" (syntax->datum #'pos)))
               ((_ (to pos))
                (format #f "position.to=\"~a\"" (syntax->datum #'pos)))
               ((_ (relative-to-end pos))
                (format #f "position.relativeToEnd=\"~a\"" (syntax->datum #'pos)))
               ((_ fixed-width)
                "fixedwidth=\"on\"")
               ((_ spifno1stsp)
                "spifno1stsp=\"on\"")
               ((_ exp)
                #'(syntax-error "Invalid property parameter:" exp)))))
          (expand
           (lambda (s)
             (with-ellipsis :::
                            (define %properties
                              '("msg" "rawmsg" "rawmsg-after-pri" "hostname" "source"
                                "fromhost" "fromhost-ip" "syslogtag" "programname" "pri"
                                "pri-text" "iut" "syslogfacility" "syslogfacility-text"
                                "syslogseverity" "syslogseverity-text" "syslogpriority"
                                "syslogpriority-text" "timegenerated" "timereported"
                                "timestamp" "protocol-version" "structured-data"
                                "app-name" "procid" "msgid" "inputname" "jsonmesg"
                                "$now" "$bom" "$myhostname" "$year" "$month" "$day"
                                "$hour" "$hhour" "$qhour" "$minute"))
                            (define (property? value)
                              (and (identifier? value)
                                   (member (symbol->string (syntax->datum value)) %properties)))
                            (define (format-constant value)
                              (format #f "    constant (value=\"~a\")~%" value))

                            (syntax-case s (constant property space newline test)
                              ((_ (constant arg))
                               (string? (syntax->datum #'arg))
                               (format-constant (syntax->datum #'arg)))
                              ((_ (property name params :::))
                               (property? #'name)
                               #'(format #f "    property (name=\"~a\"~a)~%" 'name
                                  (string-join (list (parameter params) :::) " " 'prefix)))
                              ((_ space)
                               (format-constant " "))
                              ((_ newline)
                               (format-constant "\\n"))
                              ((_ exp)
                               (cond ((property? #'exp)
                                      (format #f "    property (name=\"~a\")~%" (syntax->datum #'exp)))
                                     ((string? (syntax->datum #'exp))
                                      (format-constant (syntax->datum #'exp)))
                                     (#t
                                      #'(syntax-error "Invalid element in list template:" exp)))))))))

       (format #f "template (name=\"~a\" type=\"list\") {~%~a}~%"
               'name (string-join (list (expand args) ...) ""))))))

(define-syntax-rule (indent-block line ...)
  (let* ((body (rainer-script line ...))
         (lines (remove string-null? (string-split body #\newline)))
         (joined (string-join lines "\n    " 'prefix)))
    (string-append joined "\n")))

(define-syntax if-statement-rule
  (syntax-rules (body)
    ((_ expr (body block ...) clause ...)
     (letrec-syntax
         ((expand-clause
           (with-ellipsis :::
                          (syntax-rules (else-if otherwise)
                            ((_ (else-if cond inner :::) ::: (otherwise final :::))
                             (format #f "~a else {~a}"
                                     (expand-clause (else-if cond inner :::) :::)
                                     (indent-block final :::)))
                            ((_ (else-if cond inner :::) :::)
                             (string-join
                              (list (format #f " else if (~a) then {~a}" cond
                                            (indent-block inner :::)) :::) ""))
                            ((_ other :::)
                             (syntax-error "Invalid clause" other :::))))))
       (format #f "if (~a) then {~a}~a~%" expr
               (indent-block block ...)
               (expand-clause clause ...))))
    ((_ expr block ...)
     (format #f "if (~a) then {~a}~%" expr
             (indent-block block ...)))
    ((_ other . rest)
     (syntax-error "Invalid if statement:" other rest))))


(define-syntax rainer-script-line
  (lambda (x)
    (syntax-case x (action blank-line conditional directive forward-action if-block input legacy list-template module)
      ((_ blank-line) "\n")
      ((_ (directive name value))
       (identifier? #'name)
       #'(format #f "$~a ~a~%" 'name value))
      ((_ (module args ...))
       #'(statement-rule module args ...))
      ((_ (input args ...))
       #'(statement-rule input args ...))
      ((_ (action args ...))
       #'(statement-rule action args ...))
      ((_ (list-template name args ...))
       #'(list-template-statement name args ...))
      ((_ (legacy arg))
       #'(format #f "~a~%" arg))
      ((_ (if-block expr block ...))
       #'(if-statement-rule expr block ...))
      ((_ (conditional expr block ...))
       #'(if expr (rainer-script block ...) ""))
      ((_ (forward-action template-name server))
       #'(statement-rule action
          (type "omfwd")
          (protocol "tcp")
          (target (rsyslog-address-ip server))
          (port (rsyslog-address-port server))
          (template template-name)
          (TCP_Framing "octet-counted")
          (action.resumeRetryCount "-1")
          (queue.type "linkedlist")
          (queue.size "50000")
          (KeepAlive "on")))
      ((_ exp)
       #'(syntax-error "Invalid RainerScript expression:" exp)))))

(define-syntax-rule (rainer-script line ...)
  (string-join (list (rainer-script-line line) ...) ""))
(define-public %default-config
  (rsyslog-configuration (promtail (rsyslog-address (ip "127.0.0.1") (port 1514)))
                         (server (rsyslog-address (ip %edoras-vpn-ip-address) (port 514)))))


(define-public (test)
  (display (rsyslog-config-file %default-config)))

(define-public (rsyslog-config-file configuration)
  (match-record configuration <rsyslog-configuration>
    (forward promtail server)

    (define config-lines
      (rainer-script
       (module (load "imuxsock") (SysSock.use "on"))
       (module (load "imklog") (permitnonkernelfacility "on"))

       blank-line

       (conditional server
                    (module (load "imudp"))
                    (input (type "imudp")
                           (address (rsyslog-address-ip server))
                           (port (rsyslog-address-port server)))
                    (module (load "imtcp"))
                    (input (type "imtcp")
                           (address (rsyslog-address-ip server))
                           (port (rsyslog-address-port server))))

       blank-line
       (directive PrivDropToUser %rsyslog-user)
       (directive PrivDropToGroup %rsyslog-group)
       (directive FileOwner %rsyslog-user)
       (directive FileGroup %rsyslog-group)
       (directive FileCreateMode "0640")
       (directive DirCreateMode "0755")
       (directive Umask "0022")

       blank-line
       (list-template ShepherdFileFormat
                      timestamp
                      space
                      hostname
                      space
                      "shepherd:"
                      (property msg spifno1stsp)
                      msg
                      newline)

       blank-line
       (list-template ShepherdForwardFormat
                      "<30>" "1" space
                      (property timestamp (date-format "rfc3339")) space
                      hostname space "shepherd" space
                      procid space msgid space
                      structured-data space
                      msg newline)
       blank-line
       (if-block "$procid == 1"
                 (body
                  (action (type "omfile")
                          (file "/var/log/shepherd")
                          (template "ShepherdFileFormat"))
                  (conditional forward (forward-action "ShepherdForwardFormat" forward))
                  (conditional promtail (forward-action "ShepherdForwardFormat" promtail)))
                 (else-if "$syslogseverity-text == 'debug'"
                          (action (type "omfile")
                                  (file "/var/log/debug")
                                  (template "RSYSLOG_TraditionalFileFormat")))
                 (else-if "$syslogfacility-text == 'mail'"
                          (action (type "omfile")
                                  (file "/var/log/maillog")
                                  (template "RSYSLOG_TraditionalFileFormat")))
                 (otherwise
                  (if-block "$syslogfacility-text == 'authpriv' or $syslogfacility-text == 'auth'"
                            (action (type "omfile")
                                    (file "/var/log/secure")
                                    (template "RSYSLOG_TraditionalFileFormat")))
                  (action (type "omfile")
                          (file "/var/log/syslog")
                          (template "RSYSLOG_TraditionalFileFormat"))
                  (conditional forward (forward-action "RSYSLOG_SyslogProtocol23Format" forward))
                  (conditional promtail (forward-action "RSYSLOG_SyslogProtocol23Format" promtail))))))

    (string-append config-lines "\n")))


(define (rsyslog-print-config config-file)
  (shepherd-action
   (name 'config)
   (documentation "Print Rsyslog configuration.")
   (procedure #~(lambda (running . args)
                  (let ((output (call-with-input-file #$config-file get-string-all)))
                    (format #t "Using configuration file ~a:~%~%~a~%" #$config-file output))))))

(define (rsyslog-shepherd-service configuration)
  (define rsyslog.conf
    (plain-file "rsyslog.conf"
                (rsyslog-config-file configuration)))

  (list (shepherd-service
         (provision '(syslogd))
         (documentation "Run the Rsyslog daemon.")
         (actions (list (rsyslog-print-config rsyslog.conf)))
         (start #~(make-forkexec-constructor
                   (list (string-append #$rsyslog "/sbin/rsyslogd")
                         "-n"
                         "-i" #$%rsyslog-pid-file
                         "-f" #$rsyslog.conf)
                   #:directory #$%rsyslog-working-directory
                   #:log-file #$%rsyslog-log-file
                   #:pid-file #$%rsyslog-pid-file))
         (stop #~(make-kill-destructor)))))


(define (rsyslog-account config)
  (list (user-group
         (name %rsyslog-group)
         (system? #t))
        (user-account
         (name %rsyslog-user)
         (group %rsyslog-user)
         (system? #t)
         (comment "Rsyslog daemon user")
         (home-directory %rsyslog-working-directory)
         (shell (file-append shadow "/sbin/nologin")))))


;;;
;;; Rsyslog Service
;;;

(define (rsyslog-service-activation config)
  "Return the activation gexp for CONFIG."
  (with-imported-modules '((guix build utils)
                           (baulig build utils))
    #~(begin
        (use-modules (guix build utils)
                     (baulig build utils))
        (let ((uid (passwd:uid (getpw #$%rsyslog-user)))
              (gid (group:gid (getgr #$%rsyslog-group))))
          (create-file-with-owner "/var/log/syslog" uid gid #o640)
          (create-file-with-owner "/var/log/shepherd" uid gid #o640)
          (create-file-with-owner "/var/log/debug" uid gid #o640)
          (create-file-with-owner "/var/log/secure" uid gid #o640)
          (create-file-with-owner "/var/log/maillog" uid gid #o640)))))

(define-public rsyslog-service-type
  (service-type (name 'rsyslog-service)
                (extensions
                 (list (service-extension shepherd-root-service-type
                                          rsyslog-shepherd-service)
                       (service-extension account-service-type
                                          rsyslog-account)
                       (service-extension activation-service-type
                                          rsyslog-service-activation)))
                (description
                 "Run the the @command{rsyslogd} daemon. ")))
