;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services chrony)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services admin)
  #:use-module (gnu services configuration)
  #:use-module (gnu services shepherd)
  #:use-module (gnu system shadow)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages ntp)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (guix modules)
  #:use-module (guix packages)
  #:use-module (srfi srfi-1)
  #:use-module (rnrs enums)
  #:export (%chrony-servers
            chrony-configuration
            chrony-configuration?
            chrony-service-type))

;;; Commentary:
;;;
;;; Chrony service.
;;;
;;; Code:

(define %chrony-log-rotation
  (list (log-rotation
         (files '("/var/log/chronyd.log")))))

(define chrony-server-types (make-enumeration
                             '(pool server)))

(define-record-type* <chrony-server>
  chrony-server make-chrony-server
  chrony-server?
  (type chrony-server-type
        (default 'pool))
  (address chrony-server-address)
  (options chrony-server-options
           (default '())))

(define (chrony-server->string chrony-server)
  ;; Serialize the CHRONY server object as a string, ready to use in the CHRONY
  ;; configuration file.
  (define (flatten lst)
    (reverse
     (let loop ((x lst)
                (res '()))
       (if (list? x)
           (fold loop res x)
           (cons (format #f "~a" x) res)))))

  (match-record chrony-server <chrony-server>
                (type address options)
                ;; XXX: It'd be neater if fields were validated at the syntax level (for
                ;; static ones at least).  Perhaps the Guix record type could support a
                ;; predicate property on a field?
                (unless (enum-set-member? type chrony-server-types)
                  (error "Invalid CHRONY server type" type))
                (string-join (cons* (symbol->string type)
                                    address
                                    (flatten options)))))

(define %chrony-servers
  ;; Default set of CHRONY servers. These URLs are managed by the CHRONY Pool project.
  ;; Within Guix, Leo Famulari <leo@famulari.name> is the administrative contact
  ;; for this CHRONY pool "zone".
  ;; The full list of available URLs are 0.guix.pool.chrony.org,
  ;; 1.guix.pool.chrony.org, 2.guix.pool.chrony.org, and 3.guix.pool.chrony.org.
  (list
   (chrony-server
    (address "us.pool.ntp.org")
    (options '("iburst maxsources 5")))))

(define-record-type* <chrony-configuration>
  chrony-configuration make-chrony-configuration
  chrony-configuration?
  (chrony      chrony-configuration-chrony
               (default chrony))
  (servers  chrony-configuration-servers     ;list of <chrony-server> objects
            (default %chrony-servers)))

(define (chrony-shepherd-service config)
  (match-record config <chrony-configuration>
                (chrony servers)
                (define config
                  (string-append "driftfile /var/run/chrony/chrony.drift\n"
                                 (string-join (map chrony-server->string servers) "\n")
                                 "\nmakestep 1 -1\nmaxchange 1000 1 -1\n"))

                (define chronyd.conf
                  (plain-file "chronyd.conf" config))

                (list (shepherd-service
                       (provision '(chronyd))
                       (documentation "Run the Chrony daemon.")
                       (requirement '(user-processes networking))
                       (actions (list (shepherd-configuration-action chronyd.conf)))
                       (start #~(make-forkexec-constructor
                                 (list (string-append #$chrony "/sbin/chronyd") "-d"
                                       "-f" #$chronyd.conf "-u" "chronyd")
                                 #:log-file "/var/log/chronyd.log"))
                       (stop #~(make-kill-destructor))))))

(define %chrony-accounts
  (list (user-account
         (name "chronyd")
         (group "nogroup")
         (system? #t)
         (comment "Chrony daemon user")
         (home-directory "/var/empty")
         (shell (file-append shadow "/sbin/nologin")))))


(define (chrony-service-activation config)
  "Return the activation gexp for CONFIG."
  (with-imported-modules '((guix build utils))
    #~(begin
        (use-modules (guix build utils))
        (define %user
          (getpw "chronyd"))

        (let ((directory "/var/run/chronyd"))
          (mkdir-p directory)
          (chown directory (passwd:uid %user) (passwd:gid %user))))))

(define chrony-service-type
  (service-type (name 'chrony)
                (extensions
                 (list (service-extension shepherd-root-service-type
                                          chrony-shepherd-service)
                       (service-extension account-service-type
                                          (const %chrony-accounts))
                       (service-extension activation-service-type
                                          chrony-service-activation)
                       (service-extension rottlog-service-type
                                          (const %chrony-log-rotation))))
                (description
                 "Run the the @command{chronyd} daemon. ")
                (default-value (chrony-configuration))))

;;; chrony.scm ends here
