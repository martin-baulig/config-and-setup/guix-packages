;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services grafana)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (gnu services shepherd)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (baulig packages grafana)
  #:use-module (baulig services account)
  #:use-module (baulig services activation)
  #:use-module (baulig services parameters)
  #:use-module (baulig services secrets)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization syntax-macros)
  #:use-module (baulig services serialization underscore-key-value)
  #:use-module (baulig build config-utils)
  #:use-module (rnrs enums)
  #:use-module (srfi srfi-1)
  #:use-module (web uri))


;;;
;;; Configuration Serialization
;;;

(define-syntax define-parameter-constant
  (syntax-rules (working-directory pid-file pid-directory log-directory)
    ((_ name pid-file)
     (define-string-constant underscore-equals))
    ((_ name working-directory file)
     (define-string-constant
       underscore-equals
       name
       (format #f "@parameter:working-directory@/~a" file)))
    ((_ name log-directory file)
     (define-string-constant
       underscore-equals
       name
       (format #f "@parameter:log-directory@/~a" file)))
    ((_ name pid-directory file)
     (define-string-constant
       underscore-equals
       name
       (format #f "@parameter:pid-directory@/~a" file)))
    ((_ name value)
     (define-string-constant underscore-equals name value))))


(define-parameter-constant grafana-log-directory log-directory "grafana")
(define-parameter-constant grafana-socket-path pid-directory "grafana.socket")

(define-parameter-constant grafana-min-tls-version "TLS1.3")
(define-parameter-constant grafana-server-protocol "socket")
(define-parameter-constant grafana-socket-gid "@parameter:pid-directory-gid@")
(define-parameter-constant grafana-database-type "postgres")

(define-parameter-constant grafana-postgres-socket "/var/run/postgresql")


;;;
;;; Grafana Paths Section
;;;

(define-public-configuration grafana-paths-section
  (data
   path-name
   "Path to where Grafana stores the sqlite3 database and other data.")

  (logs
   (grafana-log-directory-constant %grafana-log-directory)
   "Path to where Grafana stores logs."))

(define-section-serializer header-key-value grafana-paths-section "paths")


;;;
;;; Grafana Server Section
;;;

(define-public-configuration grafana-server-section
  (min-tls-version
   (grafana-min-tls-version-constant %grafana-min-tls-version)
   "Minimum TLS version.")

  (protocol
   (grafana-server-protocol-constant %grafana-server-protocol)
   "Server protocol.")

  (socket
   (grafana-socket-path-constant %grafana-socket-path)
   "Path where the socket should be created.")

  (socket-gid
   (grafana-socket-gid-constant %grafana-socket-gid)
   "GID of the socket.")

  (root-url
   uri
   "Full URL used to access Grafana from a web browser.")

  (domain
   host-name
   "Server domain name.")

  (serve-from-sub-path
   (boolean #t)
   "Serve Grafana from subpath specified in 'root-url' setting.")

  (http-addr
   maybe-ipv4-address
   "HTTP listen address.")

  (http-port
   maybe-integer
   "HTTP listen port."))

(define-section-serializer header-key-value grafana-server-section "server")


;;;
;;; Grafana Security Section
;;;

(define-public-configuration grafana-security-section
  (admin-user
   user-name
   "Admin user name.")

  (admin-password
   secrets-service-password
   "Admin password.")

  (secret-key
   secrets-service-password
   "Used for signing some data source settings like secrets and passwords.")

  (strict-transport-security
   true-constant
   "Enable HTTP Strict-Transport-Security."))

(define-section-serializer header-key-value grafana-security-section "security")


;;;
;;; Grafana Database Section
;;;

(define-public-configuration grafana-database-section
  (type
   (grafana-database-type-constant %grafana-database-type)
   "Database type; hard-coded to use PostgreSQL.")

  (host
   (grafana-postgres-socket-constant %grafana-postgres-socket)
   "Path name of the PostgeSQL Unix-Domain socket.")

  (user
   (user-name "grafana")
   "The database user."))

(define-section-serializer header-key-value grafana-database-section "database")


;;;
;;; Grafana Configuration
;;;

(define-public-configuration grafana-configuration
  (instance-name
   maybe-host-name
   "Grafana instance name")

  (paths
   grafana-paths-section
   "Configure various paths.  This section is mostly populated automatically.")

  (server
   (grafana-server-section (grafana-server-section))
   "Grafana server section.")

  (database
   (grafana-database-section (grafana-database-section))
   "Grafana database section.")

  (security
   (grafana-security-section)
   "Grafana security section.")

  (trusted-ca-file
   secrets-service-blob
   "We install this as a trusted CA."
   (serializer empty-serializer)))


(define-section-serializer key-value grafana-configuration "grafana")


;;;
;;; Grafana Service
;;;


(define-record-type* <grafana-service>
  grafana-service
  make-grafana-service
  grafana-service?

  (secrets              grafana-service-secrets)
  (configuration        grafana-service-configuration))


(define* (resolve-grafana configuration #:key secrets parameters)
  (match-records* ((configuration <grafana-configuration>))
      (grafana-service
       (configuration configuration)
       (secrets secrets))))


(define* (resolve-grafana-secrets configuration #:key parameters)
  (match-records* ((configuration <grafana-configuration>))
      (create-secrets
          (daemon-account 'grafana
                          (supplementary-groups '("postgres-socket"))
                          (daemon (pid-directory-group "grafana-socket")))
          (serialize "grafana.ini" grafana configuration)
        (extra-parameters gexp #~((grafana . #$grafana))))))


(define (resolve-grafana-activation service)
  (match-records* ((service <grafana-service> configuration secrets)
                   (configuration <grafana-configuration> trusted-ca-file)
                   (secrets <secrets-service> database account requirements)
                   (account <service-account> working-directory))

      ;; To allow Grafana to connect to HTTPS servers that are using certification
      ;; from our local CA, we copy the 'trusted-ca-file' and the root certificates
      ;; into a file called "ca-certs.crt" in the service's working directory and
      ;; then set the SSL_CERT_FILE environment variable to it.
      (define activation-action
        #~(begin
            (let* ((secrets-file (secrets-file-open #$database))
                   (user #$(service-account-user account))
                   (ca (secrets-file-get-item secrets-file #$trusted-ca-file #:type 'blob))
                   (ca-file (string-append #$working-directory "/ca-certs.crt"))
                   (root-cert-path "/etc/ssl/certs/ca-certificates.crt")
                   (root-certs (call-with-input-file root-cert-path get-string-all)))
              (format #t "Grafana setup script.~%")
              (call-with-output-file ca-file
                (lambda (out)
                  (put-string out root-certs)
                  (newline out)
                  (put-bytevector out (base16-string->bytevector ca)))))))

    (activation-task
     (id 'grafana-ca-certs)
     (requirements requirements)
     (action activation-action))))



(define (resolve-grafana-shepherd service)
  (match-records* ((service <grafana-service> secrets configuration)
                   (secrets <secrets-service> account output-file requirements)
                   (account <service-account> user group supplementary-groups
                            working-directory log-file-path pid-file-path))
      (list
       (shepherd-service
        (provision '(grafana))
        (documentation "Grafana Service")
        (requirement `(networking postgres openvpn activation-task-grafana-ca-certs
                       ,@requirements))
        (respawn? #f)
        (start #~(make-forkexec-constructor
                  (list #$(file-append grafana "/bin/grafana")
                        "server"
                        "--config" #$output-file
                        "--homepath" #$grafana)
                  #:directory #$working-directory
                  #:user #$user
                  #:group #$group
                  #:supplementary-groups (list #$@supplementary-groups)
                  #:log-file #$log-file-path
                  #:environment-variables
                  `(,(string-append "SSL_CERT_FILE=" #$working-directory "/ca-certs.crt"))))
        (stop #~(make-kill-destructor))))))


(define-public grafana-service-type
  (service-type (name 'grafana)
                (extensions
                 (list
                  (service-extension activation-task-service-type
                                     resolve-grafana-activation)
                  (service-extension shepherd-root-service-type
                                     resolve-grafana-shepherd)))
                (compose identity)
                (description "Grafana Service")))


(define-parameterized-service grafana
  (record-type <grafana-configuration>)
  (parent secrets resolve-grafana-secrets)
  (resolve resolve-grafana))
