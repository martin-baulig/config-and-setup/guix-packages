;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services loki)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (baulig packages loki)
  #:use-module (baulig services account)
  #:use-module (baulig services parameters)
  #:use-module (baulig services secrets)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization syntax-macros)
  #:use-module (baulig services serialization yaml)
  #:use-module (baulig services serialization loki)
  #:use-module (baulig services serialization prometheus)
  #:use-module (baulig build config-utils)
  #:use-module (rnrs enums)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (web uri))


;;;
;;; Loki Common Section
;;;

(define-public-configuration/no-serialization loki-common-section)

(define (serialize-loki-common-section _ value)
  (string-join `("common:"
                 "  replication_factor: 1"
                 "  ring:"
                 "    kvstore:"
                 "      store: inmemory")
               "\n" 'suffix))


;;;
;;; Loki Server Config
;;;

(define-public-configuration loki-server-configuration
  (http-listen-address
   ipv4-address
   "Address to listen on.")

  (http-listen-port
   (integer 3100)
   "Port to listen on.")

  (http-path-prefix
   maybe-string
   "Base path to serve all API routes from.")

  (log-level
   maybe-loki-log-level
   "Log devel.")

  (http-tls-config
   loki-tls-server-config
   "TLS Configuration"))

(define-section-serializer yaml loki-server-configuration "server")

(define-public-maybe loki-server-configuration)


;;;
;;; Loki Period Index
;;;

(define-string-constant yaml loki-index-prefix "index_")
(define-string-constant yaml loki-index-period "24h")

(define-public-configuration loki-period-index
  (prefix
   (loki-index-prefix-constant %loki-index-prefix)
   "Prefix.")

  (period
   (loki-index-period-constant %loki-index-period)
   "Period."))

(define-section-serializer yaml loki-period-index "index")


;;;
;;; Loki Period Config
;;;

(define-string-constant yaml loki-period-start "2023-10-20")
(define-string-constant yaml loki-period-store "tsdb")
(define-string-constant yaml loki-period-object-store "filesystem")
(define-string-constant yaml loki-period-schema "v12")

(define-public-configuration loki-period-configuration
  (from
   (loki-period-start-constant %loki-period-start)
   "The date of the first day that index buckets should be created.")

  (store
   (loki-period-store-constant %loki-period-store)
   "Hard-coded to tsdb.")

  (object-store
   (loki-period-object-store-constant %loki-period-object-store)
   "Hard-coded to filesystem.")

  (schema
   (loki-period-schema-constant %loki-period-schema)
   "The schema version to use.")

  (index
   (loki-period-index (loki-period-index))
   "Index."))

(define-section-serializer yaml loki-period-configuration "period_config")

(define-array-section-serializer yaml loki-period-configuration "configs")


;;;
;;; Loki Storage Configuration
;;;

(define-string-constant yaml loki-tsdb-index "@parameter:data-directory@/tsdb-index")
(define-string-constant yaml loki-tsdb-cache "@parameter:data-directory@/tsdb-cache")

(define-public-configuration loki-tsdb-storage
  (active-index-directory
   (loki-tsdb-index-constant %loki-tsdb-index)
   "Storage path name.")

  (cache-location
   (loki-tsdb-cache-constant %loki-tsdb-cache)
   "Storage path name."))

(define-section-serializer yaml loki-tsdb-storage "tsdb_shipper")

(define-string-constant yaml loki-filesystem-directory "@parameter:data-directory@/data")

(define-public-configuration loki-filesystem-storage
  (directory
   (loki-filesystem-directory-constant %loki-filesystem-directory)
   "Storage path name."))

(define-section-serializer yaml loki-filesystem-storage "filesystem")


(define-public-configuration loki-storage-configuration
  (tsdb
   (loki-tsdb-storage (loki-tsdb-storage))
   "TSDB Storage.")

  (filesystem
   (loki-filesystem-storage (loki-filesystem-storage))
   "Filesystem Storage."))

(define-section-serializer yaml loki-storage-configuration "storage_config")


;;;
;;; Loki Schema Configuration
;;;

(define-public-configuration loki-schema-configuration
  (configs
   (list-of-loki-period-configuration (list (loki-period-configuration)))
   "Period configs."))

(define-section-serializer yaml loki-schema-configuration "schema_config")


;;;
;;; Loki Compactor Configuration
;;;

(define-string-constant yaml loki-compactor-directory "@parameter:data-directory@/compactor")

(define-public-configuration loki-compactor-configuration
  (working-directory
   (loki-compactor-directory-constant %loki-compactor-directory)
   "Working directory.")

  (shared-store
   (loki-period-object-store-constant %loki-period-object-store)
   "Hard-coded to filesystem.")

  (retention-enabled
   true-constant
   "Enable retention."))

(define-section-serializer yaml loki-compactor-configuration "compactor")


;;;
;;; Loki WAL Configuration
;;;

(define-string-constant yaml loki-wal-directory "@parameter:data-directory@/wal")

(define-public-configuration loki-wal-configuration
  (dir
   (loki-wal-directory-constant %loki-wal-directory)
   "WAL (Write Ahead Log) directory."))

(define-section-serializer yaml loki-wal-configuration "wal")


;;;
;;; Loki Ingester Configuration
;;;

(define-public-configuration loki-ingester-configuration
  (wal
   (loki-wal-configuration (loki-wal-configuration))
   "WAL Configuration."))

(define-section-serializer yaml loki-ingester-configuration "ingester")


;;;
;;; Loki Limits Configuration
;;;

(define-public-configuration loki-limits-configuration
  (ingestion-rate-mb
   maybe-integer
   "Per-user ingestion rate limit in sample size per second. Units in MB.")

  (ingestion-burst-size-mb
   maybe-integer
   "Per-user allowed ingestion burst size (in sample size).")

  (reject-old-samples
   maybe-boolean
   "Whether or not old samples will be rejected.")

  (reject-old-samples-max-age
   maybe-prometheus-duration
   "Maximum accepted sample age before rejecting."))

(define-section-serializer yaml loki-limits-configuration "limits_config")

(define-public-maybe loki-limits-configuration)


;;;
;;; Loki Configuration
;;;

(define-public-configuration loki-configuration
  (auth-enabled
   false-constant
   "This is only required in multi-tenant mode.")

  (data-directory
   path-name
   "Loki Data directory."
   (serializer empty-serializer))

  (server
   loki-server-configuration
   "Server config.")

  (common
   (loki-common-section (loki-common-section))
   "Common section.")

  (compactor
   (loki-compactor-configuration (loki-compactor-configuration))
   "Compactor config.")

  (schema
   (loki-schema-configuration (loki-schema-configuration))
   "Schema config.")

  (storage
   (loki-storage-configuration (loki-storage-configuration))
   "Storage config.")

  (limits
   maybe-loki-limits-configuration
   "Limits config.")

  (ingester
   (loki-ingester-configuration (loki-ingester-configuration))
   "Ingester config."))

(define-section-serializer yaml loki-configuration "loki")


;;;
;;; Loki Service
;;;


(define-record-type* <loki-service>
  loki-service
  make-loki-service
  loki-service?

  (secrets              loki-service-secrets)
  (configuration        loki-service-configuration))


(define* (resolve-loki configuration #:key secrets parameters)
  (match-records* ((configuration <loki-configuration>))
      (loki-service
       (configuration configuration)
       (secrets secrets))))


(define* (resolve-loki-secrets configuration #:key parameters)
  (match-records* ((configuration <loki-configuration> data-directory))
      (create-secrets
          (daemon-account 'loki)
          (serialize "loki.yaml" loki configuration)
        (extra-parameters (data-directory data-directory)))))


(define (resolve-loki-shepherd service)
  (match-records* ((service <loki-service> secrets configuration)
                   (configuration <loki-configuration>)
                   (secrets <secrets-service> account private-directory output-file requirements)
                   (account <service-account> user group supplementary-groups
                            working-directory log-file-path pid-file-path))
      (let* ((args `("-config.file" ,output-file
                     "-log.level" "warn")))
        (list
         (shepherd-service
          (provision '(loki))
          (documentation "Loki Service")
          (requirement `(networking ,@requirements))
          (respawn? #f)
          (start #~(make-forkexec-constructor
                    (list #$(file-append loki "/bin/loki") #$@args)
                    #:directory #$working-directory
                    #:user #$user
                    #:group #$group
                    #:supplementary-groups (list #$@supplementary-groups)
                    #:log-file #$log-file-path))
          (stop #~(make-kill-destructor)))))))


(define-public loki-service-type
  (service-type (name 'loki)
                (extensions
                 (list (service-extension shepherd-root-service-type
                                          resolve-loki-shepherd)))
                (compose identity)
                (description "Loki Service")))


(define-parameterized-service loki
  (record-type <loki-configuration>)
  (parent secrets resolve-loki-secrets)
  (resolve resolve-loki))
