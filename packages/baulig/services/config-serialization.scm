 ;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
 ;;;
 ;;; This file is NOT part of GNU Guix.
 ;;;
 ;;; This program is free software: you can redistribute it and/or modify
 ;;; it under the terms of the GNU Affero General Public License as
 ;;; published by the Free Software Foundation, either version 3 of the
 ;;; License, or (at your option) any later version.

 ;;; This program is distributed in the hope that it will be useful,
 ;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
 ;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ;;; GNU Affero General Public License for more details.

 ;;; You should have received a copy of the GNU Affero General Public License
 ;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services config-serialization)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module ((gnu services configuration) #:hide (serialize-configuration))
  #:use-module (guix records)
  #:use-module (guix build utils)
  #:use-module (rnrs enums)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-26)
  #:use-module (baulig build config-utils)
  #:export-syntax (define-section-serializer
                    define-array-section-serializer
                    define-enum-serializer
                    define-public-configuration
                    define-public-configuration/no-serialization
                    define-public-maybe
                    define-public-maybe/no-serialization
                    define-string-constant
                    with-default
                    with-default-list)
  #:re-export (maybe-value-set?
              empty-serializer))

;;;
;;; Macros
;;;

(define-public (serialize-configuration config fields)
  (apply string-append
         (map (lambda (field)
                ((configuration-field-serializer field)
                 (configuration-field-name field)
                 ((configuration-field-getter field) config)))
              fields)))

(define-syntax internal-make-section-serializer
  (syntax-rules ()
    ((_ section-func section serializer-name fields-name)
     #`(define (serializer-name field-name configuration)
         (section-func section
                       (serialize-configuration configuration fields-name))))))


(define-syntax define-section-serializer
  (lambda (x)
    (define (make-serializer-name name)
      (datum->syntax name
                     (symbol-append 'serialize- (syntax->datum name))))

    (define (make-fields-name name)
      (datum->syntax name
                     (symbol-append (syntax->datum name) '-fields)))

    (syntax-case x (yaml header-key-value key-value key-value-fragment space-separated)
      ((_ key-value name section)
       (and (identifier? #'name) (string? (syntax->datum #'section)))
       (internal-make-section-serializer config-section-key-value
                                         (syntax->datum #'section)
                                         #,(make-serializer-name #'name)
                                         #,(make-fields-name #'name)))

      ((_ key-value-fragment name)
       (identifier? #'name)
       (internal-make-section-serializer config-section-key-value-fragment
                                         #f
                                         #,(make-serializer-name #'name)
                                         #,(make-fields-name #'name)))

      ((_ header-key-value name section)
       (and (identifier? #'name) (string? (syntax->datum #'section)))
       (internal-make-section-serializer config-section-header-key-value
                                         (syntax->datum #'section)
                                         #,(make-serializer-name #'name)
                                         #,(make-fields-name #'name)))

      ((_ space-separated name section)
       (and (identifier? #'name) (string? (syntax->datum #'section)))
       (internal-make-section-serializer config-section-space-separated
                                         (syntax->datum #'section)
                                         #,(make-serializer-name #'name)
                                         #,(make-fields-name #'name)))

      ((_ yaml name section)
       (and (identifier? #'name) (string? (syntax->datum #'section)))
       (internal-make-section-serializer config-section-yaml
                                         (syntax->datum #'section)
                                         #,(make-serializer-name #'name)
                                         #,(make-fields-name #'name))))))


(define-syntax define-public-configuration
  (lambda (x)
    (define (make-predicate-name name)
      (datum->syntax name
                     (symbol-append (syntax->datum name) '?)))

    (define (make-serializer-name name)
      (datum->syntax name
                     (symbol-append 'serialize- (syntax->datum name))))

    (define (make-fields-name name)
      (datum->syntax name
                     (symbol-append (syntax->datum name) '-fields)))

    (syntax-case x ()
      ((_ name args ...)
       (identifier? #'name)
       (with-syntax ((predicate (make-predicate-name #'name))
                     (serializer (make-serializer-name #'name))
                     (fields (make-fields-name #'name)))
         #'(begin
             (define-configuration name args ...)
             (export name predicate serializer fields)))))))


(define-syntax define-public-configuration/no-serialization
  (lambda (x)
    (define (make-predicate-name name)
      (datum->syntax name
                     (symbol-append (syntax->datum name) '?)))

    (define (make-serializer-name name)
      (datum->syntax name
                     (symbol-append 'serialize- (syntax->datum name))))

    (define (make-fields-name name)
      (datum->syntax name
                     (symbol-append (syntax->datum name) '-fields)))

    (syntax-case x ()
      ((_ name args ...)
       (identifier? #'name)
       (with-syntax ((predicate (make-predicate-name #'name))
                     (serializer (make-serializer-name #'name))
                     (fields (make-fields-name #'name)))
         #'(begin
             (define-configuration/no-serialization name args ...)
             (export name predicate serializer fields)))))))


(define-syntax define-public-maybe
  (lambda (x)
    (define (make-predicate-name name)
      (datum->syntax name
                     (symbol-append 'maybe- (syntax->datum name) '?)))

    (define (make-serializer-name name)
      (datum->syntax name
                     (symbol-append 'serialize-maybe- (syntax->datum name))))

    (syntax-case x ()
      ((_ name)
       (identifier? #'name)
       (with-syntax ((predicate (make-predicate-name #'name))
                     (serializer (make-serializer-name #'name)))
         #'(begin
             (define-maybe name)
             (export predicate serializer)))))))


(define-syntax define-public-maybe/no-serialization
  (lambda (x)
    (define (make-predicate-name name)
      (datum->syntax name
                     (symbol-append 'maybe- (syntax->datum name) '?)))

    (syntax-case x ()
      ((_ name)
       (identifier? #'name)
       (with-syntax ((predicate (make-predicate-name #'name)))
         #'(begin
             (define-maybe/no-serialization name)
             (export predicate)))))))



(define-syntax define-string-constant
  (lambda (x)
    (define (make-constant-name name)
      (datum->syntax name
                     (symbol-append '% (syntax->datum name))))

    (define (make-predicate-name name)
      (datum->syntax name
                     (symbol-append (syntax->datum name) '-constant?)))

    (define (make-serializer-name name)
      (datum->syntax name
                     (symbol-append 'serialize- (syntax->datum name) '-constant)))

    (syntax-case x (yaml key-value underscore-equals underscore-space-semicolon)
      ((_ key-value name constant-value)
       (identifier? #'name)
       (with-syntax ((constant (make-constant-name #'name))
                     (predicate (make-predicate-name #'name))
                     (serializer (make-serializer-name #'name)))
         #`(begin
             (define-public constant constant-value)
             (define-public (predicate value)
               (equal? value constant))
             (define-public (serializer field-name value)
               (let ((field (config-field-name space field-name)))
                 (format #f "~a = ~s~%" field value))))))

      ((_ underscore-equals name constant-value)
       (identifier? #'name)
       (with-syntax ((constant (make-constant-name #'name))
                     (predicate (make-predicate-name #'name))
                     (serializer (make-serializer-name #'name)))
         #`(begin
             (define-public constant constant-value)
             (define-public (predicate value)
               (equal? value constant))
             (define-public (serializer field-name value)
               (let ((field (config-field-name underscore field-name)))
                 (format #f "~a = ~a~%" field value))))))

      ((_ underscore-space-semicolon name constant-value)
       (identifier? #'name)
       (with-syntax ((constant (make-constant-name #'name))
                     (predicate (make-predicate-name #'name))
                     (serializer (make-serializer-name #'name)))
         #`(begin
             (define-public constant constant-value)
             (define-public (predicate value)
               (equal? value constant))
             (define-public (serializer field-name value)
               (let ((field (config-field-name underscore field-name)))
                 (format #f "~a ~a;~%" field value))))))

      ((_ yaml name constant-value)
       (identifier? #'name)
       (with-syntax ((constant (make-constant-name #'name))
                     (predicate (make-predicate-name #'name))
                     (serializer (make-serializer-name #'name)))
         #`(begin
             (define-public constant constant-value)
             (define-public (predicate value)
               (equal? value constant))
             (define-public (serializer field-name value)
               (let ((field (config-field-name yaml field-name)))
                 (format #f "~a: ~s~%" field value)))))))))


(define-syntax define-array-section-serializer
  (lambda (x)
    (define (make-predicate-name name)
      (datum->syntax name
                     (symbol-append 'list-of- (syntax->datum name) '?)))

    (define (make-item-predicate-name name)
      (datum->syntax name
                     (symbol-append (syntax->datum name) '?)))

    (define (make-serializer-name name)
      (datum->syntax name
                     (symbol-append 'serialize-list-of- (syntax->datum name))))

    (define (make-fields-name name)
      (datum->syntax name
                     (symbol-append (syntax->datum name) '-fields)))

    (syntax-case x (yaml key-value key-value-fragment)
      ((_ key-value name section)
       (identifier? #'name)
       (with-syntax ((predicate (make-predicate-name #'name))
                     (item-predicate (make-item-predicate-name #'name))
                     (serializer-name (make-serializer-name #'name))
                     (fields-name (make-fields-name #'name)))
         #`(begin
             (define (predicate value)
               (and (list? value)
                    (every item-predicate value)))
             (define (serializer-name field-name value)
               (array-config-section-key-value section
                                               (map (cut serialize-configuration <> fields-name)
                                                    value))))))

      ((_ key-value-fragment name)
       (identifier? #'name)
       (with-syntax ((predicate (make-predicate-name #'name))
                     (item-predicate (make-item-predicate-name #'name))
                     (serializer-name (make-serializer-name #'name))
                     (fields-name (make-fields-name #'name)))
         #`(begin
             (define (predicate value)
               (and (list? value)
                    (every item-predicate value)))
             (define (serializer-name field-name value)
               (array-config-section-key-value-fragment
                (map (cut serialize-configuration <> fields-name)
                     value))))))

      ((_ yaml name section)
       (identifier? #'name)
       (with-syntax ((predicate (make-predicate-name #'name))
                     (item-predicate (make-item-predicate-name #'name))
                     (serializer-name (make-serializer-name #'name))
                     (fields-name (make-fields-name #'name)))
         #`(begin
             (define (predicate value)
               (and (list? value)
                    (every item-predicate value)))
             (define (serializer-name field-name value)
               (array-config-section-yaml section
                                          (map (cut serialize-configuration <> fields-name)
                                               value)))))))))


;;;
;;; Macros
;;;

(define-syntax with-default
  (syntax-rules (function)
    ((_ field default)
     (if (maybe-value-set? field) field default))
    ((_ field default arg)
     (if (maybe-value-set? field) arg default))
    ((_ field default function func)
     (if (maybe-value-set? field) (func field) default))))


(define-syntax with-default-list
  (syntax-rules (function)
    ((_ field)
     (if (maybe-value-set? field) (list field) '()))
    ((_ field arg)
     (if (maybe-value-set? field) (list arg) '()))
    ((_ field function func)
     (if (maybe-value-set? field) (list (func field)) '()))))


;;;
;;; Enums
;;;

(define-syntax internal-make-enum-serializer
  (lambda (x)
    (define (make-predicate-name name)
      (datum->syntax name
                     (symbol-append (syntax->datum name) '?)))

    (define (make-serializer-name name)
      (datum->syntax name
                     (symbol-append 'serialize- (syntax->datum name))))

    (syntax-case x ()
      ((_ type kind format-spec func)
       (with-syntax ((predicate (make-predicate-name #'type))
                     (serializer (make-serializer-name #'type)))
         #`(begin
             (define-public (predicate value)
               (enum-set-member? value type))
             (define-public (serializer field-name value)
               (let ((field (config-field-name kind field-name)))
                 (format #f format-spec field (func value))))))))))


(define-syntax define-enum-serializer
  (syntax-rules (yaml key-value underscore-equals underscore-space underscore-space-semicolon)
    ((_ key-value type func)
     (internal-make-enum-serializer type space "~a = ~a~%" func))

    ((_ key-value type)
     (internal-make-enum-serializer type space "~a = ~a~%" symbol->string))

    ((_ underscore-equals type func)
     (internal-make-enum-serializer type underscore "~a = ~a~%" func))

    ((_ underscore-equals type)
     (internal-make-enum-serializer type underscore "~a = ~a~%" symbol->string))

    ((_ underscore-space type func)
     (internal-make-enum-serializer type underscore "~a ~a~%" func))

    ((_ underscore-space type)
     (internal-make-enum-serializer type underscore "~a ~a~%" symbol->string))

    ((_ underscore-space-semicolon type func)
     (internal-make-enum-serializer type underscore "~a ~a;~%" func))

    ((_ underscore-space-semicolon type)
     (internal-make-enum-serializer type underscore "~a ~a;~%" symbol->string))

    ((_ yaml type func)
     (internal-make-enum-serializer type yaml "~a: ~a~%" func))

    ((_ yaml type)
     (internal-make-enum-serializer type yaml "~a: ~a~%" symbol->string))))
