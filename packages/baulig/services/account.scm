;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services account)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services admin)
  #:use-module (gnu services configuration)
  #:use-module (gnu services shepherd)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages guile)
  #:use-module (gnu system shadow)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (guix modules)
  #:use-module (guix monads)
  #:use-module (guix store)
  #:use-module (ice-9 exceptions)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:use-module (srfi srfi-1)
  #:use-module (baulig services activation)
  #:use-module (baulig services parameters)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization no-serialization)
  #:export (<account-configuration>

            make-daemon-account

            parameterized-service-account-service

            <service-account>
            service-account
            service-account-id
            service-account-user
            service-account-group
            service-account-supplementary-groups
            service-account-create-account?
            service-account-working-directory
            service-account-daemon-account?

            service-account-pid-directory
            service-account-pid-file-path
            service-account-log-directory
            service-account-log-file-path
            service-account-log-group))


;;;
;;; Configuration Helpers
;;;

(define (list-of-user-name? value)
  (or (not (maybe-value-set? value))
      (and (list? value)
           (every user-name? value))))


;;;
;;; Daemon Account Configuration
;;;

(define-public-configuration/no-serialization daemon-account-configuration
  (pid-directory
   maybe-path-name
   "The PID directory.

Default: /var/run/services
")

  (pid-directory-group
   maybe-user-name
   "Make PID directory group-owned by this group.")

  (pid-file
   maybe-file-name
   "File name part of the PID file.

Default: <account.id>.pid
")

  (log-directory
   maybe-path-name
   "Directory where to place log files.

Default: /var/log/services")

  (log-file
   maybe-path-name
   "Full path name of the log file.

Default: <log-directory>/<account.id>.log
"))

(define-public-maybe/no-serialization daemon-account-configuration)


;;;
;;; Account Configuration
;;;

(define-public-configuration/no-serialization account-configuration
  (id
   account-id
   "Unique ID identifying this account.")

  (create-account?
   (boolean #t)
   "Whether or not to create a system user / group.

If #f, then both 'user' and 'group' will default to 'root'.
If #t, then 'user' and 'group' will default to the stringified
version of 'id'.")

  (create-directories?
   (boolean #t)
   "Whether or not to create the account directories.")

  (root-owned-home?
   (boolean #f)
   "Whether the home directory should be root owned.")

  (user
   maybe-user-name
   "User name.")

  (group
   maybe-user-name
   "Group name.")

  (supplementary-groups
   (list-of-user-name '())
   "Supplementary groups for this user account.")

  (working-directory
   maybe-path-name
   "Working directory.

By default, the working directory will be computed based on the
service root and 'id' field.")

  (daemon
   maybe-daemon-account-configuration
   "Optional Daemon Account Configuration."))


(define-public-maybe/no-serialization account-configuration)


(define-public-configuration/no-serialization account-reference
  (id
   account-id
   "Unique ID identifying this account."))


(define-public-maybe/no-serialization account-reference)


;;;
;;; Record types
;;;


(define-record-type* <service-account>
  service-account
  make-service-account
  service-account?

  (id                       service-account-id)
  (user                     service-account-user)
  (group                    service-account-group)
  (supplementary-groups     service-account-supplementary-groups    (default '()))

  (is-reference?            service-account-is-reference?)

  (create-account?          service-account-create-account?)
  (create-directories?      service-account-create-directories?)
  (working-directory        service-account-working-directory)
  (root-owned-home?         service-account-root-owned-home?        (default #f))

  (daemon-account?          service-account-daemon-account?         (default #f))

  (pid-directory            service-account-pid-directory           (default %unset-value))
  (pid-directory-group      service-account-pid-directory-group     (default %unset-value))
  (pid-file-path            service-account-pid-file-path           (default %unset-value))

  (log-directory            service-account-log-directory           (default %unset-value))
  (log-file-path            service-account-log-file-path           (default %unset-value))
  (log-group                service-account-log-group               (default %unset-value)))


;;;
;;; Exception
;;;

(define-exception-type service-account-error &programming-error
  make-service-account-error
  service-account-error?

  (message                  service-account-error-message))


(define* (make-service-account-error* format-string #:rest args)
  (make-service-account-error (apply format `(#f ,format-string ,@args))))


;;;
;;; Resolve
;;;

(define* (make-daemon-account account #:optional daemon-defaults)
  (define (merge-daemon-config daemon default)
    (match-record daemon <daemon-account-configuration>
      (pid-directory pid-directory-group pid-file log-directory log-file)

      (daemon-account-configuration
       (pid-directory (with-default
                       pid-directory
                       (daemon-account-configuration-pid-directory default)))
       (pid-directory-group (with-default
                             pid-directory-group
                             (daemon-account-configuration-pid-directory-group default)))
       (pid-file (with-default
                  pid-file
                  (daemon-account-configuration-pid-file default)))
       (log-directory (with-default
                       log-directory
                       (daemon-account-configuration-log-directory default)))
       (log-file (with-default
                  log-file
                  (daemon-account-configuration-log-file default))))))

  (match-record account <account-configuration>
    (id daemon)
    (cond ((daemon-account-configuration? daemon-defaults)
           (account-configuration
            (inherit account)
            (daemon (merge-daemon-config
                     (with-default daemon (daemon-account-configuration))
                     daemon-defaults))))
          ((eq? daemon-defaults #f)
           (account-configuration
            (inherit account)
            (daemon (with-default daemon (daemon-account-configuration)))))
          (else (error "Invalid argument" daemon-defaults)))))



(define* (resolve-service-account configuration #:key parameters)
  (match-records* ((configuration <account-configuration> id user group
                                  supplementary-groups create-account?
                                  create-directories? working-directory
                                  root-owned-home? daemon))

      (define basic-account
        (let* ((root (system-parameters-service-root parameters))
               (real-name (symbol->string id))
               (real-user (with-default user (if create-account? real-name "root")))
               (real-group (with-default group real-user))
               (real-working-dir (with-default working-directory
                                               (string-append root "/" real-name))))
          (service-account
           (id id)
           (create-account? create-account?)
           (create-directories? create-directories?)
           (user real-user)
           (group real-group)
           (supplementary-groups supplementary-groups)
           (working-directory real-working-dir)
           (root-owned-home? root-owned-home?)
           (is-reference? #f))))

    (define (resolve-daemon-account account)
      (match-records* ((daemon <daemon-account-configuration> pid-directory
                               pid-directory-group pid-file
                               log-directory log-file))
          (let* ((real-name (symbol->string (service-account-id account)))
                 (real-name (symbol->string id))
                 (real-user (with-default user (if create-account? real-name "root")))
                 (real-group (with-default group real-user))
                 (real-pid-directory-group
                  (with-default pid-directory-group real-group))
                 (real-supplementary-groups
                  `(,@supplementary-groups
                    ,@(if (maybe-value-set? pid-directory-group)
                          (list pid-directory-group)
                          '())))
                 (real-pid-dir
                  (with-default
                   pid-directory
                   (string-append (system-parameters-service-run-root parameters)
                                  "/" real-name)))
                 (real-pid-file
                  (string-append real-pid-dir "/"
                                 (with-default
                                  pid-file
                                  (string-append real-name ".pid"))))
                 (real-log-directory
                  (with-default
                   log-directory
                   (system-parameters-service-log-root parameters)))
                 (real-log-file
                  (string-append real-log-directory "/"
                                 (with-default
                                  log-file
                                  (string-append real-name ".log")))))
            (service-account
             (inherit account)
             (daemon-account? #t)
             (pid-directory real-pid-dir)
             (pid-directory-group real-pid-directory-group)
             (pid-file-path real-pid-file)
             (supplementary-groups real-supplementary-groups)
             (log-directory real-log-directory)
             (log-file-path real-log-file)
             (log-group (system-parameters-syslog-group parameters))))))

    (if (maybe-value-set? daemon)
        (resolve-daemon-account basic-account)
        basic-account)))


(define (service-account-service-accounts configuration)
  (match-records* ((configuration <service-account> id user group
                                  supplementary-groups create-account?
                                  is-reference? working-directory))
      (if (and create-account? (not is-reference?))
          (list (user-group
                 (name group)
                 (system? #t))
                (user-account
                 (name user)
                 (group group)
                 (system? #t)
                 (supplementary-groups supplementary-groups)
                 (home-directory working-directory)
                 (shell (file-append shadow "/sbin/nologin"))))
          '())))


(define (activation-action configuration)
  (match-records* ((configuration <service-account> id user group create-account?
                                  create-directories? working-directory root-owned-home?
                                  daemon-account?
                                  pid-file-path pid-directory pid-directory-group
                                  log-file-path log-directory log-group))

    #~(begin
        (format #t "Activating account ~a: ~a - ~a / ~a - ~a / ~a - ~a~%"
                '#$id #$root-owned-home? #$user #$group #$create-account?
                #$create-directories? #$working-directory)
        (when #$create-directories?
          (let* ((uid (passwd:uid (getpw #$user)))
                 (gid (group:gid (getgr #$group))))
            (if #$root-owned-home?
                (create-directory-with-owner #$working-directory 0 0 #o700)
                (create-directory-with-owner #$working-directory uid gid #o750))
            (when #$daemon-account?
              (format #t "Daemon account.~%")
              (let* ((log-gid (group:gid (getgr #$log-group)))
                     (pid-gid (group:gid (getgr #$pid-directory-group))))
                (format #t "Creating daemon directories: ~a - ~a / ~a - ~a - ~a - ~a / ~a~%"
                        #$pid-directory #$pid-directory-group pid-gid
                        #$log-directory #$log-file-path #$log-group log-gid)
                (create-directory-with-owner #$pid-directory uid pid-gid #o750)
                (when #$log-directory
                  (create-directory-with-owner #$log-directory 0 log-gid #o750))
                (create-file-with-owner #$log-file-path uid log-gid #o640))))))))


(define (service-account-service-activation configuration)
  (match-records* ((configuration <service-account> id is-reference?))
      (if is-reference?
          '()
          (activation-task
           (id (symbol-append 'account- id))
           (modules '((guix build utils)
                      (baulig build utils)))
           (action (activation-action configuration))))))


(define-public service-account-service-type
  (service-type (name 'service-account)
                (extensions
                 (list (service-extension account-service-type
                                          service-account-service-accounts)
                       (service-extension activation-task-service-type
                                          service-account-service-activation)))
                (description
                 "Service-Account service activation.")))


(define* (monadic-resolve-service-account config #:key parameters)
  (mlet %state-monad ((service-accounts (context-variable service-accounts default '())))
    (begin
      (cond ((account-configuration? config)
             (let* ((account (resolve-service-account config #:parameters parameters))
                    (id (service-account-id account)))
               (when (assq-ref service-accounts id)
                 (throw (make-service-account-error* "Duplicate service account: ~a~%" id)))
               (mbegin %state-monad
                 (set-context-variable! service-accounts (assq-set! service-accounts id account))
                 (return account))))
            ((account-reference? config)
             (let* ((id (account-reference-id config))
                    (account (assq-ref service-accounts id)))
               (unless account
                 (throw (make-service-account-error* "No such service account: ~a~%" id)))
               (return (service-account
                        (inherit account)
                        (is-reference? #t)))))
            (else (throw 'wrong-type-arg config))))))


(define-public (get-service-account-id value)
  (cond ((account-configuration? value)
         (account-configuration-id value))
        ((account-reference? value)
         (account-reference-id value))
        (else
         (throw 'wrong-type-arg
                (format #f "Invalid account; expected account-configuration or account-reference: ~S"
                        value)))))


(define-parameterized-service service-account
  (type-predicate
   (lambda (value)
     (or (account-configuration? value)
         (account-reference? value))))
  (resolve monadic monadic-resolve-service-account)
  (unique-id get-service-account-id)
  (compose list)
  (extend list))
