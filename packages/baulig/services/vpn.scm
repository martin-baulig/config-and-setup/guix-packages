;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services vpn)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services shepherd)
  #:use-module (gnu packages vpn)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (baulig build config-utils)
  #:use-module (baulig build secrets-utils)
  #:use-module (baulig services account)
  #:use-module (baulig services parameters)
  #:use-module (baulig services secrets)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization syntax-macros)
  #:use-module (baulig services serialization dash-space)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:export (openvpn-client-service)
  #:export-syntax (openvpn-client-list))


(define-public-configuration openvpn-remote-configuration
  (name
   string
   "Server name.")

  (port
   (integer 1194)
   "Port number the server listens to."))

(define-section-serializer space-separated openvpn-remote-configuration "remote")


(define (openvpn-remote-list? val)
  (and (list? val)
       (or (eq? val '())
           (and (openvpn-remote-configuration? (car val))
                (openvpn-remote-list? (cdr val))))))

(define (serialize-openvpn-remote-list field-name val)
  (string-join
   (map (lambda (remote)
          (format #f "remote ~a ~a~%" (openvpn-remote-configuration-name remote)
                  (number->string (openvpn-remote-configuration-port remote))))
        val)))

;;;
;;; Serialization Helpers
;;;

(define-public (serialize-tls-client-auth field-name value)
  (let ((field (config-field-name dash field-name)))
    (format #f "~a @blob:~a@ 1~%" field value)))

(define-public (tls-client-auth? value)
  (secrets-file-key? value))

(define-public-maybe tls-client-auth)


(define-public (serialize-tls-server-auth field-name value)
  (let ((field (config-field-name dash field-name)))
    (format #f "~a @blob:~a@ 0~%" field value)))

(define-public (tls-server-auth? value)
  (secrets-file-key? value))

(define-public-maybe tls-server-auth)


;;;
;;; OpenVPN Client with Secrets Service integration
;;;

(define-public-configuration openvpn-client-configuration
  (ca
   secrets-service-blob
   "Secrets Service BLOB containing the CA Certificate.")

  (cert
   secrets-service-blob
   "Secrets Service BLOB containing the X509 certificate.")

  (key
   secrets-service-blob
   "Secrets Service BLOB containing the X509 certificate key.")

  (comp-lzo?
   (true-or-nothing #f)
   "Whether to use the LZO compression algorithm.")

  (persist-key?
   (true-or-nothing #t)
   "Don’t re-read key files across SIGUSR1 or –ping-restart.")

  (persist-tun?
   (true-or-nothing #t)
   "Don’t close and reopen TUN/TAP device or run up/down scripts across SIGUSR1 or –ping-restart restarts.")

  (verbosity
   (integer 3)
   "Verbosity level."
   (serializer (lambda (_ value)
                 (serialize-integer 'verb value))))

  (tls-auth
   tls-client-auth
   "Secrets Service BLOB containing HMAC authentication.")

  (remote
   (openvpn-remote-list '())
   "A list of remote servers to connect to."))


;;;
;;; OpenVPN Server with Secrets Service integration
;;;

(define-public-configuration openvpn-server-address
  (subnet
   ipv4-address
   "Server subnet")

  (netmask
   ipv4-address
   "Server netmask"))


(define-public (serialize-openvpn-server-address _ value)
  (match-records* ((value <openvpn-server-address> subnet netmask))
      (format #f "server ~a ~a~%" subnet netmask)))


(define-public-configuration openvpn-client-entry
  (host
   host-name
   "Client host name.")

  (address
   ipv4-address
   "Client IP address."))


(define (format-openvpn-client-entry value)
  (match-records* ((value <openvpn-client-entry> host address))
      (format #f "~a,~a," host address)))


(define-public (serialize-openvpn-client-entry _ value)
  (format-openvpn-client-entry value))


(define-public (list-of-openvpn-client-entry? value)
  (and (list? value)
       (every openvpn-client-entry? value)))


(define (format-list-of-openvpn-client-entry value)
  (string-join
   (map format-openvpn-client-entry value)
   "\n" 'suffix))


(define-public (serialize-list-of-openvpn-client-entry _ value)
  (format #f "ifconfig-pool-persist @parameter:ipp-txt@ 0~%"))


(define-syntax openvpn-client-list
  (syntax-rules ()
    ((_ (host-val address-val) ...)
     (list (openvpn-client-entry (host host-val) (address address-val)) ...))))


(define-public-configuration openvpn-server-configuration
  (server
   openvpn-server-address
   "Server address.")

  (client-list
   list-of-openvpn-client-entry
   "List of ipp.txt client entries.")

  (ca
   secrets-service-blob
   "Secrets Service BLOB containing the CA Certificate.")

  (cert
   secrets-service-blob
   "Secrets Service BLOB containing the X509 certificate.")

  (key
   secrets-service-blob
   "Secrets Service BLOB containing the X509 certificate key.")

  (dh
   secrets-service-blob
   "Secrets Service BLOB containing the Diffie-Hellman parameters.")

  (persist-key?
   (true-or-nothing #t)
   "Don’t re-read key files across SIGUSR1 or –ping-restart.")

  (persist-tun?
   (true-or-nothing #t)
   "Don’t close and reopen TUN/TAP device or run up/down scripts across SIGUSR1 or –ping-restart restarts.")

  (verbosity
   (integer 3)
   "Verbosity level."
   (serializer (lambda (_ value)
                 (serialize-integer 'verb value))))

  (tls-auth
   tls-server-auth
   "Secrets Service BLOB containing HMAC authentication."))


;;;
;;; OpenVPN Service
;;;

(define-record-type* <openvpn-service>
  openvpn-service
  make-openvpn-service
  openvpn-service?

  (secrets              openvpn-service-secrets)
  (server?              openvpn-service-server?)
  (configuration        openvpn-service-configuration))


;;;
;;; Parameterized Service
;;;

(define %openvpn-account
  (make-daemon-account
   (account-configuration
    (id 'openvpn))))


(define* (resolve-openvpn-client-secrets configuration #:key parameters)
  (let* ((config-template (serialize-configuration
                           configuration
                           openvpn-client-configuration-fields))
         (fixed-section (string-join
                         `("client"
                           "proto tcp"
                           "dev tun"
                           "remote-cert-tls server"
                           "tls-version-min 1.3"
                           "auth SHA512"
                           "cipher AES-256-GCM"
                           "nobind"
                           "resolv-retry infinite")
                         "\n" 'suffix)))
    (create-secrets (daemon-account 'openvpn)
        (template "openvpn-client.conf"
                  #~(string-append #$fixed-section "\n" #$config-template))
      (options root-owned-secrets))))


(define* (resolve-openvpn-server-secrets configuration #:key parameters)
  (let* ((config-template (serialize-configuration
                           configuration
                           openvpn-server-configuration-fields))
         (ipp-template (format-list-of-openvpn-client-entry
                        (openvpn-server-configuration-client-list configuration)))
         (fixed-section (string-join
                         `("topology subnet"
                           ;; Most guides recommend UDP, but I ran some speed test and am
                           ;; getting significantly better performance with TCP.
                           "proto tcp"
                           "dev tun"
                           "client-to-client"
                           "remote-cert-eku \"TLS Web Client Authentication\""
                           "tls-version-min 1.3"
                           "auth SHA256"
                           "cipher AES-256-GCM"
                           "resolv-retry infinite"
                           "user openvpn"
                           "group openvpn")
                         "\n" 'suffix)))
    (create-secrets (daemon-account 'openvpn)
        (template "openvpn-server.conf"
                  #~(string-append #$fixed-section "\n" #$config-template))
      (options root-owned-secrets)
      (extra-parameters gexp #~((ipp-txt . #$(mixed-text-file "ipp.txt" ipp-template)))))))


(define-parameterized-service openvpn-client
  (record-type <openvpn-client-configuration>)
  (parent secrets resolve-openvpn-client-secrets)
  (resolve
   (lambda* (configuration #:key secrets parameters)
     (openvpn-service
      (configuration configuration)
      (server? #f)
      (secrets secrets)))))


(define-parameterized-service openvpn-server
  (record-type <openvpn-server-configuration>)
  (parent secrets resolve-openvpn-server-secrets)
  (resolve
   (lambda* (configuration #:key secrets parameters)
     (openvpn-service
      (configuration configuration)
      (server? #t)
      (secrets secrets)))))



(define (resolve-openvpn-shepherd service)
  (match-records* ((service <openvpn-service> secrets server? configuration)
                   (secrets <secrets-service> account output-file requirements)
                   (account <service-account> working-directory log-file-path pid-file-path))
    (list
     (shepherd-service
      (provision '(openvpn))
      (documentation "OpenVPN Service")
      (requirement `(networking ,@requirements))
      (respawn? #f)
      (start #~(make-forkexec-constructor
                (list #$(file-append openvpn "/sbin/openvpn")
                      "--writepid" #$pid-file-path
                      "--config" #$output-file
                      "--daemon")
                #:directory #$working-directory
                #:log-file #$log-file-path
                #:pid-file #$pid-file-path))
      (stop #~(make-kill-destructor))))))


(define-public openvpn-client-service-type
  (service-type (name 'openvpn-client)
                (extensions
                 (list (service-extension shepherd-root-service-type
                                          resolve-openvpn-shepherd)))
                (description "OpenVPN Client with Secrets Service integration.")))


(define-public openvpn-server-service-type
  (service-type (name 'openvpn-server)
                (extensions
                 (list (service-extension shepherd-root-service-type
                                          resolve-openvpn-shepherd)))
                (description "OpenVPN Server with Secrets Service integration.")))
