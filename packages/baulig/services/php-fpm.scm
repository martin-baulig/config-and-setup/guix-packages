;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services php-fpm)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (guix build utils)
  #:use-module (gnu packages php)
  #:use-module (gnu packages web)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (gnu services shepherd)
  #:use-module (baulig build config-utils)
  #:use-module (baulig services account)
  #:use-module (baulig services activation)
  #:use-module (baulig services secrets)
  #:use-module (baulig services parameters)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization syntax-macros)
  #:use-module (baulig services serialization underscore-space-semicolon)
  #:use-module (baulig services serialization nginx)
  #:use-module (rnrs enums)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (web uri)
  #:export (<php-fpm-service>
            php-fpm-service))


;;;
;;; PHP-FPM Support
;;;

(define-public-configuration php-fpm-configuration
  (id
   maybe-account-id
   "Optional.  Unique ID of this PHP-FPM instance."
   (serializer empty-serializer))

  (fastcgi-index
   (file-name "index.php")
   "FastCGI index file.")

  (extra-directives
   (list-of-nginx-directives '())
   "Extra configuration directives."))

(define-section-serializer key-value-fragment php-fpm-configuration)

(define-public-maybe php-fpm-configuration)


(define-record-type* <php-fpm-service>
  php-fpm-service
  make-php-fpm-service
  php-fpm-service?

  (id                   php-fpm-service-id)
  (account              php-fpm-service-account)
  (secrets              php-fpm-service-secrets)
  (configuration        php-fpm-service-configuration))


;;;
;;; Internal APIs
;;;

(define (resolve-php-fpm-shepherd service)
  (match-records* ((service <php-fpm-service> id secrets)
                   (secrets <secrets-service> account output-file requirements)
                   (account <service-account> working-directory user group
                            log-file-path pid-file-path))
      (list
       (shepherd-service
        (provision `(,id))
        (documentation "Nginx FastCGI Service.")
        (requirement `(networking ,@requirements))
        (respawn? #f)
        (start #~(make-forkexec-constructor
                  (list #$(file-append php "/sbin/php-fpm")
                        "--fpm-config" #$output-file)
                  #:directory #$working-directory
                  #:user #$user
                  #:group #$group
                  #:log-file #$log-file-path
                  #:pid-file #$pid-file-path))
        (stop #~(make-kill-destructor))))))


(define-public php-fpm-service-type
  (service-type (name 'php-fpm)
                (extensions
                 (list (service-extension shepherd-root-service-type
                                          resolve-php-fpm-shepherd)))
                (description "Nginx FastCGI Service.")))


(define-public (resolve-php-fpm-id configuration)
  (match-records* ((configuration <php-fpm-configuration> id))
      (if (maybe-value-set? id)
          (symbol-append 'php-fpm- id)
          'php-fpm)))


(define-public (resolve-php-fpm-group configuration)
  (symbol->string (resolve-php-fpm-id configuration)))


(define* (resolve-php-fpm-secrets configuration #:key parameters)
  (define template-file
    (string-join
     ;; We don't set any user / group because the daemon is started by
     ;; Shepherd as the correct user / group.
     `("[global]"
       "pid = @parameter:pid-file@"
       "error_log = syslog"
       "[www]"
       "listen = @parameter:pid-directory@/php-fpm.sock"
       "pm = dynamic"
       "pm.max_children = 5"
       "pm.start_servers = 2"
       "pm.min_spare_servers = 1"
       "pm.max_spare_servers = 3"
       "php_flag[display_errors] = on"
       )
     "\n" 'suffix))

  (let* ((real-id (resolve-php-fpm-id configuration))
         (account (make-daemon-account (account-configuration (id real-id)))))
    (create-secrets
        (daemon-account-expression account)
        (template "php-fpm.conf" template-file))))


(define-parameterized-service php-fpm
  (record-type <php-fpm-configuration>)
  (parent secrets resolve-php-fpm-secrets)
  (compose list)
  (extend list)
  (resolve
   (lambda* (configuration #:key secrets parameters)
     (match-records* ((secrets <secrets-service> account)
                      (account <service-account> id)
                      (configuration <php-fpm-configuration>))
         (php-fpm-service
          (id id)
          (configuration configuration)
          (account account)
          (secrets secrets))))))
