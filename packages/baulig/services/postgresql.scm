;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services postgresql)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (guix build utils)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages databases)
  #:use-module (baulig build config-utils)
  #:use-module (baulig services account)
  #:use-module (baulig services activation)
  #:use-module (baulig services parameters)
  #:use-module (baulig services secrets)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig services serialization syntax-macros)
  #:use-module (baulig services serialization underscore-single-quotes)
  #:use-module (baulig services serialization postgresql)
  #:use-module (srfi srfi-1)
  #:export (postgresql-service))


;;;
;;; Configuration Serialization
;;;

(define-syntax define-parameter-constant
  (syntax-rules (pid-directory pid-file)
    ((_ name pid-file)
     (define-string-constant underscore-equals name "'@parameter:pid-file@'"))
    ((_ name pid-directory)
     (define-string-constant underscore-equals name "'@parameter:pid-directory@'"))
    ((_ name value)
     (define-string-constant underscore-equals name (format #f "'@parameter:~a@'" value)))))

(define-parameter-constant unix-socket-directories pid-directory)
(define-parameter-constant external-pid-file pid-file)
(define-parameter-constant hba-file-path "hba-file")
(define-parameter-constant ident-file-path "ident-file")


(define (list-of-ipv4-address? value)
  (or (not (maybe-value-set? value))
      (and (list? value)
           (every ipv4-address? value))))

(define (serialize-list-of-ipv4-address field-name value)
  (if (and (maybe-value-set? value) (not (null? value)))
      (serialize-string field-name (string-join value ", "))
      ""))

;;;
;;; PostgreSQL TLS Configuration
;;;

(define-public-configuration postgresql-tls-configuration
  (ssl
   (true-constant #t)
   "Enable TLS.")

  (ssl-ca-file
   maybe-secrets-service-blob
   "Secrets Service Blob containing the PEM encoded TLS CA certificate.")

  (ssl-cert-file
   secrets-service-blob
   "Secrets Service Blob containing the PEM encoded TLS certificate.")

  (ssl-key-file
   secrets-service-blob
   "Secrets Service Blob containing the PEM encoded TLS certificate key.")

  (ssl-dh-params-file
   secrets-service-blob
   "Secrets Service Blob containing Diffie-Hellman parameters.")

  (ssl-ciphers
   (string "HIGH:!3DES:!aNULL")
   "Specify which ciphers to use."))

(define-public-maybe postgresql-tls-configuration)

(define-section-serializer key-value-fragment postgresql-tls-configuration)


;;;
;;; PostgreSQL Configuration
;;;

(define-public-configuration postgresql-configuration
  (unix-socket-directories
   (unix-socket-directories-constant %unix-socket-directories)
   "PID file.")

  (listen-addresses
   list-of-ipv4-address
   "IP Address to bind to.")

  (external-pid-file
   (external-pid-file-constant %external-pid-file)
   "PID file path.")

  (hba-file
   (hba-file-path-constant %hba-file-path)
   "Full path of pg_hba.conf file.")

  (ident-file
   (ident-file-path-constant %ident-file-path)
   "Full path of pg_ident.conf file.")

  (tls
   maybe-postgresql-tls-configuration
   "Optional TLS Configuration Section.")

  (unix-socket-group
   maybe-user-name
   "Sets the owning group of the Unix-domain socket(s).")

  (unix-socket-permissions
   (integer #o770)
   "Sets the access permissions of the Unix-domain socket(s).")

  (log-destination
   maybe-postgresql-log-destination
   "Where to send log files to.")

  (log-min-messages
   maybe-postgresql-log-level
   "Controls which message levels are written to the server log.")

  (log-min-error-statement
   maybe-postgresql-log-level
   "Controls which SQL statements that cause an error condition are recorded in the server log.")

  (wal-level
   maybe-postgresql-wal-level
   "Determines how much information is written to the WAL.")

  (wal-compression
   maybe-boolean
   "This parameter enables compression of WAL using the specified compression method.")

  (wal-recycle
   maybe-boolean
   "It set to on (the default), this option causes WAL files to be recycled by renaming them.")

  (archive-mode
   maybe-postgresql-archive-mode
   "When enabled, completed WAL segments are set to archive storage.")

  (archive-command
   maybe-postgresql-archive-command
   "The local shell command to execute to archive a completed WAL file segment."))

;;;
;;; PostgreSQL Service
;;;

(define-record-type* <postgresql-service>
  postgresql-service
  make-postgresql-service
  postgresql-service?

  (account              postgresql-service-account)
  (postgresql           postgresql-service-postgresql
                        (default postgresql-14))
  (data-directory       postgresql-service-data-directory)
  (port                 postgresql-service-port
                        (default 5432))
  (initialize-database? postgresql-service-initialize-database?
                        (default #t))
  (hba-file             postgresql-service-hba-file)
  (ident-file           postgresql-service-ident-file)
  (configuration        postgresql-service-configuration))


(define-record-type* <postgresql-resolved-service>
  postgresql-resolved-service
  make-postgresql-resolved-service
  postgresql-resolved-service?

  (secrets              postgresql-resolved-service-secrets)
  (template             postgresql-resolved-service-template))


;;;
;;; PostgreSQL Service
;;;

(define* (resolve-postgresql-secrets service #:key parameters)
  (match-records* ((service <postgresql-service> account postgresql configuration
                            hba-file ident-file)
                   (account <account-configuration> id))
    (create-secrets
        (daemon-account-expression account)
        (serialize "postgresql.conf" postgresql configuration)
      (extra-parameters gexp #~((hba-file . #$hba-file)
                                (ident-file . #$ident-file)
                                (coreutils . #$coreutils))))))


(define (resolve-postgresql-shepherd service)
  (match-records* ((service <postgresql-resolved-service> template secrets)
                   (template <postgresql-service> initialize-database? postgresql
                             data-directory port)
                   (secrets <secrets-service> account output-file requirements)
                   (account <service-account> user group supplementary-groups
                            working-directory log-file-path pid-file-path))
      (list
       (shepherd-service
        (provision '(postgres))
        (documentation "PostgreSQL Service")
        (requirement `(networking ,@requirements
                       ,@(if initialize-database?
                             '(activation-task-postgresql-activation)
                             '()
                             )))
        (respawn? #f)
        (start #~(make-forkexec-constructor
                  (list #$(file-append postgresql "/bin/postgres")
                        "-D" #$data-directory "-p" #$(format #f "~a" port)
                        (string-append "--config-file=" #$output-file))
                  #:user #$user
                  #:group #$group
                  #:supplementary-groups (list #$@supplementary-groups)
                  #:directory #$working-directory
                  #:log-file #$log-file-path))
        (stop #~(make-kill-destructor))))))


(define (resolve-postgresql-activation service)
  (match-records* ((service <postgresql-resolved-service> template secrets)
                   (template <postgresql-service> initialize-database?
                             postgresql data-directory)
                   (secrets <secrets-service> account requirements)
                   (account <service-account> user group))
      (define activation-action
        #~(let* ((uid (passwd:uid (getpw #$user)))
                 (gid (group:gid (getgr #$group)))
                 (initdb #$(file-append postgresql "/bin/initdb")))
            (format #t "PostgreSQL activation: ~a~%" #$data-directory)
            (if (directory-exists? #$data-directory)
                (format #t "PostgreSQL data directory already exists, nothing to do.")
                (begin
                  (format #t "Need to create PostgreSQL data diretory: ~a" #$data-directory)
                  (create-directory-with-owner #$data-directory uid gid #o700)
                  (format #t "Initializing PostgreSQL database.")
                  (invoke-as-user #$user initdb #$data-directory)
                  (format #t "Done initializing PostgreSQL.")))))

    (if initialize-database?
        (activation-task
         (id 'postgresql-activation)
         (requirements requirements)
         (action activation-action))
        '())))


(define* (resolve-postgresql service-template #:key secrets parameters)
  (match-records* ((service-template <postgresql-service> configuration))
      (postgresql-resolved-service
       (template service-template)
       (secrets secrets))))


(define-public postgresql-service-type
  (service-type (name 'postgresql)
                (extensions
                 (list (service-extension shepherd-root-service-type
                                          resolve-postgresql-shepherd)
                       (service-extension activation-task-service-type
                                          resolve-postgresql-activation)))
                (description "PostgreSQL Service")))


(define-parameterized-service postgresql
  (parent secrets resolve-postgresql-secrets)
  (resolve resolve-postgresql))
