;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services gitlab-runner)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services shepherd)
  #:use-module (gnu system shadow)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (baulig packages gitlab-runner)
  #:use-module (baulig packages ksh93)
  #:use-module (ice-9 match)
  #:export (gitlab-runner-configuration
            gitlab-runner-configuration?
            gitlab-runner-service-type))

;;; Commentary:
;;;
;;; GitLab Runner service.
;;;
;;; Code:

(define %gitlab-runner-user "runner")
(define %gitlab-runner-group "runner")
(define %gitlab-runner-working-directory "/var/run/gitlab-runnerd")
(define %gitlab-runner-log-file "/var/log/gitlab-runnerd.log")

;;;
;;; <gitlab-runner-global> record
;;;

(define-record-type* <gitlab-runner-global>
  gitlab-runner-global
  make-gitlab-runner-global
  gitlab-runner-global?

  (concurrent gitlab-runner-global-concurrent
              (default 0))
  (log-level gitlab-runner-global-log-level
             (default "info"))
  (log-format gitlab-runner-global-log-format
              (default "runner"))
  (check-interval gitlab-runner-global-check-interval
                  (default 0)))


(define-public (gitlab-runner-global->string global)
  (match-record global <gitlab-runner-global>
    (concurrent log-level log-format check-interval)
    (let* ((fields `((concurrent . ,concurrent)
                     (log_level . ,log-level)
                     (log_format . ,log-format)
                     (check_interval . ,check-interval))))
      (string-join (map (match-lambda
                          ((name  . value)
                           (format #f "~a = ~s~%" name value))) fields) ""))))


;;;
;;; <gitlab-runner-runners-custom> record
;;;

(define-record-type* <gitlab-runner-custom>
  gitlab-runner-custom
  make-gitlab-runner-custom
  gitlab-runner?-custom

  (run-exec gitlab-runner-config-run-exec
            (default "echo HELLO"))

  (config-exec-timeout gitlab-runner-config-exec-timeout
                       (default 3600))
  (prepare-exec-timeout gitlab-runner-prepare-exec-timeout
                        (default 3600))
  (cleanup-exec-timeout gitlab-runner-cleanup-exec-timeout
                        (default 3600))
  (graceful-kill-timeout gitlab-runner-config-graceful-kill-timeout
                         (default 600))
  (force-kill-timeout gitlab-runner-config-force-kill-timeout
                      (default 600)))


(define-public (gitlab-runner-custom->string custom)
  (match-record custom <gitlab-runner-custom>
    (run-exec config-exec-timeout prepare-exec-timeout cleanup-exec-timeout
              graceful-kill-timeout force-kill-timeout)
    (let* ((fields `((run_exec . ,run-exec)
                     (config_exec_timeout . ,config-exec-timeout)
                     (prepare_exec_timeout . ,prepare-exec-timeout)
                     (cleanup_exec_timeout . ,cleanup-exec-timeout)
                     (graceful_kill_timeout . ,graceful-kill-timeout)
                     (force_kill_timeout . ,force-kill-timeout))))
      (string-append "\n  [runners.custom]\n"
                     (string-join
                      (map (match-lambda
                             ((name  . value)
                              (format #f "    ~a = ~s~%" name value))) fields) "")))))


;;;
;;; <gitlab-runner-runners> record
;;;

(define-record-type* <gitlab-runner-runners>
  gitlab-runner-runners
  make-gitlab-runner-runners
  gitlab-runner-runners?

  (name gitlab-runner-runners-name
        (default "Guix Runner"))
  (url gitlab-runner-runners-url
       (default "https://gitlab.com/"))
  (executor gitlab-runner-runners-log-format
            (default "custom"))
  (custom gitlab-runner-runners-custom
          (default (gitlab-runner-custom))))


(define-public (gitlab-runner-runners->string runners)
  (match-record runners <gitlab-runner-runners>
    (name url executor custom)
    (let* ((fields `((name . ,name)
                     (url . ,url)
                     (executor . ,executor))))
      (string-append "\n[[runners]]\n"
                     (string-join
                      (map (match-lambda
                             ((name  . value)
                              (format #f "  ~a = ~s~%" name value))) fields) "")
                     (if custom
                         (gitlab-runner-custom->string custom)
                         "")))))

;;;
;;; <gitlab-runner-configuration> record
;;;

(define-record-type* <gitlab-runner-configuration>
  gitlab-runner-configuration
  make-gitlab-runner-configuration
  gitlab-runner-configuration?
  (user gitlab-runner-configuration-user
        (default %gitlab-runner-user))
  (group gitlab-runner-configuration-group
         (default %gitlab-runner-group))
  (working-directory gitlab-runner-configuration-working-directory
                     (default %gitlab-runner-working-directory))
  (log-file gitlab-runner-configuration-log-file
            (default %gitlab-runner-log-file))
  (description gitlab-runner-configuration-description
               (default "Guix GitLab Runner"))
  (global gitlab-runner-configuration-global
          (default (gitlab-runner-global)))
  (runners gitlab-runner-configuration-runners
           (default (gitlab-runner-runners))))



(define* (make-runner-constructor config command need-token? #:rest args)
  (match-record config <gitlab-runner-configuration>
    (user group working-directory log-file description global)

    #~(begin
        (use-modules (ice-9 textual-ports))

        (let* ((token-env-var
                (and #$need-token?
                     (let* ((port (open-input-file "/home/private/gitlab-runner/token"))
                            (token (read-line port)))
                       (string-append "CI_SERVER_TOKEN=" token))))
               (env-vars
                (if #$need-token?
                    (list token-env-var)
                    '())))

          (make-forkexec-constructor
           (list #$(file-append gitlab-runner "/bin/gitlab-runner")
                 #$command
                 "--config" (string-append #$working-directory "/config.toml")
                 #$@args)
           #:environment-variables env-vars
           #:user #$user
           #:group #$group
           #:directory #$working-directory
           #:log-file #$log-file)))))


(define (gitlab-runner-register-action config config-file)
  (match-record config <gitlab-runner-configuration>
    (user group working-directory log-file description global)
    (shepherd-action
     (name 'register)
     (documentation "Register GitLab Runner.")
     (procedure (make-runner-constructor config "register" #t
                                         "--non-interactive"
                                         "--url" "https://gitlab.com/"
                                         "--description" description
                                         "--template-config" config-file)))))


(define (gitlab-runner-shepherd-service config)
  (match-record config <gitlab-runner-configuration>
    (user group log-file working-directory global runners)
    (define config-file
      (plain-file "config.toml"
                  (string-append
                   (gitlab-runner-global->string global)
                   (gitlab-runner-runners->string runners))))

    (list (shepherd-service
           (provision '(gitlab-runnerd))
           (documentation "Run the Gitlab-Runner daemon.")
           (requirement '(user-processes networking))
           (auto-start? #f)
           (actions (list (gitlab-runner-register-action config config-file)))
           (start (make-runner-constructor config "run" #f))
           (stop #~(make-kill-destructor))))))


(define (gitlab-runner-account config)
  (list (user-account
         (name (gitlab-runner-configuration-user config))
         (group (gitlab-runner-configuration-group config))
         (password "*")
         (comment "GitLab Runner")
         (home-directory (string-append "/home/" (gitlab-runner-configuration-user config)))
         (shell (file-append ksh93-baulig "/bin/ksh"))
         (supplementary-groups '("docker")))))


(define (gitlab-runner-service-activation config)
  "Return the activation gexp for CONFIG."
  (with-imported-modules '((guix build utils))
    #~(begin
        (use-modules (guix build utils))
        (let ((user (getpw #$(gitlab-runner-configuration-user config)))
              (group (getpw #$(gitlab-runner-configuration-group config)))
              (directory #$(gitlab-runner-configuration-working-directory config))
              (log-file #$(gitlab-runner-configuration-log-file config)))
          (mkdir-p directory)
          (chown directory (passwd:uid user) (passwd:gid user))
          (unless (file-exists? log-file)
            (close (open log-file (logior O_WRONLY O_CREAT) #o640))
            (chown log-file (passwd:uid user) (passwd:gid user)))))))

;;;
;;; GitLab Runner Service
;;;

(define gitlab-runner-service-type
  (service-type (name 'gitlab-runner)
                (extensions
                 (list (service-extension shepherd-root-service-type
                                          gitlab-runner-shepherd-service)
                       (service-extension account-service-type
                                          gitlab-runner-account)
                       (service-extension activation-service-type
                                          gitlab-runner-service-activation)))
                (description
                 "Run the the @command{gitlab-runnerd} daemon. ")
                (default-value (gitlab-runner-configuration))))

;;; gitlab-runner.scm ends here
