;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig services parameters)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services shepherd)
  #:use-module (gnu packages admin)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (guix modules)
  #:use-module (guix monads)
  #:use-module (guix store)
  #:use-module (baulig services config-serialization)
  #:use-module (baulig utils syntax-macros)
  #:use-module (ice-9 exceptions)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-26)
  #:export (<system-parameters>
            system-parameters

            system-parameters-secrets-database
            system-parameters-service-root
            system-parameters-service-log-root
            system-parameters-service-run-root
            system-parameters-syslog-group

            context-variable
            set-context-variable!

            define-parameterized-service

            parameterized-services
            parameterized-service)
  #:re-export (match-records*
               match-records*-lambda))


;;;
;;; System Parameters
;;;


(define-record-type* <system-parameters>
  system-parameters
  make-system-parameters
  system-parameters?

  (secrets-database     system-parameters-secrets-database      (default "/etc/guix/secrets.db"))

  (service-root         system-parameters-service-root          (default "/home/services"))
  (service-log-root     system-parameters-service-log-root      (default "/var/log/services"))
  (service-run-root     system-parameters-service-run-root      (default "/var/run/services"))
  (syslog-group         system-parameters-syslog-group          (default "syslog")))


;;;
;;; Monadic Context
;;;

(define-record-type* <monadic-context>
  monadic-context
  make-monadic-context
  monadic-context?

  (parameters           monadic-context-parameters)
  (services             monadic-context-services                (default '()))
  (variables            monadic-context-variables               (default '())))


(define (add-service! context service)
  (match-record context <monadic-context>
    (services)
    (monadic-context
     (inherit context)
     (services (cons service services)))))


(define-syntax context-variable
  (lambda (x)
    (syntax-case x (default)
      ((_ key default value)
       (identifier? #'key)
       #'(mlet %state-monad ((context (current-state)))
          (return (or (assq-ref (monadic-context-variables context) 'key) value))))
      ((_ key)
       (identifier? #'key)
       #'(mlet %state-monad ((context (current-state)))
          (return (assq-ref (monadic-context-variables context) 'key)))))))


(define-syntax set-context-variable!
  (lambda (x)
    (syntax-case x ()
      ((_ key value)
       (identifier? #'key)
       #'(mlet %state-monad ((context (current-state)))
          (match-record context <monadic-context>
            (variables)
            (mbegin %state-monad
              (set-current-state
               (monadic-context
                (inherit context)
                (variables (assq-set! variables 'key value))))
              (return value))))))))


;;;
;;; Parameterized Service
;;;

(define-record-type* <parameterized-service>
  parameterized-service
  make-parameterized-service
  parameterized-service?

  (type                     parameterized-service-type)
  (resolve                  parameterized-service-resolve)
  (unique-id                parameterized-service-unique-id)
  (constructor              parameterized-service-constructor)
  (type-predicate           parameterized-service-type-predicate)
  (extend-type-predicate    parameterized-service-extend-type-predicate)
  (compose                  parameterized-service-compose)
  (extend                   parameterized-service-extend)
  (parents                  parameterized-service-parents           (default '())))


(define-record-type* <service-parent>
  service-parent
  make-service-parent
  service-parent?

  ;; Symbol.
  (name                     service-parent-name)
  ;; Symbol.
  (type                     service-parent-type)
  ;; Instance of <parameterized-service>.
  (parameterized-service    service-parent-parameterized-service)
  ;; Instance of <service>.
  (base-service             service-parent-base-service)
  ;; Function mapping the current service value to the parent's.
  (make-config              service-parent-make-config)
  ;; When #t, then 'make-config' returns a list of instance values.
  (apply-as-list?           service-parent-apply-as-list?))


(define-record-type* <service-instance>
  service-instance
  make-service-instance
  service-instance?

  ;; Symbol.
  (name                     service-instance-name)
  ;; Symbol.
  (type                     service-instance-type)
  ;; Instance of <parameterized-service>.
  (parameterized-service    service-instance-parameterized-service)
  ;; Instance of <service>.
  (base-service             service-instance-base-service)
  ;; Identifier index.
  (index                    service-instance-index)
  ;; Service value.
  (value                    service-instance-value)
  ;; Optional symbol; set via (unique-id) on parameterized-service.
  (unique-id                service-instance-unique-id              (default #f))
  ;; List of integers
  (parents                  service-instance-parents                (default '()))
  ;; We are a 'parent-list' instance.
  (apply-as-list?           service-instance-apply-as-list?         (default #f)))


;;;
;;; Exception
;;;

(define-exception-type parameterized-service-error &programming-error
  make-parameterized-service-error
  parameterized-service-error?

  (message                  parameterized-service-error-message))


(define* (make-parameterized-service-error* format-string #:rest args)
  (make-parameterized-service-error (apply format `(#f ,format-string ,@args))))


;;;
;;; Internal API
;;;

(define-syntax lookup-service-type
  (lambda (x)
    (syntax-case x (base parameterized)
      ((_ parameterized type)
       (identifier? #'type)
       (datum->syntax x
                      (symbol-append 'parameterized- (syntax->datum #'type) '-service)))
      ((_ base type)
       (identifier? #'type)
       (datum->syntax x
                      (symbol-append (syntax->datum #'type) '-service-type))))))

(define-syntax set-arg
  (lambda (x)
    (syntax-case x (unique add)
      ((_ args unique key value)
       (identifier? #'key)
       #'(begin
           (when (assq-ref args 'key)
             (raise-exception
              (make-parameterized-service-error* "Duplicate '~a' clause." 'key)))
           (set! args (assq-set! args 'key value))))
      ((_ args add key value)
       (identifier? #'key)
       #'(let ((current (or (assq-ref args 'key) '())))
           (set! args (assq-set! args 'key (cons value current))))))))

(define-syntax get-arg
  (lambda (x)
    (syntax-case x (required list)
      ((_ args required key)
       (identifier? #'key)
       #'(or (assq-ref args 'key)
             (raise-exception
              (make-parameterized-service-error* "Missing '~a' clause." 'key))))
      ((_ args list key)
       (identifier? #'key)
       #'(or (assq-ref args 'key) '())))))


(define-syntax parameterized-service-clause
  (lambda (x)
    (syntax-case x (constructor compose extend list merge monadic
                                named-parent parent parent-list resolve singleton
                                extend-type-predicate record-type type-predicate
                                unique-id)
      ((_ args (constructor func))
       #'(set-arg args unique constructor func))
      ((_ args (compose list))
       #'(set-arg args unique compose 'list))
      ((_ args (compose singleton))
       #'(set-arg args unique compose 'singleton))
      ((_ args (extend list))
       #'(set-arg args unique extend 'list))
      ((_ args (extend merge func))
       #'(set-arg args unique extend func))
      ((_ args (resolve monadic func))
       #'(set-arg args unique resolve (cons #t func)))
      ((_ args (resolve func))
       #'(set-arg args unique resolve (cons #f func)))
      ((_ args (unique-id func))
       #'(set-arg args unique unique-id func))
      ((_ args (extend-type-predicate func))
       #'(set-arg args unique extend-type-predicate func))
      ((_ args (type-predicate func))
       #'(set-arg args unique type-predicate func))
      ((_ args (record-type type))
       (identifier? #'type)
       #'(set-arg args unique type-predicate
          (lambda (value) (eq? (struct-vtable value) type))))
      ((_ args (name-parent name type func))
       (and (identifier? #'name) (identifier? #'type))
       #'(set-arg args add parents
          (make-service-parent
           'name 'type
           (lookup-service-type parameterized type)
           (lookup-service-type base type)
           func #f)))
      ((_ args (parent type func))
       (identifier? #'type)
       #'(set-arg args add parents
          (make-service-parent
           'type 'type
           (lookup-service-type parameterized type)
           (lookup-service-type base type)
           func #f)))
      ((_ args (parent-list type func))
       (identifier? #'type)
       #'(set-arg args add parents
          (make-service-parent
           'type 'type
           (lookup-service-type parameterized type)
           (lookup-service-type base type)
           func #t))))))


(define-syntax define-parameterized-service
  (lambda (x)
    (define (make-service-type-name name)
      (datum->syntax name
                     (symbol-append 'parameterized- (syntax->datum name) '-service)))

    (syntax-case x ()
      ((_ this-identifier clause ...)
       (identifier? #'this-identifier)
       (with-syntax ((service-type (make-service-type-name #'this-identifier))
                     (args (datum->syntax x 'args)))
         #'(define-public service-type
             (begin
               (let ((args '()))
                 (parameterized-service-clause args clause) ...
                 (parameterized-service
                  (type 'this-identifier)
                  (constructor (assq-ref args 'constructor))
                  (compose (assq-ref args 'compose))
                  (extend (assq-ref args 'extend))
                  (resolve (get-arg args required resolve))
                  (unique-id (assq-ref args 'unique-id))
                  (extend-type-predicate (assq-ref args 'extend-type-predicate))
                  (type-predicate (assq-ref args 'type-predicate))
                  (parents (get-arg args list parents)))))))))))


;;;
;;; Macros
;;;

(define-record-type* <fold-service-elem>
  fold-service-elem
  make-fold-service-elem
  fold-service-elem?

  (kind                     fold-service-elem-kind)
  (service                  fold-service-elem-service)
  (base-service             fold-service-elem-base-service)
  (value                    fold-service-elem-value))


(define-record-type* <fold-service-iter>
  fold-service-iter
  make-fold-service-iter
  fold-service-iter?

  (last-id                  fold-service-iter-last-id               (default 0))
  (by-type                  fold-service-iter-by-type               (default '()))
  (by-index                 fold-service-iter-by-index              (default '())))


(define* (compose-type-list service instance by-type #:key (continuation raise-exception))
  (match-record service <parameterized-service>
    (type compose)

    (let* ((current-type-list (assq-ref by-type type))
           (type-list
            (cond ((not current-type-list)
                   (list instance))
                  ((or (not compose) (eq? compose 'singleton))
                   (continuation
                    (make-parameterized-service-error*
                     "Parameterized service '~a' is not composable." type)))
                  ((eq? compose 'list)
                   `(,instance ,@current-type-list)))))
      (assq-set! by-type type type-list))))


(define* (get-unique-id service value by-type #:key (continuation raise-exception))
  (match-record service <parameterized-service>
    (type unique-id)
    (if (not unique-id)
        #f
        (let ((id (unique-id value)))
          (unless (symbol? id)
            (continuation
             (make-parameterized-service-error*
              "Unique ID function of service ~a did dot return a symbol: ~a" type id)))
          (when (memq id by-type)
            (continuation
             (make-parameterized-service-error*
              "Duplicate instance '~a' of service '~a'." id type)))
          unique-id))))


(define* (lookup-instance-by-index iter index #:key (continuation raise-exception))
  (match-record iter <fold-service-iter>
    (by-index)
    (or (assq-ref by-index index)
        (continuation
         (make-parameterized-service-error*
          "No service instance with index '~a'." index)))))


(define-syntax %create-instance
  (syntax-rules (with-func)
    ((_ iter continuation name type service base-service instance-value (with-func _) clause ...)
     (%create-instance iter continuation name type service base-service instance-value clause ...))
    ((_ iter continuation instance-name type service base-service instance-value clause ...)
     (match-record iter <fold-service-iter>
       (last-id by-type)
       (let ((unique-id (get-unique-id service instance-value by-type #:continuation continuation)))
         (service-instance
          (index last-id)
          (name instance-name)
          (type type)
          (parameterized-service service)
          (base-service base-service)
          (value instance-value)
          clause ...))))))

(define-syntax %apply-func
  (syntax-rules (with-func)
    ((_ instance new-iter inner-iter (with-func func) clause ...)
     (func instance new-iter inner-iter))
    ((_ instance new-iter inner-iter clause ...)
     (values new-iter inner-iter))))


(define-syntax add-instance-list-iter
  (syntax-rules ()
    ((_ (main-iter inner-iter) continuation
        name type service base-service instance-value clause ...)
     (match-record main-iter <fold-service-iter>
       (last-id by-type by-index)
       (let* ((instance (%create-instance main-iter continuation name type
                                          service base-service instance-value clause ...))
              (by-type (compose-type-list service last-id by-type #:continuation continuation))
              (by-index (assq-set! by-index last-id instance))
              (new-iter
               (fold-service-iter
                (last-id (+ 1 last-id))
                (by-type by-type)
                (by-index by-index))))
         (%apply-func instance new-iter inner-iter clause ...))))))


(define-syntax modify-instance-list-iter
  (syntax-rules (add add-list
                     fold-list fold-list* fold-instances fold-instances*
                     modify-singleton modify-instance* with-func)
    ((_ (main-iter inner-iter) continuation
        (fold-list func list args ...))
     (values (fold (cut func <> <> args ...) main-iter list) inner-iter))
    ((_ (main-iter inner-iter) continuation
        (fold-list* func list args ...))
     (fold-values func (main-iter inner-iter) list args ...))
    ((_ (main-iter inner-iter) continuation
        (fold-instances func args ...))
     (match-record main-iter <fold-service-iter>
       (by-index)
       (let* ((instances (map cdr by-index)))
         (values (fold (cut func <> <> args ...) main-iter instances) inner-iter))))
    ((_ (main-iter inner-iter) continuation
        (fold-instances* func args ...))
     (match-record main-iter <fold-service-iter>
       (by-index)
       (let* ((instances (map cdr by-index)))
         (fold-values func (main-iter inner-iter) instances args ...))))
    ((_ (main-iter inner-iter) continuation
        (modify-instance* index func))
     (let ((instance (lookup-instance-by-index main-iter index #:continuation continuation)))
       (let-values (((new-instance new-main-iter new-inner-iter)
                     (func instance main-iter inner-iter)))
         (let* ((new-by-index (fold-service-iter-by-index new-main-iter))
                (new-main-iter (fold-service-iter
                                (inherit new-main-iter)
                                (by-index (assq-set! new-by-index index new-instance)))))
           (values new-main-iter new-inner-iter)))))
    ((_ (main-iter inner-iter) continuation
        (modify-singleton type func))
     (match-record main-iter <fold-service-iter>
       (by-type by-index)
       (match (assq-ref by-type type)
         ((index)
          (let* ((instance (assq-ref by-index index))
                 (new-instance (func instance))
                 (new-by-index (assq-set! by-index index new-instance)))
            (values (fold-service-iter (inherit main-iter) (by-index new-by-index)) inner-iter)))
         ((_ . _)
          (continuation
           (make-parameterized-service-error*
            "Cannot extend parent service '~a': multiple instances found." type)))
         (else
          (continuation
           (make-parameterized-service-error*
            "Cannot extend parent service '~a': no instance of this type found." type))))))
    ((_ (main-iter inner-iter) continuation
        (add name type service base-service instance-value clause ...))
     (add-instance-list-iter (main-iter inner-iter) continuation
                             name type service base-service instance-value clause ...))
    ((_ (main-iter inner-iter) continuation
        (add-list name type service base-service instance-values clause ...))
     (fold-values
      (lambda (item current-main-iter current-inner-iter)
        (add-instance-list-iter (current-main-iter current-inner-iter) continuation
                                name type service base-service item clause ...))
      (main-iter inner-iter) instance-values))))


(define-syntax with-instance-list-iter
  (syntax-rules ()
    ((_ iter continuation clause)
     (let-values (((new-iter any)
                   (modify-instance-list-iter (iter #f) continuation clause)))
       new-iter))
    ((_ iter continuation clause clauses ...)
     (let-values (((new-iter any)
                   (modify-instance-list-iter (iter #f) continuation clause)))
       (with-instance-list-iter new-iter continuation clauses ...)))))


(define-syntax with-instance-list-iter*
  (syntax-rules ()
    ((_ (main-iter inner-iter) continuation clause)
     (modify-instance-list-iter (main-iter inner-iter) continuation clause))
    ((_ (main-iter inner-iter) continuation clause clauses ...)
     (let-values (((new-main-iter new-inner-iter)
                   (modify-instance-list-iter (main-iter inner-iter) continuation clause)))
       (with-instance-list-iter*
        (new-main-iter new-inner-iter)
        continuation clauses ...)))))


(define* (fold-parameterized-services service-elements #:key continuation parameters)
  (define* (report-error #:rest args)
    (continuation
     (apply make-parameterized-service-error* args)))

  (define (merge-extend-parent parent iter value)
    (match-record parent <parameterized-service>
      (extend type)
      (unless (procedure? extend)
        (report-error
         "Cannot extend service '~a' via merge strategy." type))
      (with-instance-list-iter iter continuation
        (modify-singleton type
          (lambda (instance)
            (let* ((current-value (service-instance-value instance))
                   (new-value (extend current-value value)))
              (service-instance
               (inherit instance)
               (value new-value))))))))

  (define (merge-extensible-parent parent iter value)
    (match-records* ((parent <service-parent> parameterized-service make-config)
                     (parameterized-service <parameterized-service> type extend))
        (let ((parent-value (make-config value #:parameters parameters)))
          (unless (procedure? extend)
            (report-error
             "Cannot extend service '~a' via merge strategy." type))

          (with-instance-list-iter iter continuation
            (modify-singleton type
              (lambda (instance)
                (let* ((current-value (service-instance-value instance))
                       (new-value (extend current-value parent-value)))
                  (service-instance
                   (inherit instance)
                   (value new-value)))))))))

  (define (collect-extensible-parents service)
    (filter
     (match-records*-lambda ((<service-parent> parameterized-service)
                             (parameterized-service <parameterized-service> extend))
         (procedure? extend))
     (parameterized-service-parents service)))

  (define* (resolve-instances initial #:key parameters)
    (fold
     (match-lambda*
       ((elem iter)
        (match-records* ((elem <fold-service-elem> kind service base-service value)
                         (service <parameterized-service> type constructor))
            (let* ((effective-value (if constructor
                                        (constructor value #:parameters parameters)
                                        value))
                   (instance-id (fold-service-iter-last-id iter)))
              (match kind
                ('extend
                 (merge-extend-parent service iter value))

                ('parameterized
                 (let* ((extensible-parents (collect-extensible-parents service)))
                   (with-instance-list-iter iter continuation
                     (fold-list merge-extensible-parent extensible-parents value)
                     (add type type service base-service effective-value)))))))))
     initial service-elements))

  (define (resolve-parent parent main-iter list-iter instance)
    (match-record parent <service-parent>
      (name type parameterized-service base-service make-config apply-as-list?)

      (let ((value (make-config (service-instance-value instance) #:parameters parameters)))
        (if apply-as-list?
            (with-instance-list-iter* (main-iter list-iter) continuation
              (add-list name type parameterized-service base-service value
                        (with-func fold-parents)
                        (apply-as-list? #t)))
            (with-instance-list-iter* (main-iter list-iter) continuation
              (add name type parameterized-service base-service value
                   (with-func fold-parents)))))))

  (define (fold-parents instance main-iter parent-iter)
    (define (collect-remaining-parents parent current)
      (match-record parent <service-parent>
        (parameterized-service)
        (match-record parameterized-service <parameterized-service>
          (type extend)
          (cond ((eq? extend 'list)
                 (cons parent current))
                ((procedure? extend)
                 current)
                (else
                 (report-error "Parent service '~a' is not extensible." type))))))

    (match-record instance <service-instance>
      (type index parameterized-service)
      (let* ((all-parents (parameterized-service-parents parameterized-service))
             (parent-list (fold collect-remaining-parents '() all-parents)))
        (with-instance-list-iter*
         (main-iter '()) continuation
         (fold-list* resolve-parent parent-list instance)
         (modify-instance* index
             (lambda (_ final-main-iter new-parent-iter)
               (let* ((current-parents (service-instance-parents instance))
                      (final-instance (service-instance
                                       (inherit instance)
                                       (parents new-parent-iter)))
                      (final-parent-iter `(,@parent-iter ,index)))
                 (values final-instance final-main-iter final-parent-iter))))))))


  (let* ((first-fold (resolve-instances (fold-service-iter))))
    (let-values (((parent-fold _)
                  (with-instance-list-iter* (first-fold '()) continuation
                    (fold-instances* fold-parents))))
      parent-fold)))


(define (resolve-instance instance main-iter)
  (define (add-as-list parent-list name parent)
    (let ((existing (or (assq-ref parent-list name) '())))
      (assq-set! parent-list name (cons parent existing))))

  (define (add-as-singleton parent-list name type parent)
    (when (assq-ref parent-list name)
      (raise-exception
       (make-parameterized-service-error*
        "Duplicate parent '~a' of type '~a' in '~a' instance."
        name type (service-instance-type instance))))
    (assq-set! parent-list name parent))

  (define (resolve-parent parent-index parent-list)
    (let ((parent (lookup-instance-by-index main-iter parent-index)))
      (match-records* ((parent <service-instance> name type apply-as-list?))
          (mlet %state-monad ((resolved-parent (resolve-instance parent main-iter)))
            (let ((value (service-value resolved-parent)))
              (return
               (if apply-as-list?
                   (add-as-list parent-list name value)
                   (add-as-singleton parent-list name type value))))))))

  (define (apply-resolve-func service context value resolved-parents)
    (let* ((maybe-monadic-resolve-func (parameterized-service-resolve service))
           (parameters (monadic-context-parameters context))
           (arg-list (fold (lambda (item current)
                             `(,@current ,(symbol->keyword (car item)) ,(cdr item)))
                           `(,value #:parameters ,parameters) resolved-parents))
           (resolve-func (cdr maybe-monadic-resolve-func)))
      (with-monad %state-monad
        (if (car maybe-monadic-resolve-func)
            (apply resolve-func arg-list)
            (return (apply resolve-func arg-list))))))

  (define (inner-resolve service index value)
    (let* ((parents (service-instance-parents instance)))
      (mlet* %state-monad ((resolved-parents (foldm %state-monad resolve-parent '() parents))
                           (context (current-state))
                           (returned-service (apply-resolve-func service context value resolved-parents))
                           (returned-context (current-state)))
        (mbegin %state-monad
          (set-current-state (monadic-context
                              (inherit returned-context)
                              (services (assq-set! (monadic-context-services returned-context)
                                                   index returned-service))))
          (return (cons index returned-service))))))

  (match-record instance <service-instance>
    (type parameterized-service base-service index value)
    (mlet %state-monad ((context (current-state)))
      (let* ((service-alist (monadic-context-services context))
             (already-resolved (assq-ref service-alist index)))
        (mbegin %state-monad
          (munless already-resolved
            (inner-resolve parameterized-service index value))
          (mlet %state-monad ((final-context (current-state)))
            (let ((value (assq-ref (monadic-context-services final-context) index)))
              (return (service base-service value)))))))))


(define-syntax create-fold-service-elem
  (lambda (x)
    (define (predicate-field-name name)
      (datum->syntax name
                     (symbol-append 'parameterized-service- (syntax->datum name))))

    (syntax-case x ()
      ((_ kind predicate-field type value)
       (and (identifier? #'kind) (identifier? #'predicate-field))
       (with-syntax ((predicate (predicate-field-name #'predicate-field))
                     (service #'(lookup-service-type parameterized type))
                     (base-service #'(lookup-service-type base type)))
         #'(let ((predicate-field (predicate service)))
             (when (and predicate-field
                        (not (predicate-field value)))
               (throw 'wrong-type-arg value))
             (make-fold-service-elem 'kind service base-service value)))))))


(define-syntax parameterized-services-clause
  (lambda (x)
    (syntax-case x (extend-service parameterized-service)
      ((_ (extend-service type value))
       (identifier? #'type)
       #'(create-fold-service-elem extend extend-type-predicate type value))
      ((_ (parameterized-service type value))
       (identifier? #'type)
       #'(create-fold-service-elem parameterized type-predicate type value)))))


(define-syntax parameterized-services
  (lambda (x)
    (syntax-case x ()
      ((_ parameters clause ...)
       #'(begin
           (let* ((services-or-exception
                   ;; Make sure all errors are reported at the macro expansion site,
                   ;; not inside fold-parameterized-services.
                   (call-with-current-continuation
                    (lambda (continuation)
                      (fold-parameterized-services
                       (list (parameterized-services-clause clause) ...)
                       #:continuation continuation
                       #:parameters parameters))))
                  (instance-iter (if (exception? services-or-exception)
                                     (raise-exception services-or-exception)
                                     services-or-exception))
                  (instance-list (map cdr (fold-service-iter-by-index instance-iter)))
                  (context (make-monadic-context parameters '() '())))
             (run-with-state
                 (mapm %state-monad (cut resolve-instance <> instance-iter) instance-list)
               context)))))))


;; Local Variables:
;; eval: (put 'modify-instance* 'scheme-indent-function 2)
;; eval: (put 'with-instance-list-iter 'scheme-indent-function 2)
;; eval: (put 'with-instance-list-iter* 'scheme-indent-function 2)
;; eval: (put 'modify-singleton 'scheme-indent-function 1)
;; End:
