;;; Copyright © 2023 Martin Baulig <martin@baulig.is>
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Affero General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.

;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (baulig utils syntax-macros)
  #:use-module (guix records)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:export (fold-values
            match-records*
            match-records*-lambda))


(define-syntax fold-values
  (syntax-rules ()
    ((_ func (iter extra-iters ...) input-list extra-args ...)
     (let ((input-iters (list iter extra-iters ...)))
       (if (null? input-list)
           (apply values input-iters)
           (apply values
                  (fold
                   (lambda (item current)
                     (call-with-values
                         (lambda () (apply func `(,item ,@current ,@(list extra-args ...))))
                       (lambda args args)))
                   (list iter extra-iters ...) input-list)))))))


(define-syntax %match-records
  (lambda (x)
    (syntax-case x ()
      ((_ (var type) body ...)
       (identifier? #'var)
       #'(begin
           (unless (eq? (struct-vtable var) type)
             (throw 'wrong-type-arg var))
           (begin body ...)))
      ((_ (var type args ...) body ...)
       (and (identifier? #'var)
            (every identifier? #'(list args ...)))
       #'(match-record var type (args ...) (begin body ...))))))


(define-syntax match-records*
  (syntax-rules ()
    ((_ (clause) body ...)
     (%match-records clause body ...))
    ((_ (clause clauses ...) body ...)
     (match-records* (clause)
         (match-records* (clauses ...) body ...)))))


(define-syntax match-records*-lambda
  (syntax-rules ()
    ((_ ((type args ...) clauses ...) body ...)
     (lambda (record)
       (match-records* ((record type args ...) clauses ...) body ...)))))
